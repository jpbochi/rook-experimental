﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using NAnt.Core;
using NAnt.Core.Attributes;
using Xunit;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Xunit.Runner.Nant
{
    [TaskName("xunit")]
    public class XUnitNantTask : Task
    {
        public XUnitNantTask()
        {
            ShadowCopy = true;
        }

        [TaskAttribute("assemblies", Required = true)]
        public string Assemblies { get; set; }

        [TaskAttribute("xml")]
        public string Xml { get; set; }

        [TaskAttribute("nunitXml")]
        public string NUnitXml { get; set; }
        
        [TaskAttribute("html")]
        public string Html { get; set; }

        [TaskAttribute("workingDir")]
        public string WorkingDir { get; set; }

        [TaskAttribute("shadowCopy")]
        public bool ShadowCopy { get; set; }

        [TaskAttribute("teamCity")]
        public bool TeamCity { get; set; }

        [TaskAttribute("requiresHomogenousAppDomain")]
        public bool RequiresHomogenousAppDomain { get; set; }
		        
        protected override void ExecuteTask()
        {
			if (TeamCity) Log(Level.Info, string.Format("TeamCity log is ON."));

            if (!string.IsNullOrWhiteSpace(WorkingDir)) Directory.SetCurrentDirectory(WorkingDir);

			var xunitVersion = Assembly.GetAssembly(typeof(FactAttribute)).GetName().Version;

			Log(Level.Info, "xUnit.net XUnit runner (xunit.dll version {0} - {1}-bit .NET {2})", xunitVersion, IntPtr.Size * 8, Environment.Version);

            var logger
				= TeamCity ? (IRunnerLogger)new TeamCityLogger(this)
				: Verbose ? new VerboseLogger(this)
				: new StandardLogger(this)
			;

			var assemblyFullpaths = Assemblies.Split('|').Select(x => new FileInfo(x).FullName);

            var assembliesNode = new StringBuilder();
            assembliesNode.Append("<assemblies>");

			bool succeeded = true;

            foreach (var assemblyPath in assemblyFullpaths)
            {
                string assemblyFilename = assemblyPath;
                string configFilename = null; // TODO: config files not supported yet

				bool asmSucceeded;
                assembliesNode.Append(ExecuteAssembly(this, assemblyFilename, configFilename, logger, out asmSucceeded));
				succeeded &= asmSucceeded;
            }

            assembliesNode.Append("</assemblies>");

            string fullXml = assembliesNode.ToString();

            if (Xml != null) new NullTransformer(Xml).Transform(fullXml);

            if (NUnitXml != null) {
                using (Stream xmlStream = ResourceStream("NUnitXml.xslt"))
                    new XslStreamTransformer(xmlStream, NUnitXml).Transform(fullXml);
			}

            if (Html != null) {
                using (Stream htmlStream = ResourceStream("HTML.xslt"))
                    new XslStreamTransformer(htmlStream, Html).Transform(fullXml);
			}

			if (!succeeded) throw new BuildException("unit.runner.nant tast failed.");
		}

        static Stream ResourceStream(string xmlResourceName)
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Xunit.Runner.Nant." + xmlResourceName);
        }

		public string ExecuteAssembly(Task log, string assemblyFilename, string configFilename, IRunnerLogger logger, out bool outSucceeded)
		{
			try
			{
				using (var wrapper = new ExecutorWrapper(assemblyFilename, configFilename, ShadowCopy))
				{
					log.Log(Level.Info, "xunit.dll:     Version {0}", wrapper.XunitVersion);
					log.Log(Level.Info, "Test assembly: {0}", assemblyFilename);

					var runner = new XmlTestRunner(wrapper, logger);
					var result = runner.RunAssembly();
					
					outSucceeded = result != TestRunnerResult.Failed;
					return runner.Xml;
				}
			}
			catch (Exception ex)
			{
				while (ex != null)
				{
					log.Log(Level.Error, ex.GetType().FullName + ": " + ex.Message);

					foreach (string stackLine in ex.StackTrace.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
						Log(Level.Error, stackLine);

					ex = ex.InnerException;
				}

				outSucceeded = false;
				return string.Empty;
			}
		}
    }
}