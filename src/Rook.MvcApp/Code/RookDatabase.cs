﻿using System.Configuration;
using System.Data.EntityClient;
using Rook.MvcApp.Entities;

namespace Rook.MvcApp
{
	internal class RookDatabase
	{
		static readonly string RookEntitiesConnStr;

		static RookDatabase()
		{
			var configured = ConfigurationManager.ConnectionStrings["RookEntities"].ConnectionString;

			var builder = new EntityConnectionStringBuilder(configured);

			builder.ProviderConnectionString = ConfigurationManager.ConnectionStrings["RookDb"].ConnectionString;

			RookEntitiesConnStr = builder.ConnectionString;
		}

		public static string GetRookEntitiesConnStr()
		{
			return RookEntitiesConnStr;
		}

		public static RookEntities GetEntities()
		{
			return new RookEntities(RookEntitiesConnStr);
		}
	}
}