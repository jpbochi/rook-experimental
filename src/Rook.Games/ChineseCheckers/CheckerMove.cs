﻿using System;

namespace Rook.Games.ChineseCheckers
{
	internal class CheckerMove : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return CheckerMove.Template; } }

			IGameMove IMoveBuilder.Build(UriTemplateMatch match, IGameState state)
			{
				return new CheckerMove(
					match.BoundVariables["playerid"],
					state.Board.Pieces.ById(match.BoundVariables["piece"]),
					match.BoundVariables["destination"]
				);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/move/{playerid}/{piece}/{destination}");
		internal const string Relation = RookRelations.Moves.MovePiece;

		public RName PlayerId { get; private set; }
		public IGamePiece Piece { get; private set; }
		public RName Destination { get; private set; }

		public CheckerMove(RName playerId, IGamePiece piece, RName destination)
		{
			this.PlayerId = playerId;
			this.Piece = piece;
			this.Destination = destination;
		}

		string IGameMove.RequiredRole { get { return PlayerId; } }

		IHyperLink IGameMove.Link
		{
			get {
				return HyperLink.From(Template, Relation, new { PlayerId, Piece = Piece.Id, Destination }, new { PlayerId, Origin = Piece.Position, Destination });
			}
		}

		public void Execute(IMoveExecutionContext context)
		{
			var cmds = new [] {
				HyperLink.BuildUri(CommandTemplates.MovePiece, new { Piece = Piece.Id, Destination }),
				HyperLink.BuildUri(CommandTemplates.CycleTurn),
				HyperLink.BuildUri(ChineseCheckersScoreCommand.Template),
			};

			foreach (var cmd in cmds) context.ExecuteMove(cmd);
		}
	}
}
