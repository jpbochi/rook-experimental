﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using JpLabs.Extensions;
using Xunit;
using Xunit.Sdk;

namespace Rook
{
	public static partial class ShouldExt
	{
		class EnumerableComparer<T> : IEqualityComparer<IEnumerable<T>>
		{
			public static IEqualityComparer<T> DefaultComparer = EqualityComparer<T>.Default;
			private readonly IEqualityComparer<T> comparer;

			public EnumerableComparer(IEqualityComparer<T> comparer)
			{
				this.comparer = comparer ?? DefaultComparer;
			}

			public bool Equals(IEnumerable<T> x, IEnumerable<T> y)
			{
				return x.Count() == y.Count() && x.Zip(y, (xi, yi) => comparer.Equals(xi, yi) ).All( b => b );
			}

			public int GetHashCode(IEnumerable<T> obj)
			{
				throw new NotImplementedException();
			}
		}

		public static TExpected ShouldBeOfType<TExpected>(this object obj)
		{
			Assert.IsAssignableFrom<TExpected>(obj);
			return (TExpected)obj;
		}

		public static T ShouldNotBeNull<T>(this T obj)
		{
			Assert.NotNull(obj);
			return obj;
		}

		public static T ShouldBe<T>(this T actual, T expected)
		{
			if (expected is IEquatable<T>) {
				if (!((IEquatable<T>)expected).Equals(actual)) throw new EqualException(expected, actual);
			} else if (expected is IStructuralEquatable) {
				if (!StructuralComparisons.StructuralEqualityComparer.Equals(expected, actual)) throw new EqualException(expected, actual);
			} else {
				Assert.Equal(expected, actual);
			}
			return actual;
		}

		public static T ShouldNotBe<T>(this T actual, T expected)
		{
			if (expected is IEquatable<T>) {
				if (((IEquatable<T>)expected).Equals(actual)) throw new EqualException(expected, actual);
			} else if (expected is IStructuralEquatable) {
				if (StructuralComparisons.StructuralEqualityComparer.Equals(expected, actual)) throw new EqualException(expected, actual);
			} else {
				Assert.Equal(expected, actual);
			}
			return actual;
		}

		public static T ShouldBe<T>(this T actual, T expected, IEqualityComparer<T> comparer)
		{
			Assert.Equal(expected, actual, comparer);
			return actual;
		}

		public static void ShouldStartWith(this string actual, string start)
		{
			if (!actual.StartsWith(start)) throw new AssertActualExpectedException(actual, start, "String did not start with Expected value");
		}

		public static void ShouldMatch(this string actual, string pattern)
		{
			if (!Regex.IsMatch(actual, pattern)) throw new AssertActualExpectedException(actual, pattern, "String did not match Pattern");
		}

		public static void ShouldMatch(this Uri actual, string template)
		{
			var uriTemplate = new UriTemplate(template);

			Uri baseUri, absoluteUri;
			if (actual.IsAbsoluteUri) {
				baseUri = new Uri(actual.GetLeftPart(UriPartial.Authority));
				absoluteUri = actual;
			} else {
				baseUri = new Uri("http://test");
				absoluteUri = new Uri(baseUri, actual);
			}

			if (uriTemplate.Match(baseUri, absoluteUri) == null) throw new AssertActualExpectedException(actual, template, "Uri did not match Template");
		}

		public static void ShouldEach<T>(this IEnumerable<T> source, Action<T> assertAction)
		{
			source.ForEach(assertAction);
		}

		public static void ShouldEachBeOfType<T>(this IEnumerable source)
		{
			source.ForEach(item => Assert.IsAssignableFrom<T>(item));
		}

		public static void ShouldBeEmpty<T>(this IEnumerable<T> series)
		{
			ShouldContainSameAs(series, Enumerable.Empty<T>());
		}
		
		public static void ShouldHaveNoDuplicates<T>(this IEnumerable<T> list)
		{
			list.ShouldContainSameAs(list.Distinct());
		}

		public static void ShouldContainSameAs<T>(this IEnumerable<T> actual, IEnumerable<T> expected, IEqualityComparer<T> comparer = null)
		{
			Func<T,int> hashCodeFunc = ((T x) => comparer.GetHashCode(x));

			if (comparer == null) hashCodeFunc = ((T x) => StructuralComparisons.StructuralEqualityComparer.GetHashCode(x));

			var orderedExpected = expected.OrderBy(hashCodeFunc).ToArray();
			var orderedActual = actual.OrderBy(hashCodeFunc).ToArray();

			if (!new EnumerableComparer<T>(comparer).Equals(orderedExpected, orderedActual)) {
				throw new AssertActualExpectedException(expected.ToArray(), actual.ToArray(), "Enumerable don't contain same expected items", true);
			}
		}

		public static void ShouldBeEqualInOrder<T>(this IEnumerable<T> actual, IEnumerable<T> expected, IEqualityComparer<T> comparer = null)
		{
			Assert.Equal(expected.ToArray(), actual.ToArray(), new EnumerableComparer<T>(comparer));
		}

		public static T GetDynamic<T>(this object obj, Func<dynamic,dynamic> selector)
		{
			return (T)selector((dynamic)obj);
		}
	}
}
