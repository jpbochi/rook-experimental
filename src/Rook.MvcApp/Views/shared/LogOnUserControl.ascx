﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="Rook.MvcApp.Controllers" %>


<% if (Request.IsAuthenticated) { %>

	<span>Welcome <b><%= Html.Encode(Page.User.Identity.Name) %></b> | </span>
	<%= Html.ActionLink("Log Out", UsersController.LogOutActionName, UsersController.ControllerName, null, new { @class = "button" } ) %>
<%-- 
	<br />
	<%= Html.ActionLink("Update Profile", "Edit", "Account", new { userName = Page.User.Identity.Name }, null ) %>
--%>

<% } else { %>

	<%= Html.ActionLink("Log In", UsersController.LogInActionName, UsersController.ControllerName, null, new { @class = "button" } ) %>

<% } %>
