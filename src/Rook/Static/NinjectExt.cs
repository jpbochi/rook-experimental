﻿using System.Collections.Generic;
using JpLabs.Extensions;
using JpLabs.Symbols;
using Ninject;
using Ninject.Parameters;
using Ninject.Syntax;

namespace Rook
{
	public static class NinjectExt
	{
		public static T InjectedBy<T>(this T obj, IKernel kernel, params IParameter[] parameters)
		{
			kernel.Inject(obj, parameters);
			return obj;
		}

		public static IGameReferee GetReferee(this IResolutionRoot kernel)
		{
			return MasterReferee.MergeReferees(kernel.GetAll<IGameReferee>());
		}

		public static IGameReferee GetReferee(this IResolutionRoot kernel, string name)
		{
			return kernel.Get<IGameReferee>(name);
		}

		public static IBindingWithOrOnSyntax<T> Named<T>(this IBindingNamedSyntax<T> that, Symbol symbol)
		{
			return that.Named(symbol.FullName);
		}

		internal static IEnumerable<IParameter> GetParameters(object args)
		{
			if (args == null) yield break;

			foreach (var pi in args.GetType().GetProperties()) {
				yield return new ConstructorArgument(pi.Name.ToCamelCase(), pi.GetValue(args, null));
			}
		}
	}
}
