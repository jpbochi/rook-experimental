﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Symbols;

namespace Rook.Games.Go
{
	internal class GoReferee : BaseReferee, IGameReferee
	{
		private static readonly MoveBuilderTable moveTable = new MoveBuilderTable(new [] {
			GoDropStoneMove.Builder,
			PassTurnMove.Builder,
			GoScoreCommand.Builder,
		});

		public IEnumerable<IHyperLink> GetMoves(IGameState state)
		{
			if (state.IsOver()) yield break;

			var currentPlayerId = state.CurrentPlayerId;
			yield return new PassTurnMove(currentPlayerId).Link;

			var opponent = Rook.Commands.CycleTurnCommand.GetNextPlayer(state).Id;
			var board = state.Board;

			var pieceClass = GoPieces.OfPlayer[currentPlayerId];
			var opponentPieceClass = GoPieces.OfPlayer[opponent];
			
			//Searching for valid moves in parallel showed a 60% gain in measured performance
			var dropMoves = board
				.Region(Tiles.MainBoard)
				.AsParallel()
				.Where(p => board.PiecesAt(p).IsEmpty())
				.Select(p => new GoDropStoneMove(currentPlayerId, pieceClass, p))
				.Where(m => !m.IsSuicide(board, opponentPieceClass))
				.Where(m => !m.IsKo(state))
				.Select(m => m.Link);
			;

			foreach (var move in dropMoves) yield return move;
		}

		public override IGameMove FindMove(Uri moveUri, IGameState state)
		{
			//TODO: Optimization opportunity: Go has a exceptionally high number of available moves (361 at start with a 19x19 board)
			// When trying to play a move, I MUST verify if that move is valid, instead of creating a full list.
			// In any case, I MUST NOT duplicate rule implementations.

			return moveTable.Match(moveUri, state);
		}
	}

	internal static class GoExtensions
	{
		internal static IEnumerable<RName> GetGroup(this IGameBoard board, RName root, Func<RName,bool> stopGroupingFunc)
		{
			var group = new HashSet<RName>();

			return YieldConnectedGroup(board, root, group, stopGroupingFunc);
		}

		private static IEnumerable<RName> YieldConnectedGroup(this IGameBoard board, RName tile, ISet<RName> group, Func<RName,bool> stopGroupingFunc)
		{
			if (group.Contains(tile)) yield break;
			group.Add(tile);

			yield return tile;

			if (stopGroupingFunc(tile)) yield break;

			foreach (var link in board.Graph.GetOutLinks(tile))
			foreach (var connected in board.YieldConnectedGroup(link.Target, group, stopGroupingFunc)) yield return connected;
		}

		internal static IEnumerable<IEnumerable<RName>> GroupBy(this IGameBoard board, Func<RName,IEnumerable<RName>> groupFunc)
		{
			//*//
			//var pendingTiles = board.GetAllTiles().ToList();
			var visitedTiles = new HashSet<RName>();
			//for (int i=0; i<pendingTiles.Count; i++) {
			//	var tile = pendingTiles[i];
			foreach (var tile in board.Region(Tiles.MainBoard)) {
				if (!visitedTiles.Contains(tile)) {
					var group = groupFunc(tile).ToArray();
					visitedTiles.UnionWith(group);

					yield return group;
				}
			}
			/*/
			var pendingTiles = new HashSet<PointInt>(board.GetAllTiles());
			while (pendingTiles.Count > 0) {
				var tile = pendingTiles.First();

				var group = groupFunc(tile).ToArray();
				pendingTiles.ExceptWith(group);

				yield return group;
			}//*/
		}

		internal static IEnumerable<IEnumerable<RName>> GroupBy(this IGameBoard board, Func<RName,bool> stopGroupingFunc)
		{
			return board.GroupBy(tile => board.GetGroup(tile, stopGroupingFunc));
		}
	}
}
