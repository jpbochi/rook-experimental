﻿using System.Linq;
using Xunit;

namespace Rook.Games.Tests.Amazons
{
	public class MachineTests
	{
		private readonly Mother mother = new Mother();

		[Fact]
		void should_provide_initial_move_options()
		{
			var game = mother.GetNewGameMachine();

			AmazonsGameModuleTests.VerifyInitialState(game.State);
			AmazonsRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		[Fact]
		void should_provide_resources()
		{
			var resources = mother.GetNewGameMachine().Resources.Select(r => r.ShouldBeOfType<EmbeddedResourceInfo>());;

			resources.Select(r => r.CssClass).ShouldContainSameAs(new [] { "board-backboard squares", "piece-set chess" });
		}

		[Fact]
		void should_play_valid_first_move()
		{
			var game = mother.GetNewGameMachine();

			game.PlayMove("c1", "c5");

			string expectedBoard = (@"
				....A..
				.......
				A.....A
				.......
				a.A...a
				.......
				..a.a.."
			).ClearLiteral();

			Assert.Equal(expectedBoard, Helper.DumpBoard(game.State.Board));
			Assert.Equal(ClassicPlayer.White, game.State.CurrentPlayerId);
			game.State.AssertScoreIsIndeterminate();
		}

		[Fact]
		void should_declare_win_after_killing_White_move()
		{
			string initialBoard = (@"
				A.A
				##.
				a.a"
			).ClearLiteral();
			string finalBoard = (@"
				A..
				##A
				a#a"
			).ClearLiteral();

			mother.ParseAndBindBoard(initialBoard);
			var game = mother.GetNewGameMachine();

			game.PlayMove("c1", "c2");
			game.PlayDropAt("b3");

			Assert.Equal(finalBoard, Helper.DumpBoard(game.State.Board));

			game.State.GetPlayerById(ClassicPlayer.White).Score.ShouldBe(GameScore.Won);
			game.State.GetPlayerById(ClassicPlayer.Black).Score.ShouldBe(GameScore.Lost);
		}

		[Fact]
		void should_declare_win_after_killing_Black_move()
		{
			string initialBoard = (@"
				A.A
				#.#
				a.a"
			).ClearLiteral();
			string finalBoard = (@"
				A#A
				#a#
				a.."
			).ClearLiteral();

			mother.ParseAndBindBoard(initialBoard);
			var game = mother.GetNewGameMachine();
			game.State.CurrentPlayerId = ClassicPlayer.Black;

			game.PlayMove("c3", "b2");
			game.PlayDropAt("b1");

			Assert.Equal(finalBoard, Helper.DumpBoard(game.State.Board));

			game.State.GetPlayerById(ClassicPlayer.Black).Score.ShouldBe(GameScore.Won);
			game.State.GetPlayerById(ClassicPlayer.White).Score.ShouldBe(GameScore.Lost);
		}
	}
}
