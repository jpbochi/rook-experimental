﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Rook.Api;
using Rook.Games.Go;
using Xunit;
using Xunit.Extensions;

namespace Rook.Games.Tests.Go
{
	public class GoGameModuleTests
	{
		private readonly Mother mother = new Mother();

		public static IEnumerable<object[]> TestBoards
		{
			get { return Mother.GetBoards().Select( x => new object[] { x } ); }
		}

		[Fact]
		void should_create_initial_state()
		{
			var state = mother.GetNewState();
			
			VerifyInitialState(state);
		}

		public static void VerifyInitialState(IGameState state)
		{
			VerifyAnyState(state);

			state.CurrentPlayerId.ShouldBe(ClassicPlayer.Black);

			state.Board.PiecesAt(Tiles.MainBoard).ShouldBeEmpty();

			Assert.Equal(2, state.Board.Classes.Count());
			state.Board.Pieces.ById(GoPieces.BlackStone).ShouldNotBeNull().GetBanner().ShouldBe(ClassicPlayer.Black);
			state.Board.Pieces.ById(GoPieces.WhiteStone).ShouldNotBeNull().GetBanner().ShouldBe(ClassicPlayer.White);
		}

		internal static void VerifyAnyState(IGameState state)
		{
			state.Players.Select( p => p.Id ).ShouldBeEqualInOrder(new []{ ClassicPlayer.Black, ClassicPlayer.White });

			state.Board
			.ShouldNotBeNull()
			.Region(Tiles.MainBoard)
			.ShouldContainSameAs(
				from c in Enumerable.Range(0, Rook.Games.Go.GoGameModule.DefaultBoardSize)
				from r in Enumerable.Range(0, Rook.Games.Go.GoGameModule.DefaultBoardSize)
				select TilePos.Create(c, r).ToTileName()
			);

			state.Board.PiecesAt(Tiles.MainBoard).ShouldEach( p => GoPieces.OfPlayer[p.GetBanner()].ShouldBe(p.Class) );
		}
		
		[Theory]
		[PropertyData("TestBoards")]
		void should_parse_and_dump_board(string boardDesc)
		{
			var board = mother.ParseBoard(boardDesc);

			Assert.Equal(boardDesc, Helper.DumpBoard(board));
		}

		[Fact]
		void pieces_have_images_and_offset()
		{
			var state = mother.GetNewState();

			state.Board.Pieces.ById(GoPieces.BlackStone).GetDisplayImageOffset().ShouldBe(PieceImageSet.BlackChecker);
			state.Board.Pieces.ById(GoPieces.WhiteStone).GetDisplayImageOffset().ShouldBe(PieceImageSet.WhiteChecker);
		}

		[Fact]
		void should_create_board_with_configured_size()
		{
			const ushort expectedCols = 3;
			const ushort expectedRows = 4;
			var boardSize = new JpLabs.Geometry.PointInt(expectedCols, expectedRows);

			mother.GetModule<GoGameModule>().BoardSize = boardSize;

			var board = mother.GetNewState().Board.ShouldNotBeNull();

			board.Region(Tiles.MainBoard).ShouldContainSameAs(
				new RectangularGridTileSet(expectedCols, expectedRows).GetAllTiles()
			);
		}
	}
}
