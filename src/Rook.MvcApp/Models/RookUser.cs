﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using Rook.Api;

namespace Rook.MvcApp.Models
{
	public class RookUser
	{
		[DisplayName("OpenID")][StringLength(255)]
		public string OpenId { get; set; }

		[DisplayName("User Name")][Required][StringLength(42, MinimumLength=4)][RegularExpression(@"[a-zA-Z]\w*(?:\@\w+)?\.?\w+")]
		public string UserName { get; set; }

		[DisplayName("Full Name")][StringLength(128)]
		public string FullName { get; set; }

		[DisplayName("Email")][StringLength(128)]
		public string Email { get; set; }

		public static RookUser FromOpenId(IAuthenticationResponse response)
		{
			//var fetch = response.GetExtension<FetchResponse>();
			var claims = response.GetExtension<ClaimsResponse>();

			string fullName = (claims == null) ? null : claims.FullName;
			string nickname = (claims == null) ? null : claims.Nickname;
			string email = (claims == null) ? null : claims.Email;

			string username =
				(!string.IsNullOrWhiteSpace(nickname)) ? nickname :
				//(!string.IsNullOrWhiteSpace(fullName)) ? fullName :
				(!string.IsNullOrWhiteSpace(email)) ? email :
				response.FriendlyIdentifierForDisplay;

			return new RookUser() {
				OpenId = response.ClaimedIdentifier,
				UserName = username,
				FullName = fullName,
				Email = email
			};
		}

		public static RookUser FromIdentity(IOpenIdIdentity identity)
		{
			return new RookUser() {
				OpenId = identity.OpenId,
				UserName = identity.Name
			};
		}
	}
}