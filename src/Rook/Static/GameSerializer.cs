﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using JpLabs.Extensions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using Rook.Serialization;

namespace Rook
{
	public class GameSerializer
	{
		/*
		private static XmlObjectSerializer CreateSerializer<T>()
		{
			var knownTypes = GetKnownTypes();
			int maxItemsInObjectGraph = int.MaxValue;
			bool ignoreExtensionDataObject = false;
			//IDataContractSurrogate dataContractSurrogate = new SerializeBaseTypeSurrogate();
			//bool alwaysEmitTypeInformation = false;
			//
			//return new DataContractJsonSerializer(
			//	typeof(T),
			//	"root",
			//	knownTypes,
			//	maxItemsInObjectGraph,
			//	ignoreExtensionDataObject,
			//	dataContractSurrogate,
			//	alwaysEmitTypeInformation
			//);
			bool preserveObjectReferences = false;
			IDataContractSurrogate dataContractSurrogate = null;
			DataContractResolver dataContractResolver = new SymbolContractResolver();

			return new DataContractSerializer(
				typeof(T),
				knownTypes,
				maxItemsInObjectGraph,
				ignoreExtensionDataObject,
				preserveObjectReferences,
				dataContractSurrogate,
				dataContractResolver
			);
		}//*/

		private static IEnumerable<Type> GetKnownTypes()
		{
			return new [] {
				typeof(BasicGameState),
				typeof(BasicPieceContainer),
				typeof(BasicBoard),
				typeof(BasicTileGraph),
				typeof(BasicPlayer),
				typeof(BasicPiece),
				//typeof(Symbol),

				typeof(VectorBasedLinkSet),
				typeof(HexLinkSet),

				typeof(BasicTileSet),
				typeof(MergedTileSet),
				typeof(RectangularGridTileSet)
			};
		}

		class IgnoreIfInterface : IIgnoreIfNullConvention
		{
			bool IIgnoreIfNullConvention.IgnoreIfNull(MemberInfo memberInfo)
			{
				var dataMemberAttr = memberInfo.GetSingleAttrOrNull<DataMemberAttribute>(false);
				if (dataMemberAttr != null && !dataMemberAttr.EmitDefaultValue) return true;

				var propInfo = memberInfo as PropertyInfo;
				if (propInfo != null) return propInfo.PropertyType.IsInterface;

				var fieldInfo = memberInfo as FieldInfo;
				if (fieldInfo != null) return fieldInfo.FieldType.IsInterface;

				throw new NotSupportedException();
			}
		}

		class MemberFinder : IMemberFinderConvention
		{
			public IEnumerable<MemberInfo> FindMembers(Type type)
			{
				 return Enumerable.Concat<MemberInfo>(
					type
					.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
					.Where(p => p.CanRead && p.CanWrite && 
						(p.IsDefinedAttr<DataMemberAttribute>(false)
						//|| p.IsDefinedAttr<BsonExtraElementsAttribute>(false)
						)
					),
					type
					.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
					.Where(p => p.IsDefinedAttr<DataMemberAttribute>(false))
				);
			}
		}

		static GameSerializer()
		{
			var convention = new ConventionProfile()
				.SetIgnoreIfNullConvention(new IgnoreIfInterface())
				.SetMemberFinderConvention(new MemberFinder())
				//.SetExtraElementsMemberConvention(new NamedExtraElementsMemberConvention("ExtraElements"))
			;
			BsonClassMap.RegisterConventions(
				convention,
				type => !type.Assembly.IsBuiltByMicrosoft()
			);

			BsonSerializer.RegisterGenericSerializerDefinition(typeof(ReadOnlyCollection<>), typeof(EnumerableSerializer<>));
			
			BsonSerializer.RegisterSerializer(typeof(XElement), new XElementSerializer());
			BsonSerializer.RegisterSerializer(typeof(Uri), new UriSerializer());
			BsonSerializer.RegisterSerializer(typeof(RName), new RNameSerializer());
			BsonSerializer.RegisterSerializer(typeof(Point), new PointSerializer());

			GetKnownTypes()
				.Select(t => BsonClassMap.LookupClassMap(t))
				.ForEach(m => {
					if (!BsonClassMap.IsClassMapRegistered(m.ClassType)) BsonClassMap.RegisterClassMap(m);
				}
			);
		}

		public static string Serialize<T>(T obj)
		{
			/*
			using (var stream = new MemoryStream())
			{
				XmlObjectSerializer serializer = CreateSerializer<T>();

				serializer.WriteObject(stream, obj);
				stream.Seek(0, SeekOrigin.Begin);

				using (var reader = new StreamReader(stream)) return reader.ReadToEnd();
			}
			/* /
			var output = new StringBuilder();
			using (var textWriter = new StringWriter(output))
			using (var xmlWriter = new XmlTextWriter(textWriter) { Formatting = Formatting.Indented } )
			{
				XmlObjectSerializer serializer = CreateSerializer<T>();
				serializer.WriteObject(xmlWriter, obj);
			}
			
			return output.ToString();
			/*/
			return obj.ToJson();
			//*/
		}

		public static bool TryDeserialize<T>(string serializedObj, out T deserialized)
		{
			deserialized = default(T);
			try {
				deserialized = Deserialize<T>(serializedObj);
				return true;
			}
			catch (TypeLoadException) { return false; }
			catch (FileFormatException) { return false; }
			catch (SerializationException) { return false; }
			catch (BsonException) { return false; }
		}

		public static T Deserialize<T>(string serializedObj)
		{
			/*
			using (var stream = new MemoryStream(Encoding.Default.GetBytes(serializedObj.Replace("\r\n", ""))))
			{
				XmlObjectSerializer serializer = CreateSerializer<T>();
				return (T)serializer.ReadObject(stream);
			}
			/* /
			using (var textReader = new StringReader(serializedObj))
			using (var xmlReader = new XmlTextReader(textReader))
			{
				XmlObjectSerializer serializer = CreateSerializer<T>();
				return (T)serializer.ReadObject(xmlReader);
			}
			/*/
			return BsonSerializer.Deserialize<T>(serializedObj);
			//*/
		}
	}
}
