﻿using System.Drawing;
using Rook.Augmentation;

namespace Rook.Api
{
	public static class PieceImageSet
	{
		public static readonly Point WhiteChecker = new Point(0, 0);
		public static readonly Point BlackChecker = new Point(0, 1);
		public static readonly Point WhiteQueen = new Point(2, 0);
		public static readonly Point BlackQueen = new Point(2, 1);
		public static readonly Point LightRook = new Point(7, 0);
		public static readonly Point DarkRook = new Point(7, 1);
		public static readonly Point X = new Point(8, 0);
		public static readonly Point O = new Point(8, 1);
	}

	public static class PieceExt
	{
		public static AugProperty DisplayImageOffsetProperty =
			AugProperty.Create("DisplayImageOffset", typeof(Point), typeof(PieceExt), PieceImageSet.DarkRook);

		public static IGamePiece SetDisplayImageOffset(this IGamePiece piece, Point offset)
		{
			piece.SetValue(DisplayImageOffsetProperty, offset);
			return piece;
		}

		public static Point GetDisplayImageOffset(this IGamePiece piece)
		{
			return piece.GetValue<Point>(DisplayImageOffsetProperty);
		}
	}
}
