﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Rook.Api;
using Rook.Augmentation;
using Rook.MvcApp.RookAuthentication;

namespace Rook.MvcApp.Models
{
	public enum PlayersToControl
	{
		Random = -1,
		Hotseat = -2,
		First = 0,
		Second = 1
	}

	public class OptionViewModel
	{
		public string InitializerType { get; set; }
		public string PropertyName { get; set; }
		public string DisplayName { get; set; }
		public string Value { get; set; }
	}

	public class NewGameOptionsViewModel
	{
		public static IEnumerable<PlayersToControl> PlayerOrderValues
		{
			get { return Enum.GetValues(typeof(PlayersToControl)).Cast<PlayersToControl>(); }
		}

		public NewGameOptionsViewModel()
		{
			Options = new List<OptionViewModel>();
		}

		//http://haacked.com/archive/2008/10/23/model-binding-to-a-list.aspx
		public IList<OptionViewModel> Options { get; set; }

		[DisplayName("Game Name")]
		public string GameName { get; set; }
		
		[DisplayName("Game Type")]
		[Editable(false)]
		public string GameTypeName { get; set; }

		public IOpenIdIdentity CreatorIdentity { get; set; }

		[DefaultValue(PlayersToControl.Random)]
		[DisplayName("Play As")]
		public PlayersToControl PlayersToControl { get; set; }

		public IEnumerable<IGameModuleInitializer> GetInitializers(IModuleLocator moduleLocator)
		{
			var gameType = moduleLocator.FindType(GameTypeName);

			var initializerTypes = moduleLocator.FindInitializerTypesFor(gameType);
			foreach (var initializerType in initializerTypes) {
				var initializer = (IGameModuleInitializer)Activator.CreateInstance(initializerType);

				foreach (var prop in initializerType.GetProperties()) {
					var option = Options.First(
						x => x.InitializerType == moduleLocator.GetTypeName(initializerType)
						&& x.PropertyName == prop.Name
					);
					if (option != null) {
						object value;
						if (AugTypeConverter.Get(prop.PropertyType).TryConvert(option.Value, out value)) {
							prop.SetValue(initializer, value, null);
						}
					}
				}

				yield return initializer;
			}
		}
	}
}