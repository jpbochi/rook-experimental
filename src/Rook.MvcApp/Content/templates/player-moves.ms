﻿<div class="moves">
	<span>You can:</span>
	<ul class="list">
	{{#moves}}
		<li><a href="{{link.href}}" rel="{{link.rel}}" class="move-link">{{title}}</a></li>
	{{/moves}}
	</ul>
</div>
