﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Rook.MvcApp.Models.GameEntryViewModel>" %>
<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="JpLabs.Extensions" %>

<asp:Content ContentPlaceHolderID="TitleContent" runat="server">
<%: (this.GetEnvironment() == AppEnvironment.Release) ? "Rook" : "Rook Dev" %> - <%: Model.Entry.DisplayName%>
</asp:Content>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
	<link href="<%= ResolveUrl("~/content/css/play.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="hidden-move-list">
		<% Html.RenderAction("moves", new { gameId = Model.Entry.Id, playerId = Model.Entry.State.CurrentPlayerId }); %>
	</div>
	
	<% Html.RenderPartial("gameview"); %>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/jquery.min.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/json2.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/linq.min.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/jquery.linq.min.js")%>"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.0/mustache.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/sammy.js/0.7.1/sammy.min.js"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/sammy.mustache-0.7.2.min.js")%>"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/extensions.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/helpers.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/helpers/ajaxwaiter.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/graphics/boardCoordinateSystem.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/graphics/boardRenderer.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/graphics/templates.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/graphics/boardRoot.js")%>"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameparser.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/board.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameclient.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/viewui.js")%>"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gamecontroller.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/playevents.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/app.js")%>"></script>

	<% Html.RenderPartial("gamestate.js"); %>
</asp:Content>
