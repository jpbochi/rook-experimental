﻿using System.Reflection;

namespace Rook.Games.Tests.ChineseCheckers
{
	public class ChChSerializationTests : BaseSerializationTests
	{
		private readonly ChChMother mother = new ChChMother();

		protected override string GetSerializedInitialGameInProd()
		{
			return ReadTextResource("Rook.Games.Tests.SampleData.initial-chin-checkers.bson.txt", Assembly.GetExecutingAssembly());
		}

		protected override IGameState GetNewInitialState()
		{
			return mother.GetNewState();
		}

		protected override IGameState GetNewChangedState()
		{
			var game = mother.GetNewGameMachine();

			game.PlayMove("g15", "f13");

			return game.State;
		}

		protected override IGameMachine GetNewGameMachine()
		{
			return mother.GetNewGameMachine();
		}

		protected override void VerifyInitialState(IGameState state)
		{
			ChChModuleTests.VerifyInitialState(state);

			mother.BindState(state);
			var game = mother.GetNewGameMachine();

			ChChRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		protected override void VerifyAnyState(IGameState state)
		{
			ChChModuleTests.VerifyAnyState(state);
		}
	}
}
