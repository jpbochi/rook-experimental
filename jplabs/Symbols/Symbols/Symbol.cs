﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Schema;

namespace JpLabs.Symbols
{
	public static class SymbolExt
	{
		public static Symbol<TEnum> As<TEnum>(this Symbol symbol)
		{
			if (symbol is Symbol<TEnum>) return (Symbol<TEnum>)symbol;

			if (symbol == null || !symbol.Is<TEnum>()) return null;

			return new Symbol<TEnum>(symbol);
		}
	}

	internal class SymbolComparer : IEqualityComparer<Symbol>
	{
		bool IEqualityComparer<Symbol>.Equals(Symbol x, Symbol y)
		{
			return x == y;
		}

		int IEqualityComparer<Symbol>.GetHashCode(Symbol obj)
		{
			return (obj == null) ? -1 : obj.GetHashCode();
		}
	}

	//[Serializable]
	[TypeConverter(typeof(SymbolConverter))]
	//[DataContract(Namespace="http://jplabs.bochi.it/symbols")]//[KnownType(typeof(Symbol<>))]
	[XmlSchemaProvider("XmlSchemaProvider_GetSchema")]
	public class Symbol : IEquatable<Symbol>, IXmlSerializable
	{
		public static Symbol Null = Symbol.Declare(null, string.Empty);

		public static TypeConverter Converter = TypeDescriptor.GetConverter(typeof(Symbol));
		public static IEqualityComparer<Symbol> Comparer = new SymbolComparer();

		private static Regex SymbolParserRegex = new Regex(@"^:(?<t>[\w.+]*):(?<n>.+)$", RegexOptions.Compiled | RegexOptions.CultureInvariant);

		[IgnoreDataMember]protected string name;
		[IgnoreDataMember]protected Type declaringType;

		[DataMember(Name="symbol")]private string fullName;
		[IgnoreDataMember]private int hashCode;

		//[DataMember(Name="_type")]
		//private string SerializableDeclaringType
		//{
		//	get { return declaringType.FullName; } //I don't want to use the AssemblyQualifiedName
		//	set { declaringType = ResolveTypeFromFullName(value, true); }
		//}

		internal Symbol(){} //for serialization only
 
		internal Symbol(Type declaringType, string name)
		{
			//if (!string.IsInterned(symbol)) throw new ArgumentException();

			this.declaringType = declaringType;
			this.name = name;

			this.fullName = GetToString(declaringType, name);
			this.hashCode = fullName.GetHashCode();
		}

		internal Symbol(Symbol symbol) : this(symbol.EnumType, symbol.Name)
		{}

		public string Name { get { return name; } }

		public Type EnumType { get { return declaringType; } }

		public string FullName { get { return fullName; } }

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			//this.fullName = GetToString(declaringType, name);
			var parsed = Parse(fullName);
			this.name = parsed.name;
			this.declaringType = parsed.declaringType;
			this.hashCode = fullName.GetHashCode();
		}

		private static Type ResolveTypeFromFullName(string typeName, bool throwIfTypeNotFound)
		{
			return Type.GetType(
				typeName,
				null,
				(asm, name, caseSensitive) =>
					AppDomain.CurrentDomain.GetAssemblies().Select( subAsm => subAsm.GetType(name) ).Where(t => t != null).FirstOrDefault()
				,
				throwIfTypeNotFound
			);
		}

		/*
		private const string NameSerializationName = "s";
		private const string TypeSerializationName = "t";

		[Obsolete("A class can't be marked with Serializable and DataContract", true)]
		protected Symbol(SerializationInfo info, StreamingContext context)
		{
			this.name = info.GetString(NameSerializationName);
			this.declaringType = ResolveType(info.GetString(TypeSerializationName));

			this.asString = GetToString(declaringType, name);
			this.hashCode = asString.GetHashCode();
		}
 
		[Obsolete("", true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue(NameSerializationName, name);
			info.AddValue(TypeSerializationName, declaringType.AssemblyQualifiedName);
		}//*/

		public static Symbol Declare(Type declaringType, string name)
		{
			///about comparison of interned strings: http://dotnetperls.com/string-intern

			name = string.Intern(name);
			return new Symbol(declaringType, name);
		}

		public static Symbol<TEnum> Declare<TEnum>(string name)
		{
			name = string.Intern(name);
			return new Symbol<TEnum>(name);
		}

		public static bool TryParse(string symbolFullName, out Symbol symbol)
		{
			symbol = null;

			var match = SymbolParserRegex.Match(symbolFullName);
			if (!match.Success) return false;

			var typeName = match.Groups["t"].Value;
			Type declaringType;
			if (typeName == "") {
				declaringType = null;
			} else {
				declaringType = ResolveTypeFromFullName(typeName, false);
				if (declaringType == null) return false;
			}

			symbol = new Symbol(declaringType, match.Groups["n"].Value);
			return true;
		}

		public static Symbol Parse(string symbolFullName)
		{
			var match = SymbolParserRegex.Match(symbolFullName);

			var typeName = match.Groups["t"].Value;
			var declaringType = (typeName == "") ? null : ResolveTypeFromFullName(typeName, true);

			return new Symbol(declaringType, match.Groups["n"].Value);
		}

		public static Symbol Parse(Type declaringType, string symbol)
		{
			return new Symbol(declaringType, symbol);
		}

		public static IEnumerable<Symbol> GetEnumValues<TEnum>(bool includeInherited)
		{
			return GetEnumValues(typeof(TEnum), includeInherited);
		}

		public static IEnumerable<Symbol> GetEnumValues(Type declaringType, bool includeInherited)
		{
			return GetSymbolFields(declaringType, includeInherited).Select(fi => (Symbol)fi.GetValue(null));
		}

		internal static IEnumerable<FieldInfo> GetSymbolFields(Type declaringType, bool includeInherited)
		{
			var bindingFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

			if (includeInherited) bindingFlags |= BindingFlags.FlattenHierarchy;

			return declaringType.GetFields(bindingFlags).Where(
				fi => typeof(Symbol).IsAssignableFrom(fi.FieldType)
			);
		}

		public ICustomAttributeProvider GetAttributeProvider()
		{
			return GetSymbolFields(declaringType, true).Where(fi => Equals(fi.GetValue(null))).FirstOrDefault();
		}

		public bool Is<TEnum>()
		{
			return typeof(TEnum).IsAssignableFrom(declaringType);
		}

		public bool Is(Type enumType)
		{
			return enumType.IsAssignableFrom(declaringType);
		}

		public static bool operator ==(Symbol a, Symbol b)
		{
			return Equals(a, b);
		}

		public static bool operator !=(Symbol a, Symbol b)
		{
			return !Equals(a, b);
		}

		public override bool Equals(object obj)
		{
			return Equals(this, obj as Symbol);
		}

		public bool Equals(Symbol other)
		{
			return Equals(this, other);
		}

		public static bool Equals(Symbol a, Symbol b)
		{
			var oa = (object)a;
			var ob = (object)b;
			if (oa == ob) return true;
			if (oa == null || ob == null) return false;

			return a.declaringType == b.declaringType && a.name == b.name;
		}
 
		public override int GetHashCode()
		{
			return hashCode;
		}

		public Symbol Clone()
		{
			return new Symbol(this);
		}

		public override string ToString()
		{
			return fullName;
		}

		private static string GetToString(Type declaringType, string symbol)
		{
			var typeName = (declaringType == null) ? string.Empty : declaringType.FullName;
			return string.Concat(":", typeName, ":", symbol);
		}

		#region IXmlSerializable Members
		
		protected static XmlQualifiedName XmlSchemaProvider_GetSchema(XmlSchemaSet xs)
		{
			return new XmlQualifiedName("Symbol", "http://jplabs.bochi.it/symbols");
		}
		//*
		System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
		{
			throw new NotImplementedException();
		}

		void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
		{
			reader.MoveToContent();
			//var symbolFullName = reader.GetAttribute("symbol");
			var symbolFullName = reader.ReadString();

			var s = Parse(symbolFullName);

			this.declaringType = s.declaringType;
			this.name = s.name;

			this.fullName = GetToString(declaringType, name);
			this.hashCode = fullName.GetHashCode();
		}

		void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
		{
			//writer.WriteAttributeString("symbol", this.ToString());
			writer.WriteString(this.ToString());
		}
		//*/
		#endregion
	}

	//[DataContract(Namespace="http://jplabs.bochi.it/symbols")]
	[XmlSchemaProvider("XmlSchemaProvider_GetSchema")]
	public sealed class Symbol<TEnum> : Symbol
	{
		public static new TypeConverter Converter = TypeDescriptor.GetConverter(typeof(Symbol<TEnum>));

		internal Symbol() : base() {} //for serialization only

		internal Symbol(string name) : base(typeof(TEnum), name) {}

		internal Symbol(Symbol symbol) : base(symbol) {}

		private new static XmlQualifiedName XmlSchemaProvider_GetSchema(XmlSchemaSet xs)
		{
			return Symbol.XmlSchemaProvider_GetSchema(xs);
		}
	}
}
