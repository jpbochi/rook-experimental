﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Symbols;
using System;

namespace Rook.Games.TicTacToe
{
	internal class TicTacToeReferee : BaseReferee, IGameReferee
	{
		private static readonly MoveBuilderTable moveTable = new MoveBuilderTable(new [] {
			TicTacToeMove.Builder,
			TicTacToeScoreCommand.Builder,
		});

		public IEnumerable<IHyperLink> GetMoves(IGameState state)
		{
			if (state.IsOver()) return Enumerable.Empty<HyperLink>();

			var currentPlayerId = state.CurrentPlayerId;

			var pieceClass = state.Board.Pieces.ById(TicTacToePlayer.Piece[currentPlayerId]);

			return state.Board
				.Region(Tiles.MainBoard)
				.Where(p => state.Board.PiecesAt(p).IsEmpty())
				.Select(p => GetTicTacToeMove(currentPlayerId, pieceClass.Class, p))
				.Select(m => m.Link)
			;
		}

		public override IGameMove FindMove(Uri moveUri, IGameState state)
		{
			return moveTable.Match(moveUri, state);
		}

		IGameMove GetTicTacToeMove(RName playerId, RName pieceClass, RName position)
		{
			return new TicTacToeMove(playerId, pieceClass, position);
		}
	}
}
