﻿/// <reference path="board.js" />
/// <reference path="gameparser.js" />
"use strict";

var Rook = Rook || {};
Rook.GameClient = {
	loadState: function(stateHref, callback) {
		return $.ajax({
			url: stateHref,
			dataType: 'json',
			success: function(data) {
				var state = Rook.Parser.parseState(data);
				callback && callback(state);
			}
		});
	},
	loadGame: function(gameHref, callback) {
		return $.ajax({
			url: gameHref,
			dataType: 'json',
			success: function(data) {
				var game = Rook.Parser.parseGame(data);
				callback && callback(game);
			}
		});
	}
};