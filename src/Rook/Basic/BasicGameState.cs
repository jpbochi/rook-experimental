﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using JpLabs.Extensions;
using Rook.Augmentation;

namespace Rook
{
	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class BasicGameState : AugObject, IGameState
	{
		public static AugProperty CurrentPlayerIdProperty =
			AugProperty.Create("CurrentPlayerId", typeof(RName), typeof(BasicGameState), null);
		public static AugProperty IdProperty =
			AugProperty.Create("Id", typeof(string), typeof(BasicGameState), null);

		protected override AugObject AugParent { get { return null; } }
		
		[DataMember]
		public string Version { get; private set; }

		[DataMember]
		public IEnumerable<IGamePlayer> Players { get; private set; }

		[DataMember]
		public IGameBoard Board { get; private set; }

		[IgnoreDataMember]
		public RName CurrentPlayerId
		{
			get { return (RName)GetValue(CurrentPlayerIdProperty); }
			set { SetValue(CurrentPlayerIdProperty, value); }
		}

		[IgnoreDataMember]
		public string Id
		{
			get { return (string)GetValue(IdProperty); }
			set { SetValue(IdProperty, value); }
		}

		[DataMember(EmitDefaultValue=false)]
		public IEnumerable<RName> Winners { get; set; }

		public BasicGameState(IEnumerable<IGamePlayer> players, IGameBoard board)
		{
			this.Players = players.EmptyIfNull().ToReadOnlyColl();
			this.Board = board;
			Revise();
		}

		public void Revise()
		{
			Version = ShortGuid.NewGuid().ToString();
		}
	}
}
