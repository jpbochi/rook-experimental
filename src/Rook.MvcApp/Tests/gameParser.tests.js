﻿/*global module, test, equal, deepEqual, sinon */
/*global Rook */
/// <reference path="../scripts/game/gameParser.js" />
/// <reference path="http://code.jquery.com/qunit/qunit-1.10.0.js" />
/// <reference path="lib/sinon.js" />
/// <reference path="lib/sinon-qunit.js" />

(function() {
	function buildStateJson(state) {
		return _.defaults(state || {}, {
			"version": "RvyKGOSsoUmvJ4gWBmoNHw",
			"currentPlayer": "player!black",
			"players": [
				{
					"id": "player!white",
					"score": "playing",
					"name": "White",
					"links": [{
						"rel": "game-for-player",
						"href": "/game/27/for/player!white"
					}]
				},
				{
					"id": "player!black",
					"score": "playing",
					"name": "Black",
					"links": [{
						"rel": "game-for-player",
						"href": "/game/27/for/player!black"
					}]
				}
			],
			"tiles": [
				{ "name": "a1", "position": { "x": 0, "y": 0 } },
				{ "name": "a2", "position": { "x": 0, "y": 1 } }
			],
			"pieces": [
				{
					"id": "amazons!amazon-16909a31",
					"position": "c3",
					"imagePos": { "x": 2, "y": 0 }
				},
				{
					"id": "amazons!amazon-63a90e9",
					"position": "c1",
					"imagePos": { "x": 2, "y": 0 }
				}
			]
		});
	}

	function buildGameJson(game) {
		return _.defaults(game || {}, {
			state: buildStateJson(),
			moves: [
				{
					"href": "/api/games/211/move/resign/coringa",
					"rel": "resign",
					"data": {
						"PlayerId": "coringa",
						"Title": "Resign"
					}
				},
				{
					"href": "/api/games/211/move/slide/coringa/capanga-123/a1",
					"rel": "move-piece",
					"data": {
						"PlayerId": "coringa",
						"Origin": "a3",
						"Destination": "a1"
					}
				}
			]
		});
	}

	module("Rook.Parser/parseState");

	test("should copy version and currentPlayer", function() {
		var stateJson = buildStateJson({ version: 'XYZ', currentPlayer: 'batima' });
		var parsed = Rook.Parser.parseState(stateJson);

		equal(parsed.version, stateJson.version);
		equal(parsed.currentPlayer, stateJson.currentPlayer);
	});

	test("should copy player list", function() {
		var stateJson = buildStateJson();
		var parsed = Rook.Parser.parseState(stateJson);

		equal(parsed.players, stateJson.players);
	});

	test("should parse players", function() {
		var stateJson = buildStateJson();
		var parsed = Rook.Parser.parseState(stateJson);

		var expectedPlayersById = {
			"player!white": {
				"score": "playing",
				"name": "White",
				"links": {
					"game-for-player": "/game/27/for/player!white"
				}
			},
			"player!black": {
				"score": "playing",
				"name": "Black",
				"links": {
					"game-for-player": "/game/27/for/player!black"
				}
			}
		};
		deepEqual(parsed.playersById, expectedPlayersById);
	});

	test("should parse board", function() {
		var stubbedBoard = { stub: 'return from Rook.Parser.parseBoard()' };
		this.stub(Rook.Parser, 'parseBoard').returns(stubbedBoard);

		var gameToParse = '"game to parse"';
		var parsed = Rook.Parser.parseState(gameToParse);

		equal(parsed.board, stubbedBoard);
		sinon.assert.calledWith(Rook.Parser.parseBoard, gameToParse);
	});

	module("Rook.Parser/parseBoard");

	test("should parseBoard with tiles and pieces", function() {
		var stubbedPieces = '"stubbed return from Rook.Parser.parsePieces()"';
		this.stub(Rook.Parser, 'parsePieces').returns(stubbedPieces);

		var stubbedBoard = { stub: 'stubbed return from Rook.Board()' };
		this.stub(Rook, 'Board').returns(stubbedBoard);

		var board = {
			tile: '"tiles to parse"',
			pieces: '"pieces to parse"'
		};
		var parsedBoard = Rook.Parser.parseBoard(board);

		equal(parsedBoard, stubbedBoard);
		sinon.assert.calledWith(Rook.Parser.parsePieces, board.pieces);
		sinon.assert.calledWith(Rook.Board, board.tiles, stubbedPieces);
	});

	module("Rook.Parser/parsePieces");

	test("should parse pieces", function() {
		var piecesToParse = ['raw piece 01', 'raw piece 02'];
		var expectedParsedPieces = ['parsed piece 01', 'parsed piece 02'];

		var pieceStub = this.stub(Rook.Parser, 'parsePiece');
		pieceStub.withArgs(piecesToParse[0]).returns(expectedParsedPieces[0]);
		pieceStub.withArgs(piecesToParse[1]).returns(expectedParsedPieces[1]);
		
		var parsedPieces = Rook.Parser.parsePieces(piecesToParse);

		deepEqual(parsedPieces, expectedParsedPieces);
	});

	module("Rook.Parser/parsePiece");

	test("should parse one piece", function() {
		var pieceToParse = {
			"id": "amazons!amazon-16909a31",
			"position": "c3",
			"imagePos": { "x": 2, "y": 0 }
		};

		var stubbedPiece = { stub: 'return from Rook.Piece()' };
		this.stub(Rook, 'Piece').returns(stubbedPiece);

		var parsedPiece = Rook.Parser.parsePiece(pieceToParse);

		equal(parsedPiece, stubbedPiece);
		sinon.assert.calledWith(Rook.Piece,
			pieceToParse.id,
			pieceToParse.position,
			{ dx: pieceToParse.imagePos.x, dy: pieceToParse.imagePos.y }
		);
	});

	module("Rook.Parser/parseGame");

	test("should parse player id that request game", function() {
		var gameJson = buildGameJson({ player: 'mr-white' });

		var parsed = Rook.Parser.parseGame(gameJson);

		equal(parsed.player, 'mr-white');
	});

	test("should parse game state", function() {
		var gameJson = buildGameJson({ state: 'fake state' });

		var stubbedState = 'stubbed return from Game.Parser.parseState()';
		this.stub(Rook.Parser, 'parseState').withArgs(gameJson.state).returns(stubbedState);

		var parsed = Rook.Parser.parseGame(gameJson);

		equal(parsed.state, stubbedState);
	});

	test("should parse moves", function() {
		var gameJson = buildGameJson({ moves: 'fake moves' });

		var stubbedMoves = 'stubbed return from Game.Parser.parseMoves()';
		this.stub(Rook.Parser, 'parseMoves').withArgs(gameJson.moves).returns(stubbedMoves);

		var parsed = Rook.Parser.parseGame(gameJson);

		equal(parsed.moves, stubbedMoves);
	});

	module("Rook.Parser/parseMoves");

	test("should parse moves", function() {
		var movesJson = buildGameJson().moves;
		var expectedMoves = {
			'coringa': [
				{
					player: 'coringa',
					title: 'Resign',
					link: {
						href: '/api/games/211/move/resign/coringa',
						rel: 'resign'
					}
				},
				{
					player: 'coringa',
					origin: 'a3',
					destination: 'a1',
					link: {
						href: '/api/games/211/move/slide/coringa/capanga-123/a1',
						rel: 'move-piece'
					}
				}
			]
		};

		var parsed = Rook.Parser.parseMoves(movesJson);

		deepEqual(parsed, expectedMoves);
	});
})();