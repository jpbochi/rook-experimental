﻿using System.Web.Mvc;

namespace Rook.MvcApp.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}
