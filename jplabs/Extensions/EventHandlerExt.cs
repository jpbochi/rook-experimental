﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.Extensions
{
	public static class EventHandlerExt
	{
		public static void RaiseEvent(this EventHandler handler, object sender, EventArgs e)
		{
			if (handler != null) handler(sender, e);
		}
		
		public static void RaiseEvent<T>(this EventHandler<T> handler, object sender, T e) where T : EventArgs
		{
			if (handler != null) handler(sender, e);
		}
	}
}
