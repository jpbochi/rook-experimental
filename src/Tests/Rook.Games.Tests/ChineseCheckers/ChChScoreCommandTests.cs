﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Rook.Games.ChineseCheckers;
using Rook.Tests.Extensions;
using Xunit;
using Xunit.Extensions;

namespace Rook.Games.Tests.ChineseCheckers
{
	public class ChineseCheckersScoreCommandTests
	{
		private readonly ChChMother mother = new ChChMother();

		[Fact]
		void should_declare_South_win()
		{
			var board = "N-g9|S-e4|S-f2|S-f3|S-f4|S-g1|S-g2|S-g3|S-g4|S-h3|S-h4";
			mother.ParseAndBindBoard(board);
			var state = mother.GetNewState();

			var recorder = new UriCommandRecorder();
			var cmd = new ChineseCheckersScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", ChineseCheckersPlayer.First, GameScore.Won), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ChineseCheckersPlayer.Second, GameScore.Lost), UriKind.Relative),
				new Uri("/set-game-over", UriKind.Relative),
			});
		}

		[Fact]
		void should_declare_North_win()
		{
			var board = "N-e14|N-f14|N-f15|N-f16|N-g14|N-g15|N-g16|N-g17|N-h14|N-h15|S-g8";
			mother.ParseAndBindBoard(board);
			var state = mother.GetNewState();

			var recorder = new UriCommandRecorder();
			var cmd = new ChineseCheckersScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", ChineseCheckersPlayer.Second, GameScore.Won), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", ChineseCheckersPlayer.First, GameScore.Lost), UriKind.Relative),
				new Uri("/set-game-over", UriKind.Relative),
			});
		}
	}
}
