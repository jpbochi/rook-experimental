﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Extensions;
using Xunit;

namespace JpLabs.Extensions.Tests
{
	public class CompressionExt_Tests
	{
		static Random GetDeterministicRandom()
		{
			return new Random(42);
		}

		static byte[] GetData()
		{
			return Enumerable.Range(0, 4096).Select(i => (byte)i).ToArray();
		}

		static string GetDataString()
		{
			return
@"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
 et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
 Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
 amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
 aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita
 kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
 consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
 erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
 gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
		}

		[Fact]
		void should_encode_to_base64_string()
		{
			Assert.Equal("/A==", new byte[] { 63 << 2 }.ToBase64String());
			Assert.Equal("+A==", new byte[] { 62 << 2 }.ToBase64String());
			Assert.Equal("BwM=", new byte[] { 7, 3 }.ToBase64String());
		}

		[Fact]
		void should_encode_to_base64_url()
		{
			Assert.Equal("_A", new byte[] { 63 << 2 }.ToBase64Url());
			Assert.Equal("-A", new byte[] { 62 << 2 }.ToBase64Url());
			Assert.Equal("BwM", new byte[] { 7, 3 }.ToBase64Url());
		}

		[Fact]
		void CompressBytes()
		{
			var data = GetData();

			var compressed = data.Compress();

			Assert.NotEqual(data.Length, compressed.Length);
		}

		[Fact]
		void DecompressBytes_Gives_SameData()
		{
			var data = GetData();

			var compressed = data.Compress();
			var decompressed = compressed.Decompress();

			Assert.Equal(data, decompressed);
		}


		[Fact]
		void CompressString()
		{
			var text = GetDataString();

			var compressed = text.Compress();

			Assert.NotEqual(text.Length, compressed.Length);
		}

		[Fact]
		void DecompressString_Gives_SameData()
		{
			var text = GetDataString();

			var compressed = text.Compress();
			var decompressed = compressed.DecompressToString();

			Assert.Equal(text, decompressed);
		}
	}
}
