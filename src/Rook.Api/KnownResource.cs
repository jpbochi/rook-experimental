﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rook.Api
{
	static public class KnownResource
	{
		static public class Avatar
		{
			public static readonly IHyperLink Default = DotBlack;

			public static readonly IHyperLink DotWhite = new HyperLink("/content/img/avatar/dot-white.gif", "player-avatar");
			public static readonly IHyperLink DotBlack = new HyperLink("/content/img/avatar/dot-black.gif", "player-avatar");

			public static readonly IHyperLink TttX = new HyperLink("/content/img/avatar/ttt-x.png", "player-avatar");
			public static readonly IHyperLink TttO = new HyperLink("/content/img/avatar/ttt-o.png", "player-avatar");

			public static readonly IHyperLink White = new HyperLink("/content/img/avatar/good-white.png", "player-avatar");
			public static readonly IHyperLink Black = new HyperLink("/content/img/avatar/good-black.png", "player-avatar");
			public static readonly IHyperLink Blue = new HyperLink("/content/img/avatar/good-blue.png", "player-avatar");
			public static readonly IHyperLink Green = new HyperLink("/content/img/avatar/good-green.png", "player-avatar");
			public static readonly IHyperLink Orange = new HyperLink("/content/img/avatar/good-orange.png", "player-avatar");
			public static readonly IHyperLink Red = new HyperLink("/content/img/avatar/good-red.png", "player-avatar");
		}
	}
}
