﻿/*global Rook, Game, UI, Graphics */
/// <reference path="lib/jquery.min.js" />
/// <reference path="lib/jquery.linq.js" />
/// <reference path="lib/linq.js" />
/// <reference path="lib/json2.js" />
/// <reference path="viewui.js" />
/// <reference path="helpers.js" />
/// <reference path="game.js" />

function showNonBoardMoves(game, context) {
	if (!game.moves) { return; }

	var player = game.player;
	var model = {
		moves: _(game.moves[player]).filter(function(m) { return m.title; })
	};

	context.
	render(context.templatePath('player-moves'), model).
	replace('.player-details').
	then(function() {
		context.$element('.moves .move-link').click(function (e) {
			e.preventDefault();
			Game.submitMove({ link: { href: $(this).attr('href') } });
		});
	});
}

function showPlayerStatus(game, context) {
	var gameState = game.state || game;
	var model = {
		gameOver: !gameState.currentPlayer,
		isCurrentPlayer: gameState.currentPlayer === game.player,
		currentPlayer: gameState.playersById[gameState.currentPlayer]
	};

	context.
	render(context.templatePath('player-status'), model).
	replace('.player-status');
}

function setupCanvas(canvas, game) {
	if (canvas.getContext) {
		var imageRepository = new Graphics.ImageRepository($('#board-image-repository'));
		var board = game.board;

		var rendererArgs = UI.getBoardRendererArgs ? UI.getBoardRendererArgs(board, imageRepository) : {
			rootObj: new Graphics.RectBoardRoot(board.getSize()),
			templates: {
				circle: new Graphics.CircleTemplate(),
				tile: new Graphics.SquareTileTemplate(),
				coordinates: new Graphics.RectBoardCoordinatesTemplate(),
				piece: new Graphics.PieceTemplate(imageRepository.get('.piece-set'))
			},
			coordinateSystem: new Graphics.BoardCoordinateSystem(board.getSize())
		};

		UI.boardRenderer = new Graphics.BoardRenderer(canvas, game.board, imageRepository, rendererArgs);

		$('#board-image-repository img').load(function () {
			UI.boardRenderer.repaint();
		});
	}
}

function startGame(game, context) {
	var gameState = game.state || game;
	setupCanvas($('#board_canvas').get(0), gameState);

	$('#board_canvas').unbind('mouseout').mouseout(function () {
		UI.boardRenderer.removeDecoration('mouse');
		UI.boardRenderer.repaint();
	});

	UI.boardRenderer.mousemove('.tile', function(hit) {
		UI.boardRenderer.decoration('mouse', {
			draw: function (ctx) {
				ctx.drawAt(hit.position, function() {
					ctx.drawTemplate('tile', 0.07);
					ctx.strokeStyle = 'rgba(73, 73, 255, 0.8)';
					ctx.stroke();
				});
			}
		});
		UI.boardRenderer.repaint();
	});

	showPlayerStatus(game, context);
	showNonBoardMoves(game, context);
	UI.boardRenderer.repaint();

	var startPlaying = function() {
		var refreshUI = function() {
			UI.displayMessage(Game.Controller.currentMode().instructions);
			UI.boardRenderer.decoration('controller', Game.Controller.createDecorationWithMoveHighlights());
			UI.boardRenderer.repaint();
		};

		var tryPlay = function() {
			var move = Game.Controller.getSelectedMove();

			if (move === null) {
				UI.displayMessage('Invalid move selected. Click again to clear.');
			} else {
				Game.submitMove(move);
			}
		};

		var isAutoPlayOn = function() {
			return $('#auto-play-checkbox').is(':checked');
		};

		var refreshAutoPlayUI = function() {
			var isOn = isAutoPlayOn();
			$('#auto-play-checkbox-label').text(isOn ? 'disable move auto-commit' : 'enable move auto-commit');
			$('#button-play-move').toggle(!isOn);
		};

		$('#auto-play-checkbox').attr('checked', Rook.Config.autoPlay() ? 'checked' : null);
		$('#auto-play-checkbox').change(function() {
			Rook.Config.autoPlay(isAutoPlayOn());

			refreshAutoPlayUI();
		});

		Game.Controller = new Rook.GameController(gameState.board, gameState.currentPlayer, game.moves);
		refreshUI();
		refreshAutoPlayUI();

		UI.boardRenderer.click('.tile', function(hit) {
			Game.Controller.click(hit.position.name);
			refreshUI();

			if (isAutoPlayOn() && Game.Controller.currentMode().isFinal) {
				UI.displayMessage('playing&hellip;');
				tryPlay();
            }
		});

		$('#button-play-move').click(function() { tryPlay(); });
	};
	startPlaying();
}
