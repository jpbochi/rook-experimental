﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using Rook.Api;
using Rook.Games.Amazons;
using Xunit;
using Xunit.Extensions;
using JpLabs.Extensions;

namespace Rook.Games.Tests.Amazons
{
	public class AmazonsGameModuleTests
	{
		private readonly Mother mother = new Mother();

		public static IEnumerable<object[]> TestBoards
		{
			get { return Mother.GetBoards().Select( x => new object[] { x } ); }
		}

		[Theory]
		[PropertyData("TestBoards")]
		void should_parse_and_dump_board(string boardDesc)
		{
			var board = mother.ParseBoard(boardDesc);

			Assert.Equal(boardDesc, Helper.DumpBoard(board));
		}

		[Fact]
		void should_create_initial_state()
		{
			var state = mother.GetNewState();
			
			VerifyInitialState(state);
		}

		[Fact]
		void board_has_no_duplicate_pieces()
		{
			//TODO: inject a stubbed random number generator in the game instead of trying several times
			Enumerable.Range(0, 42).ForEach(_ => {
				mother.GetNewState().Board.Pieces.Select(p => p.Id).ShouldHaveNoDuplicates();
			});
		}

		[Fact]
		void should_bind_to_AmazonsScoreCommand()
		{
			mother.Kernel.Get<IGameMove>(CommandType.UpdateScore.FullName).ShouldBeOfType<AmazonsScoreCommand>();
		}

		[Fact]
		void pieces_have_images_and_offset()
		{
			var state = mother.GetNewState();

			state.Board.Pieces.ById(AmazonsPieces.Arrow).GetDisplayImageOffset().ShouldBe(PieceImageSet.LightRook);
			state.Board.Pieces
				.Where(p => p.GetBanner() == ClassicPlayer.White)
				.Select(p => p.GetDisplayImageOffset())
				.ShouldContainSameAs(Enumerable.Range(0, 5).Select(_ => PieceImageSet.WhiteQueen))
			;
			state.Board.Pieces
				.Where(p => p.GetBanner() == ClassicPlayer.Black)
				.Select(p => p.GetDisplayImageOffset())
				.ShouldContainSameAs(Enumerable.Range(0, 5).Select(_ => PieceImageSet.BlackQueen))
			;
		}

		[Fact]
		void should_create_board_with_configured_size()
		{
			const ushort expectedSize = 10;

			mother.GetModule<AmazonsGameModule>().BoardSize = expectedSize;

			var board = mother.GetNewState().Board;

			string expectedBoard = (@"
				...A..A...
				..........
				..........
				A........A
				..........
				..........
				a........a
				..........
				..........
				...a..a..."
			).ClearLiteral();
			Assert.Equal(expectedBoard, Helper.DumpBoard(board));
		}

		internal static void VerifyInitialState(IGameState state)
		{
			VerifyAnyState(state);

			state.CurrentPlayerId.ShouldBe(ClassicPlayer.White);

			string expectedBoard = (@"
				..A.A..
				.......
				A.....A
				.......
				a.....a
				.......
				..a.a.."
			).ClearLiteral();
			Assert.Equal(expectedBoard, Helper.DumpBoard(state.Board));

			state.Board.PiecesAt(Tiles.MainBoard).ShouldEach( p => p.Class.ShouldBe(AmazonsPieces.Amazon) );
			state.Board.PiecesAt(Tiles.MainBoard).ShouldEach( p => p.GetArrowClass().ShouldBe(AmazonsPieces.Arrow) );
		}

		internal static void VerifyAnyState(IGameState state)
		{
			state.Players.Select( p => p.Id ).ShouldBeEqualInOrder(new []{ ClassicPlayer.White, ClassicPlayer.Black });

			state.Board
			.ShouldNotBeNull()
			.Region(Tiles.MainBoard)
			.ShouldContainSameAs(
				from c in Enumerable.Range(0, 7)
				from r in Enumerable.Range(0, 7)
				select TilePos.Create(c, r).ToTileName()
			);

			state.Board.PiecesAt(Tiles.MainBoard).Where(p => p.Class == AmazonsPieces.Amazon).ShouldEach( p => p.GetBanner().ShouldNotBeNull() );

			//Amazons and Arrows classes have not default Banner
			state.Board.Pieces.ById(AmazonsPieces.Arrow).ShouldNotBeNull().GetBanner().ShouldBeNull();
			state.Board.Pieces.ById(AmazonsPieces.Amazon).ShouldNotBeNull().GetBanner().ShouldBeNull();

			state.Board.Pieces.Where(p => p.Class == AmazonsPieces.Amazon).ShouldEach( p => p.GetArrowClass().ShouldBe(AmazonsPieces.Arrow) );
		}
	}
}
