﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Controllers;
using JpLabs.Extensions;
using Newtonsoft.Json.Linq;
using Rook.Api;

namespace Rook.MvcApp.Api
{
	public class GamesController : ApiController
	{
		public const string ControllerName = "games";

		readonly IGameRepository repository;
		readonly IGameResourceSerializer serializer;

		public GamesController() : this(new GameRepository(), new GameResourceSerializer(null)) {}

		public GamesController(IGameRepository repository, IGameResourceSerializer serializer)
		{
			this.repository = repository;
			this.serializer = serializer;
		}

		protected override void Initialize(HttpControllerContext controllerContext)
		{
			base.Initialize(controllerContext);

			this.serializer.Url = this.Url;
		}

		[HttpGet]
		public HttpResponseMessage Get(int gameId)
		{
			var game = repository.GetById(gameId);
			if (game == null) return Request.CreateResponse(HttpStatusCode.NotFound);

			var ifNoneMatch = Request.Headers.IfNoneMatch;
			var gameETag = game.State.ETag();
			if (ifNoneMatch.Contains(gameETag)) return Request.CreateResponse(HttpStatusCode.NotModified);

			return Request.CreateResponse(HttpStatusCode.OK, serializer.From(game.State)).Tap(resp =>
				SetETagCacheControl(resp, gameETag)
			);
		}

		[Authorize]
		[HttpGet][ActionName(ApiRelation.GameForPlayer)]
		public HttpResponseMessage GameForPlayer(int gameId, string playerId)
		{
			var game = repository.GetById(gameId);
			if (game == null) return Request.CreateResponse(HttpStatusCode.NotFound);
			var player = game.State.GetPlayerById(playerId);
			if (player == null) return Request.CreateResponse(HttpStatusCode.NotFound);

			var ifNoneMatch = Request.Headers.IfNoneMatch;
			var gameETag = game.State.ETag();
			if (ifNoneMatch.Contains(gameETag)) return Request.CreateResponse(HttpStatusCode.NotModified);

			if (!player.IsControlledBy(User.GetOpenIdIdentity())) return Request.CreateResponse(HttpStatusCode.Forbidden);

			return Request.CreateResponse(HttpStatusCode.OK, serializer.From(game.Machine, playerId)).Tap(resp =>
				SetETagCacheControl(resp, gameETag)
			);
		}

		[HttpGet][ActionName(ApiRelation.Move)]
		public HttpResponseMessage Move(int gameId, string moveHref)
		{
			var resp = new JObject {
				{ "gameId", gameId },
				{ "moveHref", moveHref }
			};
			return Request.CreateResponse(HttpStatusCode.OK, resp);
		}

		[HttpPost][ActionName(ApiRelation.Move)]
		public HttpResponseMessage PostMove(int gameId, string moveHref)
		{
			return Request.CreateResponse(HttpStatusCode.NotImplemented, "Not Implemented (yet)");
		}

		void SetETagCacheControl(HttpResponseMessage resp, EntityTagHeaderValue etag)
		{
			resp.Headers.CacheControl = new CacheControlHeaderValue{
				MaxAge = new TimeSpan(0, 0, 1), SharedMaxAge = TimeSpan.Zero
			};
			resp.Headers.ETag = etag;
		}
	}
}