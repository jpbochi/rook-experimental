﻿/// <reference path="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js" />
/// <reference path="board.js" />
"use strict";

var Rook = Rook || {};
Rook.Parser = (function() {
	function groupByKey(list, key, selector) {
		var hash = _(list).groupBy(key, selector);
		_.chain(hash).keys().each(function (k) {
			hash[k] = _(hash[k]).first();
		});
		return hash;
	}

	return {
		parseState: function (state) {
			return {
				version: state.version,
				currentPlayer: state.currentPlayer,
				players: state.players,
				playersById: this.parsePlayers(state.players),
				board: this.parseBoard(state)
			};
		},
		parseGame: function (game) {
			return {
				player: game.player,
				state: this.parseState(game.state),
				moves: this.parseMoves(game.moves)
			};
		},
		parsePlayers: function(players) {
			return groupByKey(
				players,
				function(p) { return p.id; },
				function(p) {
					var links = groupByKey(p.links, 'rel', 'href');
					return {
						score: p.score,
						name: p.name,
						links: links
					};
				}
			);
		},
		parseBoard: function (state) {
			return new Rook.Board(state.tiles, this.parsePieces(state.pieces));
		},
		parsePieces: function (pieces) {
			return $.map(pieces, this.parsePiece);
		},
		parsePiece: function (piece) {
			return new Rook.Piece(
				piece.id,
				piece.position,
				{ dx: piece.imagePos.x, dy: piece.imagePos.y }
			);
		},
		parseMoves: function (movesJson) {
			return _.chain(movesJson).map(function(m){
				var move = {
					link: {
						href: m.href,
						rel: m.rel
					}
				};
				m.data.PlayerId && (move.player = m.data.PlayerId);
				m.data.Origin && (move.origin = m.data.Origin);
				m.data.Destination && (move.destination = m.data.Destination);
				m.data.Title && (move.title = m.data.Title);
				return move;
			}).groupBy('player').value();
		}
	};
})();
