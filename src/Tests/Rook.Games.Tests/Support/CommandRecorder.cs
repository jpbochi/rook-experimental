﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using JpLabs.Symbols;

namespace Rook.Games.Tests
{
	internal class UriCommandRecorder : IGameReferee
	{
		public readonly Queue<Uri> Commands = new Queue<Uri>();
		private readonly IGameReferee referee;

		public UriCommandRecorder() : this(null) {}

		public UriCommandRecorder(IGameReferee referee)
		{
			this.referee = referee;
		}

		IEnumerable<IHyperLink> IGameReferee.GetMoves(IGameState state)
		{
			if (referee == null) throw new NotImplementedException();
			return referee.GetMoves(state);
		}

		IGameMove IGameReferee.FindMove(Uri moveUri, IGameState state)
		{
			if (referee == null) throw new NotImplementedException();
			return referee.FindMove(moveUri, state);
		}

		bool IGameReferee.ExecuteMove(Uri moveUri, IMoveExecutionContext context)
		{
			Commands.Enqueue(moveUri);
			return (referee == null) ? true : referee.ExecuteMove(moveUri, context);
		}
	}
}
