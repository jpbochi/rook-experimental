﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using JpLabs.Extensions;

namespace Rook.Tests
{
	public static class ProcessHelper
	{
		public static string SolutionDir { get; private set; }

		static ProcessHelper()
		{
			SolutionDir = GetSolutionDir();
		}

		static string GetSolutionDir()
		{
			using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SolutionDir.txt"))) {
				return reader.ReadLine().Trim();
			}
		}

		public static void TraceWriteWithCategory(this StreamReader reader, string category, Func<string, bool> includeLineFunc)
		{
			while (reader.Peek() != -1) {
				var line = reader.ReadLine();
				Trace.WriteLineIf(includeLineFunc(line), line, category);
			}
			reader.DiscardBufferedData();
		}
	}
}
