﻿using System;
using OpenQA.Selenium;

namespace Rook.Browser.Tests
{
    public class SeleniumBrowserFixture : IDisposable
	{
		public IWebDriver Driver { get; private set; }

		public SeleniumBrowserFixture()
		{ 
			Driver = new OpenQA.Selenium.Firefox.FirefoxDriver();
		}

		public void Dispose()
		{
			((IDisposable)Driver).Dispose();
		}
	}
}
