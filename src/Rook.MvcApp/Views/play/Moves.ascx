﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<IHyperLink>>" %>
<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="JpLabs.Extensions" %>

<span>You have <%: Model.Count() %>  moves available.</span>
<ul>
<% if (ViewContext.IsChildAction) foreach (var link in Model) { %>
	<li><%= link.ToHtmlAnchor() %></li>
<% } else {
	var gameId = ViewContext.RouteData.Values["gameId"].ToString();
	var linkTemplate = new UriTemplate("/play/moves/{id}/{*href}");
	var baseUri = new Uri("http://rook");
	
	foreach (var link in Model) { %>
		<%
		var anchor = link.ToHtmlAnchor();
		var href = anchor.Attribute("href").Value;
		href = linkTemplate.BindByPosition(baseUri, gameId, href.TrimStart('/')).PathAndQuery;
		anchor.Attribute("href").Value = href;
		%>
		<li><%= anchor %></li>
	<% } %>
<% } %>
</ul>
