﻿/// <reference path="../lib/linq.js" />
/// <reference path="boardCoordinateSystem.js" />

var Graphics = Graphics || {};

Graphics.RectBackgroundRenderer = function (backgroundImage, boardRenderer) {
	var consts = {
		Margin: 16,
		TileSize: 56,
		BoardBorderStyle: "rgb(102, 102, 102)"
	};

	this.zorder = -1;
	this.hasCustomTransformation = true;

	// Draws coordinate numbers and letters
	var drawCoordinates = function (ctx) {
		ctx.save();

		//ctx.scaleFromTileToPixel();
		//ctx.translate(-consts.Margin, -consts.Margin);

		ctx.fillStyle = "#666666";
		ctx.font = "bold 12px verdana,Arial,Helvetica,sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";

		var canvasHeight = ctx.canvas.height / boardRenderer.zoom;
		var tileSize = consts.TileSize;

		for (var i = 0; i < boardRenderer.board.cols; i++) {
			var left = consts.Margin + ((i + 0.5) * tileSize);
			var bottom = consts.Margin / 2.5;
			ctx.fillText(String.fromCharCode(97 + i), left, canvasHeight - bottom);

			//ctx.strokeRect(left - 14.5, canvasHeight - bottom - 3.5, 29, 7);
		}

		for (var i = 0; i < boardRenderer.board.rows; i++) {
			var left = consts.Margin / 2;
			var bottom = consts.Margin + ((i + 0.5) * tileSize);
			ctx.fillText((i + 1).toString(), left, canvasHeight - bottom);

			//ctx.strokeRect(left - 3.5, canvasHeight - bottom - 14.5, 7, 29);
		}
		ctx.restore();
	};

	this.draw = function (ctx) {
		ctx.save();

		ctx.translateToBoard();
		if (backgroundImage.complete) {
			ctx.fillStyle = ctx.createPattern(backgroundImage, "repeat");
			ctx.fillRect(0, 0, boardRenderer.board.cols * consts.TileSize, boardRenderer.board.rows * consts.TileSize);
		}

		ctx.scaleFromPixelToTile();
		var halfPixel = ctx.lineWidth / 2;
		ctx.strokeStyle = consts.BoardBorderStyle;
		ctx.strokeRect(-halfPixel, -halfPixel, boardRenderer.board.cols + 2 * halfPixel, boardRenderer.board.rows + 2 * halfPixel);

		ctx.restore();

		drawCoordinates(ctx);
	};
};

Graphics.RectBoardRenderer = function (canvas, board, pieceSetImg) {
	var consts = {
		Margin: 16,
		TileSize: 56,
		PieceTileSize: 56
	};

	this.canvas = canvas;
	this.board = board;
	this.zoom = 0.75;
	this.coordinates = new Graphics.BoardCoordinates(this.board);

	this.decorations = [];
	this.pieceSetImg = pieceSetImg;

	this.mouseToPosition = function (mouse) {
		//Mouse events can be handled in a much nicer way using the CanvasRenderingContext2D.isPointInPath(x, y) function
		// See this: http://www.html5canvastutorials.com/advanced/html5-canvas-image-mouseover/
		// Reference here: http://www.w3.org/TR/2010/WD-2dcontext-20100304/#dom-context-2d-ispointinpath

		return this.coordinates.pointToPosition({
			x: Math.floor(((mouse.x / this.zoom) - consts.Margin) / consts.TileSize),
			y: Math.floor(((mouse.y / this.zoom) - consts.Margin) / consts.TileSize)
		});
	};

	this.resetCanvas = function () {
		this.canvas.width = (consts.Margin * 2 + this.board.cols * consts.TileSize) * this.zoom;
		this.canvas.height = (consts.Margin * 2 + this.board.rows * consts.TileSize) * this.zoom;
	};

	this.getContext = function () {
		var ctx = this.canvas.getContext("2d");
		var that = this;

		ctx.canvas = canvas;
		ctx.lineWidth = 1.0;

		ctx.applyZoom = function () {
			ctx.scale(that.zoom, that.zoom);
		};

		ctx.translateToBoard = function () {
			ctx.translate(consts.Margin, consts.Margin);
		};

		ctx.scaleFromPixelToTile = function () {
			var scale = consts.TileSize;
			ctx.lineWidth /= scale;
			ctx.scale(scale, scale);
		};

		ctx.scaleFromTileToPixel = function () {
			var scale = 1 / consts.TileSize;
			ctx.scale(scale, scale);
		};

		ctx.translateFromBoardToTile = function (position) {
			position = that.coordinates.positionToPoint(position);
			ctx.translate(position.x, position.y);
		};

		ctx.drawAt = function (position, drawFunc) {
			ctx.save();
			ctx.translateFromBoardToTile(position);

			drawFunc();

			ctx.restore();
		};

		ctx.drawTile = function (lineWidth, lineJoin) {
			lineWidth = lineWidth || 1;
			lineJoin = lineJoin || "round";

			ctx.lineWidth *= lineWidth;
			ctx.lineJoin = lineJoin;

			var halfPixel = 0.5 * ctx.lineWidth;
			ctx.beginPath();
			ctx.rect(-halfPixel, -halfPixel, 1 + 2 * halfPixel, 1 + 2 * halfPixel);
			ctx.closePath();
		};

		return ctx;
	};

	this.drawPiece = function (ctx, piece) {
		if (!this.pieceSetImg.complete) return;

		ctx.save();

		ctx.translateFromBoardToTile(piece.position);

		ctx.drawImage(
			this.pieceSetImg,
			piece.img.dx * consts.PieceTileSize, piece.img.dy * consts.PieceTileSize, consts.PieceTileSize, consts.PieceTileSize,
			0, 0, 1, 1
		);

		ctx.restore();
	};

	this.drawDecoration = function (ctx, decoration) {
		if (!decoration) return;
		ctx.save();

		if (!decoration.hasCustomTransformation) {
			ctx.translateToBoard();
			ctx.scaleFromPixelToTile();
		}

		if (decoration.position) ctx.translateFromBoardToTile(decoration.position);
		decoration.draw(ctx);

		ctx.restore();
	};

	this.drawBoardContent = function (ctx) {
		ctx.save();
		ctx.applyZoom();

		var that = this;

		var getAllValues = function (obj) {
			var values = [];
			for (var key in obj) values.push(obj[key]);
			return values;
		};

		Enumerable.From(getAllValues(this.decorations)).OrderBy(function (dec) {
			return dec.zorder || 0;
		}).ForEach(function (dec) {
			that.drawDecoration(ctx, dec);
		});

		ctx.translateToBoard();
		ctx.scaleFromPixelToTile();

		Enumerable.From(this.board.pieces).OrderBy(function (piece) {
			return piece.zorder || 0;
		}).ForEach(function (piece) {
			that.drawPiece(ctx, piece);
		});

		ctx.restore();
	};

	this.drawBoard = function (ctx) {
		//Read this: http://diveintohtml5.org/canvas.html
		//Sduty this: http://www.html5canvastutorials.com/
		//Research here: http://www.w3.org/TR/2dcontext/
		//Cheat from here: http://simon.html5.org/dump/html5-canvas-cheat-sheet.html

		this.drawBoardContent(ctx);
	};

	this.repaint = function () {
		this.resetCanvas();
		this.drawBoard(this.getContext());
	};

	this.removeDecoration = function (key) {
		delete this.decorations[key];
	};

	this.clearDecorations = function () {
		this.decorations = [];
	};
};
