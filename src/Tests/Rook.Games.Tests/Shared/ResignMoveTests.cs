﻿using System;
using System.Linq;
using Rook.Games.Common;
using Xunit;

namespace Rook.Games.Tests.Common
{
	public class ResignMoveTests
	{
		[Fact]
		void should_pass_turn_and_update_score_when_current_player_resigns()
		{
			var state = RookTestHelper.CreateGameWithPlayers(3);
			var recorder = new UriCommandRecorder();

			var resigningPlayer = state.GetCurrentPlayer().Id;
			var move = new ResignMove(resigningPlayer);
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", resigningPlayer, GameScore.Resigned), UriKind.Relative),
				new Uri("/cycle-turn", UriKind.Relative),
			});
		}

		[Fact]
		void should_update_score_when_other_player_resigns()
		{
			var state = RookTestHelper.CreateGameWithPlayers(3);
			var recorder = new UriCommandRecorder();

			var resigningPlayer = state.Players.Last().Id;
			var move = new ResignMove(resigningPlayer);
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", resigningPlayer, GameScore.Resigned), UriKind.Relative),
			});
		}

		[Fact]
		void should_update_scores_and_end_game_when_last_player_resigns()
		{
			var state = RookTestHelper.CreateGameWithPlayers(3);
			var recorder = new UriCommandRecorder();

			var winningPlayer = state.Players.ElementAt(0).Id;
			var resigningPlayer = state.Players.ElementAt(1).Id;
			var deadPlayer = state.Players.ElementAt(2);
			deadPlayer.Score = GameScore.Lost;

			var move = new ResignMove(resigningPlayer);
			move.Execute(new MoveExecutionContext(state, recorder, null));

			recorder.Commands.ShouldBeEqualInOrder(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", resigningPlayer, GameScore.Resigned), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", winningPlayer, GameScore.Won), UriKind.Relative),
				new Uri("/set-game-over", UriKind.Relative),
			});
		}
	}
}
