﻿using System;

namespace Rook.Games.Amazons
{
	internal class AmazonsShootArrowMove : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return AmazonsShootArrowMove.Template; } }

			IGameMove IMoveBuilder.Build(UriTemplateMatch match, IGameState state)
			{
				return new AmazonsShootArrowMove(
					match.BoundVariables["playerid"],
					match.BoundVariables["piece"],
					match.BoundVariables["position"]
				);
			}
		}

		internal static readonly UriTemplate Template = new UriTemplate("/shoot/{playerid}/{piece}/{position}");
		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal const string Relation = RookRelations.Moves.DropPiece;

		public RName PlayerId { get; private set; }
		public RName Piece { get; private set; }
		public RName Position { get; private set; }

		public AmazonsShootArrowMove(RName playerId, RName pieceClass, RName position)
		{
			this.PlayerId = playerId;
			this.Piece = pieceClass;
			this.Position = position;
		}

		string IGameMove.RequiredRole { get { return PlayerId; } }

		IHyperLink IGameMove.Link
		{
			get {
				return HyperLink.From(Template, Relation, new { PlayerId, Piece, Position }, new { PlayerId, Destination = Position });
			}
		}

		public void Execute(IMoveExecutionContext context)
		{
			var cmds = new [] {
				HyperLink.BuildUri(CommandTemplates.AddPiece, new { Piece, Position }),
				HyperLink.BuildUri(CommandTemplates.ResetGameProperty, new { Property = AmazonsProperties.LastPieceToMoveProperty.ToRName() }),
				HyperLink.BuildUri(CommandTemplates.CycleTurn),
				HyperLink.BuildUri(AmazonsScoreCommand.Template)
			};

			foreach (var cmd in cmds) context.ExecuteMove(cmd);
		}
	}
}
