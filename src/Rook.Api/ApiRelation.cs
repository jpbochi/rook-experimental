﻿namespace Rook.Api
{
	// http://blog.whatwg.org/the-road-to-html-5-link-relations
	// http://microformats.org/wiki/existing-rel-values 
	// http://www.iana.org/assignments/link-relations/link-relations.xml
	public static class ApiRelation {
		public const string Game = "game";
		public const string GameForPlayer = "game-for-player";
		public const string Move = "move";
		public const string Avatar = "icon";
		public const string Join = "join";
	}
}