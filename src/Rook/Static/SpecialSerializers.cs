﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Rook.Serialization
{
	abstract class SpecialStringSerializer : BsonBaseSerializer
	{
		protected abstract object Deserialize(string value);

		protected virtual string Serialize(object value)
		{
			return value.ToString();
		}

		public override object Deserialize(MongoDB.Bson.IO.BsonReader bsonReader, Type nominalType, Type actualType, IBsonSerializationOptions options)
		{
			if (bsonReader.CurrentBsonType == BsonType.Null) {
				bsonReader.ReadNull();
				return null;
			}

			return Deserialize(bsonReader.ReadString());
		}

		public override void Serialize(MongoDB.Bson.IO.BsonWriter bsonWriter, Type nominalType, object value, IBsonSerializationOptions options)
		{
			if (value == null) {
				bsonWriter.WriteNull();
			} else {
				bsonWriter.WriteString(Serialize(value));
			}
		}
	}

	class XElementSerializer : SpecialStringSerializer
	{
		protected override object Deserialize(string value)
		{
			return XElement.Parse(value);
		}
	}

	//class UriSerializer : SpecialStringSerializer
	//{
	//	protected override object Deserialize(string value)
	//	{
	//		return new Uri(value, UriKind.RelativeOrAbsolute);
	//	}
	//}

	class RNameSerializer : SpecialStringSerializer
	{
		protected override object Deserialize(string value)
		{
			return RName.Get(value);
		}
	}

	class PointSerializer : SpecialStringSerializer
	{
		protected override string Serialize(object value)
		{
			var point = ((Point)value);
			return string.Concat(point.X, ";", point.Y);
		}

		protected override object Deserialize(string value)
		{
			var coords = value.Split(';');
			return new Point(int.Parse(coords[0]), int.Parse(coords[1]));
		}
	}
}
