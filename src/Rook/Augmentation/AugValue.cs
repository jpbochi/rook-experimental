﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Linq;

namespace Rook.Augmentation
{
	[ImmutableObject(true)] //Aspect: all IAugValues should be immutable
	public interface IAugValue
	{
		object GetValue(AugObject owner);
		
		//TODO: consider the following:
		//object GetLocalValue(AugObject owner); //without functionAugValues
		//IAugValue CombineValue(AugObject owner, IAugValue augValue);
	}
	
	public interface IXSerializable
	{
		XObject ToXml(XName name);
	}
	
	public static class AugValue
	{
		public static object GetDefaultValue(this Type type)
		{
			return (type.IsValueType) ? Activator.CreateInstance(type) : null;
		}

		public static T GetValue<T>(this IAugValue value, AugObject owner)
		{
			return (T)value.GetValue(owner);
		}
		
		public static IAugValue Create(object value, AugProperty prop)
		{
			//if (value is LambdaExpression)	return FunctionAugValue.Create((LambdaExpression)value, prop);
			//if (value is Delegate)			return FunctionAugValue.Create((Delegate)value, prop);
			return new ConstantAugValue(value);
		}
		
		public static XObject ToXml(this KeyValuePair<AugProperty,IAugValue> pair)
		{
			return pair.Key.ToXml(pair.Value);
		}

		public static KeyValuePair<AugProperty,IAugValue> FromXml(XObject x)
		{
			var attr = x as XAttribute;
			if (attr != null) {
				var prop = AugProperty.FromXName(attr.Name);

				var value = Create(prop.ConvertValue(attr.Value), prop);

				return new KeyValuePair<AugProperty,IAugValue>(prop, value);
			}

			var elem = x as XElement;
			if (elem != null) {
				var prop = AugProperty.FromXName(elem.Name);
			}

			throw new NotSupportedException();
		}
	}
	
	[ImmutableObject(true)]
	//[DataContract]
	internal sealed class ConstantAugValue : IAugValue, IXSerializable
	{
		//[DataMember]
		public object Value { get; private set; }

		public object GetValue(AugObject owner) { return Value; }
		
		public ConstantAugValue(object value) { Value = value; }
		
		static public IAugValue Create(object value)
		{
			return new ConstantAugValue(value);
		}

		public override bool Equals(object obj)
		{
			return (obj is ConstantAugValue) && Value == ((ConstantAugValue)obj).Value;
		}

		public override int GetHashCode()
		{
			return (Value == null) ? -1 : Value.GetHashCode();
		}

		public override string ToString()
		{
			return (Value == null) ? "{null}" : XUtil.ToString(Value);
		}
		
		XObject IXSerializable.ToXml(XName name)
		{
			object value = Value ?? "{null}";

			return XUtil.ToXml(name, value);

			//if (Value is AugObject) return new XElement(name, ((AugObject)Value).ToXml());
			//return new XAttribute(name, this.ToString());
		}
	}
}
