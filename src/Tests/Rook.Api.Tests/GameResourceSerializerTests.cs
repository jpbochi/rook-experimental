﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using JpLabs.Extensions;
using Moq;
using Newtonsoft.Json.Linq;
using Rook.Tests;
using Xunit;

namespace Rook.Api.Tests
{
	public class GameResourceSerializerTests
	{
		static UrlHelper FakeUrl()
		{
			var routes = Fake.Routes(r => {
				r.MapHttpRoute(ApiRelation.GameForPlayer, "api/games/{gameId}/for/{playerId}");
				r.MapHttpRoute(ApiRelation.Move, "api/games/{gameId}/move/{*moveHref}");
				r.MapHttpRoute(ApiRelation.Game, "api/games/{gameId}");
				r.MapHttpRoute(ApiRelation.Join, "api/to-be-implemented");
			});

			var request = Fake.Request(routes);
			request.GetRouteData().Values.Tap(values => {
				values.Add("gameId", 42);
			});

			return new UrlHelper(request);
		}

		public class from_public_game_state
		{
			[Fact]
			void should_output_version()
			{
				var state = new BasicGameState(null, null);
				state.Revise();

				var model = new GameResourceSerializer(null).From(state);

				model.ShouldBeOfType<JObject>();
				model["version"].ShouldBeOfType<JValue>().ToString().ShouldBe(state.Version);
			}

			[Fact]
			void should_output_current_player()
			{
				var state = new BasicGameState(null, null) { CurrentPlayerId = "mr_white" };

				var model = new GameResourceSerializer(null).From(state);

				model.ShouldBeOfType<JObject>();
				model["currentPlayer"].ShouldBeOfType<JValue>().ToString().ShouldBe("mr_white");
			}

			public class players
			{
				[Fact]
				void have_their_ids()
				{
					var state = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");

					var model = new GameResourceSerializer(FakeUrl()).From(state);

					var players = model["players"].ShouldBeOfType<IEnumerable<JToken>>();
					var ids = new JArray(players.Select(p => p["id"]));
					ids.ToString().ShouldBe(new JArray("batima", "robin", "coringa").ToString());
				}

				[Fact]
				void have_their_names()
				{
					var state = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");
					state.Players.ElementAt(0).SetValue(PlayerExt.NameProperty, "Adam West");
					state.Players.ElementAt(1).SetValue(PlayerExt.NameProperty, "Burt Ward");
					state.Players.ElementAt(2).SetValue(PlayerExt.NameProperty, "Cesar Romero");

					var model = new GameResourceSerializer(FakeUrl()).From(state);

					var players = model["players"].ShouldBeOfType<IEnumerable<JToken>>();
					var names = new JArray(players.Select(p => p["name"]));
					names.ToString().ShouldBe(new JArray("Adam West", "Burt Ward", "Cesar Romero").ToString());
				}

				[Fact]
				void have_their_scores()
				{
					var state = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");
					state.Players.ElementAt(2).SetValue(BasicPlayer.ScoreProperty, GameScore.Resigned);

					var model = new GameResourceSerializer(FakeUrl()).From(state);

					var players = model["players"].ShouldBeOfType<IEnumerable<JToken>>();
					var scores = new JArray(players.Select(p => p["score"]));
					scores.ToString().ShouldBe(new JArray(
						GameScore.StillPlaying.Name,
						GameScore.StillPlaying.Name,
						GameScore.Resigned.Name
					).ToString());
				}

				[Fact]
				void have_links()
				{
					var state = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");
					state.Players.ForEach(p => p.SetValue(PlayerExt.ControllerIdProperty, "person@example.org"));
					state.Id = "feiradafruta";

					var model = new GameResourceSerializer(FakeUrl()).From(state);

					var players = model["players"].ShouldBeOfType<IEnumerable<JToken>>();
					var links = new JArray(players.Select(p => p["links"]));
					links.ToString().ShouldBe(new JArray(
						new JArray(
							new JObject { { "rel", ApiRelation.GameForPlayer }, { "href", "/api/games/feiradafruta/for/batima" } }
						),
						new JArray(
							new JObject { { "rel", ApiRelation.GameForPlayer }, { "href", "/api/games/feiradafruta/for/robin" } }
						),
						new JArray(
							new JObject { { "rel", ApiRelation.GameForPlayer }, { "href", "/api/games/feiradafruta/for/coringa" } }
						)
					).ToString());
				}

				[Fact]
				void have_a_link_to_their_avatar()
				{
					var state = RookTestHelper.CreateGameWithPlayers("mr-black", "mr-white");
					state.Id = "dogs";
					state.Players.ElementAt(0).SetValue(PlayerExt.AvatarHrefProperty, "/content/img/playerblack.gif");
					state.Players.ElementAt(1).SetValue(PlayerExt.AvatarHrefProperty, "/content/img/playerwhite.gif");

					var model = new GameResourceSerializer(FakeUrl()).From(state);

					var players = model["players"].ShouldBeOfType<IEnumerable<JToken>>();
					var linksList = players
						.Select(p => p["links"])
						.Select(links => new JArray(links.Where(l => l["rel"].ToString() == ApiRelation.Avatar)));
					new JArray(linksList).ToString().ShouldBe(new JArray(
						new JArray(
							new JObject { { "rel", ApiRelation.Avatar }, { "href", "/content/img/playerblack.gif" } }
						),
						new JArray(
							new JObject { { "rel", ApiRelation.Avatar }, { "href", "/content/img/playerwhite.gif" } }
						)
					).ToString());
				}

				[Fact]
				void have_a_link_to_join_when_slot_is_open()
				{
					var state = RookTestHelper.CreateGameWithPlayers("challenger", "anyone");
					state.Players.First().SetValue(PlayerExt.ControllerIdProperty, "real-person@example.org");

					var model = new GameResourceSerializer(FakeUrl()).From(state);

					var players = model["players"].ShouldBeOfType<IEnumerable<JToken>>();
					var linksList = players
						.Select(p => p["links"])
						.Select(links => new JArray(links.Where(l => l["rel"].ToString() == ApiRelation.Join)));
					new JArray(linksList).ToString().ShouldBe(new JArray(
						new JArray(),
						new JArray(
							new JObject { { "rel", ApiRelation.Join }, { "href", "/api/to-be-implemented" } }
						)
					).ToString());
				}
			}

			public class board
			{
				[Fact]
				void has_list_of_tiles()
				{
					var board = new BasicBoard(null, new BasicTileGraph(new BasicTileSet(new RName[] { "a1", "a2", "b2", }), null));
					var state = new BasicGameState(null, board);

					var model = new GameResourceSerializer(null).From(state);

					model.ShouldBeOfType<JObject>();
					var tiles = model["tiles"].ShouldBeOfType<IEnumerable<JToken>>();
					tiles.ShouldContainSameAs(new []{
						new JObject {
							{ "name", "a1" },
							{ "position", new JObject { { "x", 0 }, { "y", 0 } } }
						},
						new JObject {
							{ "name", "a2" },
							{ "position", new JObject { { "x", 0 }, { "y", 1 } } }
						},
						new JObject {
							{ "name", "b2" },
							{ "position", new JObject { { "x", 1 }, { "y", 1 } } }
						}
					}, JToken.EqualityComparer);
				}

				[Fact]
				void has_list_of_pieces_on_board()
				{
					var pieces = new BasicPieceContainer {
						{ new BasicPiece("king"), "e1" },
						{ new BasicPiece("bishop"), "c1" },
						{ new BasicPiece("rook"), "a1" }
					};
					var board = new BasicBoard(pieces, null);
					var state = new BasicGameState(null, board);
					var model = new GameResourceSerializer(null).From(state);

					model.ShouldBeOfType<JObject>();
					var tiles = model["pieces"].ShouldBeOfType<IEnumerable<JToken>>();
					tiles.ShouldContainSameAs(new []{
						new JObject {
							{ "id", "king" },
							{ "position", "e1" },
							{ "imagePos", new JObject { { "x", 7 }, { "y", 1 } } }
						},
						new JObject {
							{ "id", "bishop" },
							{ "position", "c1" },
							{ "imagePos", new JObject { { "x", 7 }, { "y", 1 } } }
						},
						new JObject {
							{ "id", "rook" },
							{ "position", "a1" },
							{ "imagePos", new JObject { { "x", 7 }, { "y", 1 } } }
						},
					}, JToken.EqualityComparer);
				}
			}
		}

		public class from_machine_and_player
		{
			[Fact]
			void should_output_requested_player()
			{
				var mockMachine = new Mock<IGameMachine>();
				mockMachine.Setup(m => m.State).Returns(new BasicGameState(null, null));

				var model = new GameResourceSerializer(FakeUrl()).From(mockMachine.Object, "le-player");

				model["player"].ShouldBeOfType<JValue>().ToString().ShouldBe("le-player");
			}

			[Fact]
			void should_output_game_state()
			{
				var state = new BasicGameState(null, null) { CurrentPlayerId = "ze_player" };
				var mockMachine = new Mock<IGameMachine>();
				mockMachine.Setup(m => m.State).Returns(state);

				var model = new GameResourceSerializer(null).From(mockMachine.Object, "ze_player");

				model.ShouldBeOfType<JObject>();
				var stateModel = model["state"].ShouldBeOfType<JObject>();
				stateModel["currentPlayer"].ShouldBeOfType<JValue>().ToString().ShouldBe("ze_player");
			}

			[Fact]
			void should_output_moves_for_selected_player_from_machine()
			{
				RName playerOne = "one";
				RName playerTwo = "two";
				 
				var mockMachine = new Mock<IGameMachine>();
				mockMachine.Setup(m => m.State).Returns(new BasicGameState(null, null));
				mockMachine.Setup(m => m.GetMoveOptions()).Returns(new [] {
					HyperLink.From(new UriTemplate("/doit/better"), "/test/daft", null, new { playerid = playerOne, title = "Better" }),
					HyperLink.From(new UriTemplate("/doit/harder"), "/test/daft", null, new { playerid = playerOne, title = "Harder" }),
					HyperLink.From(new UriTemplate("/celebrate"), "/test/onemoretime", null, new { playerid = playerTwo }),
				});

				var model = new GameResourceSerializer(FakeUrl()).From(mockMachine.Object, playerOne);

				model.ShouldBeOfType<JObject>();
				var moves = model["moves"].ShouldBeOfType<IEnumerable<JToken>>();
				moves.ShouldContainSameAs(new []{
					new JObject {
						{ "href", "/api/games/42/move//doit/better" }, // * = look below
						{ "rel", "/test/daft" },
						{ "data", new JObject {
							{ "playerid", "one" },
							{ "title", "Better" },
						}},
					},
					new JObject {
						{ "href", "/api/games/42/move//doit/harder" },
						{ "rel", "/test/daft" },
						{ "data", new JObject {
							{ "playerid", "one" },
							{ "title", "Harder" },
						}},
					},
				}, JToken.EqualityComparer);

				// * = the double '/' after 'move' is caused by a bug in System.Web.Http.Routing.HttpRoute
				// The production code uses this internal class System.Web.Http.WebHost.Routing.HostedHttpRoute
				// I'll let the wrong expectation there until I find a proper way to fake the routes
			}
		}
	}
}
