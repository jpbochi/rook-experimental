﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;
using JpLabs.Extensions;

namespace Rook.Browser.Tests
{
	public class IISExpressRunner : TestClassCommand, ITestClassCommand
	{
		public static IISExpressProcess IISProcess { get; private set; }

		public bool CanRunTests()
		{
			return Environment.UserInteractive;
		}

		public new Exception ClassStart()
		{
			var baseEx = base.ClassStart();
			if (baseEx != null) return baseEx;

			if (!CanRunTests()) return null;
			try {
				var args = base.TypeUnderTest.Type.GetSingleAttr<IISParamsAttribute>(true);
				IISProcess = new IISExpressProcess(args.RelativeAppPath, args.Port);
				return null;
			} catch (Exception ex) {
				return ex;
			}
		}

		public new Exception ClassFinish()
		{
			var baseEx = base.ClassFinish();
			if (baseEx != null) return baseEx;

			if (!CanRunTests()) return null;
			try {
				if (IISProcess != null) IISProcess.Dispose();
				IISProcess = null;
				return null;
			} catch (Exception ex) {
				return ex;
			}
		}

		public new IEnumerable<IMethodInfo> EnumerateTestMethods()
		{
			if (!CanRunTests()) return Enumerable.Empty<IMethodInfo>();

			return base.EnumerateTestMethods();
		}
	}
}
