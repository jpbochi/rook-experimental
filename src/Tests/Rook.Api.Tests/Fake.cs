﻿using System;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using JpLabs.Extensions;
using Rook.Api;

namespace Rook.Tests
{
	public static class Fake
	{
		public static IPrincipal User(string userName = "Bruce Lee")
		{
			return new GenericPrincipal(new RookIdentity(RookIdentity.GenerateTicket(userName)), null);
		}

		public static HttpRouteCollection Routes(Action<HttpRouteCollection> setup)
		{
			return new HttpRouteCollection().Tap(setup);
		}

		public static HttpRequestMessage Request(HttpRouteCollection routes)
		{
			var config = new HttpConfiguration(routes);
			var routeData = new HttpRouteData(new HttpRoute());

			var request = new System.Net.Http.HttpRequestMessage(HttpMethod.Get, "/");
			request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
			request.Properties[HttpPropertyKeys.HttpRouteDataKey] = routeData;

			return request;
		}
	}
}
