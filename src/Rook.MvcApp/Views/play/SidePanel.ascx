﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rook.MvcApp.Models.GameEntryViewModel>" %>

<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.Api" %>
<%@ Import Namespace="Rook.MvcApp" %>
<%@ Import Namespace="Rook.MvcApp.Controllers" %>

<%
var game = Model.Entry;
var machine = game.Machine;
var currentPlayer = game.State.GetCurrentPlayer();
%>

<div class="side-panel">
	<div class="player-status"></div>
	<div class="winner-status">
		<ul class="info-list">
		<% var winner = game.State.Players.Where(p => p.Score == GameScore.Won).FirstOrDefault(); %>
		<% if (winner != null) { %>
			<li class="winner-player">
				<span><%= winner.GetHtmlName() %> (<%: winner.Id.Name %>) <span class="score">Won</span></span>
			</li>
		<% } %>
		</ul>
	</div>
	<div class="player-details"></div>
</div>