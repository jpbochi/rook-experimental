﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using JpLabs.Extensions;
using JpLabs.Geometry;

namespace Rook
{
	[DataContract(Namespace=RookDataContracts.Namespace)]
	[KnownType(typeof(VectorDirection))]
	public class VectorBasedLinkSet : IGraphLinkSet
	{
		[DataMember]
		private ICollection<VectorDirection> directions;
		
		private VectorBasedLinkSet(IEnumerable<VectorDirection> directions)
		{
			this.directions = directions.ToReadOnlyColl();
		}

		public static IGraphLinkSet CreateCardinal()
		{
			return new VectorBasedLinkSet(new [] {
				new VectorDirection("S", new VectorInt(0, -1)),		new VectorDirection("N", new VectorInt(0, 1)),
				new VectorDirection("W", new VectorInt(-1, 0)),		new VectorDirection("E", new VectorInt(1, 0)),
			});
		}

		public static IGraphLinkSet CreateCardinalPlusIntercardinal()
		{
			return new VectorBasedLinkSet(new [] {
				new VectorDirection("S", new VectorInt(0, -1)),		new VectorDirection("N", new VectorInt(0, 1)),
				new VectorDirection("W", new VectorInt(-1, 0)),		new VectorDirection("E", new VectorInt(1, 0)),
				new VectorDirection("SW", new VectorInt(-1, -1)),	new VectorDirection("NE", new VectorInt(1, 1)),
				new VectorDirection("NW", new VectorInt(-1, 1)),	new VectorDirection("SE", new VectorInt(1, -1)),
			});
		}

		public IEnumerable<IGraphLink> GetOutLinks(RName tile)
		{
			var position = tile.ToPointInt();
			if (position == null) yield break;
			
			foreach (var direction in directions) {
				var target = direction.Transform(position.Value).ToTileName();
			
				yield return new BasicBoardLink(direction.Name, tile, target);
			}
		}
	}

	[DataContract(Namespace=RookDataContracts.Namespace)]
	internal class VectorDirection
	{
		[DataMember]public string Name { get; private set; }
		[IgnoreDataMember]public VectorInt Vector { get; private set; }

		[DataMember(Name="Vector")]
		private string SerializableVector
		{
			get { return Vector.ToString(); }
			set { Vector = VectorInt.Parse(value); }
		}

		public VectorDirection(string name, VectorInt vector)
		{
			this.Name = name;
			this.Vector = vector;
		}

		public PointInt Transform(PointInt origin)
		{
			return origin + Vector;
		}
	}
}
