﻿using System.Collections.Generic;

namespace Rook
{
	/// <summary>
	/// Represents an frozen game state.
	/// The rules necessary to say what moves are availlable are kept in a Referee.
	/// It must be clonable and serializable
	/// </summary>
	public interface IGameState : IAugObject
	{
		string Version { get; }
		IEnumerable<IGamePlayer> Players { get; }
		IGameBoard Board { get; }

		string Id { get; set; }
		RName CurrentPlayerId { get; set; }

		void Revise();

		//Extended properties candidates: GameCreationArguments, IsStarted
	}

	public interface IGamePlayer : IAugObject
	{
		RName Id { get; }
		RName Score { get; set; }

		//Extended properties candidates: Name; HasResigned; DisplayImage; Team; RemoteUri; Power;
	}

	public interface IGameBoard
	{
		/// TODO:
		/// Represent boards as directed multigraphs (http://en.wikipedia.org/wiki/Multigraph)
		///   where links have labels (ex.: "n, cardinal" or "nw, intercardinal")
		///   with a map (i.e., a functor) to a N-dimension discrete world
		///   and/or another map to a N-dimension continuous world.
		/// These functors may be useful to calculate distances and directions.
		/// All functors should have a reverse functor.
		/// 
		/// Grids (square-based or hex-based) are subtypes of this generic view.
		///  
		/// Merge PieceClass into Piece, and PieceClassDomain into PieceContainer
		///		a class is nothing but a Piece that's not on the board
		///		a Piece can be extended by any other Piece (but, usually, it will be a class)
		///		a class is identified by something like :Games.Chess:Rook
		///		a Piece is identified by something like :Games.Chess:Rook+3
		///		a Piece that isn't not on a board yet is identified by something like :Games.Chess:Rook
		///		a PieceContainer that can contain pieces with a 'null' position, can work as a PieceClassDomain
		///	
		/// use System.Web.HttpUtility
		/// GET (to state server):
		///	/players
		///		/player/white
		///		/player/current
		/// /pieces
		///		/piece/rook
		///		/piece/white-rook
		///		/piece/bishop:wq
		///	/classes
		///		/class/queen
		///		/class/black-queen
		///	/regions
		///		/region/rank-8 (contain links to each tile, and to each piece there)
		///		/region/file-h
		///	/tiles
		///		/tile/a1 (contain links to connected tiles)
		///	/game/history
		///		/game/history/last-move
		///		/game/history/1.w
		///	/images/large-background
		///	/scripts/board-renderer.js
		///	
		///	PUT (from referee to state server):
		///	/player/set-current/white
		///	/piece/move/rook:bk/c8
		///	/piece/add/x-mark/b2
		///	/piece/move/queen:w/captured-by-black
		/// /game/over
		/// 
		/// POST /game/history (pushes a new move to the history)
		///	
		/// GET /moves (to referee) links to POSTs:
		/// /resign
		/// /shoot/arrow/d5
		/// /slide/
		/// 
		/// http://msdn.microsoft.com/en-us/library/system.uritemplate.aspx
		/// http://msdn.microsoft.com/en-us/library/system.uritemplatetable.aspx
		/// http://msdn.microsoft.com/en-us/library/bb675245.aspx (UriTemplate and UriTemplateTable)
		///	

		IEnumerable<IGamePiece> PiecesAt(RName tileOrRegion);	//GET /pieces/at/{tile}
		IEnumerable<IGamePiece> Classes { get; }				//GET /pieces/abstract/all
		IEnumerable<RName> Region(RName region);				//GET /tile/{tile}/region

		IPieceContainer Pieces { get; }
		IGameTileGraph Graph { get; }

		//POST /piece
		IGamePiece AddPiece(IGamePiece piece, RName posistion);
		//PUT /piece/{id}
		IGamePiece MovePiece(IGamePiece piece, RName destination);
	}

	public interface IPieceContainer : IEnumerable<IGamePiece>
	{
		//GET /piece/{id}
		IGamePiece ById(RName id);
		
		//POST /piece
		IGamePiece Add(IGamePiece piece);
		//POST /piece/{tile}
		IGamePiece Add(IGamePiece piece, RName position);
		//PUT /piece
		IGamePiece Move(IGamePiece piece, RName destination);
	}

	public interface IGameTileGraph
	{
		//GET /tile/{tile}
		bool Contains(RName tile);
		//GET /tile/{tile}/region
		IEnumerable<RName> Region(RName region);

		//GET /tile/{tile}/links
		IEnumerable<IGraphLink> GetOutLinks(RName tileName);
		
		//Under review:
		//JpLabs.Geometry.PointInt? GetIntCoordinates(RName tileName);
		//JpLabs.Geometry.PointReal GetCenterCoordinates();
		//IBoardTile At(JpLabs.Geometry.PointInt coordinates);
		//IBoardTile At(JpLabs.Geometry.PointReal coordinates);
	}

	public interface IGraphTileSet
	{
		bool Contains(RName tile);
		IEnumerable<RName> GetAllTiles();
	}

	public interface IGraphLinkSet
	{
		IEnumerable<IGraphLink> GetOutLinks(RName tileName);
	}

	public interface IGraphLink
	{
		string Label { get; }
		RName Origin { get; }
		RName Target { get; }

		//Under review:
		//IBoardLink Invert();
		//IBoardLink TurnRight();
		//IBoardLink TurnLeft();
	}

	public interface IGamePiece : IAugObject
	{
		RName Id { get; }
		RName BasePieceId { get; }
		RName Class { get; }
		RName Position { get; }
	
		void ResolveReferencesAt(IPieceContainer container);
		void SetPosition(RName position);

		IGamePiece New();
	}

	public interface IAugObject
	{
		object GetValue(Augmentation.AugProperty property);
		void SetValue(Augmentation.AugProperty property, object value);

		bool IsValueUnset(Augmentation.AugProperty property);
		void ClearValue(Augmentation.AugProperty property);
	}
}
