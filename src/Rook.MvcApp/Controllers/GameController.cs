﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using JpLabs.Extensions;
using Ninject.Modules;
using Rook.Api;
using Rook.Games;
using Rook.MvcApp.Models;

namespace Rook.MvcApp.Controllers
{
	public class GameController : Controller
	{
		private readonly IGameRepository repository;
		private readonly IGameLoader gameLoader;
		private readonly Random rnd;
		private readonly IModuleLocator moduleLocator;

		public GameController() : this(new GameRepository(), new GameLoader(), GameModuleLocator.FromCurrentDomain()) {}

		public GameController(IGameRepository repository, IGameLoader gameLoader, IModuleLocator gameModuleLocator, Random rnd = null)
		{
			this.repository = repository;
			this.gameLoader = gameLoader;
			this.rnd = rnd ?? new Random();
			this.moduleLocator = gameModuleLocator;
		}

		[HttpGet][Authorize]
		public ActionResult Types()
		{
			return View(
				moduleLocator
				.FindModuleTypes()
				.Select(t => new KeyValuePair<string,Type>(moduleLocator.GetTypeName(t), t))
			);
		}

		[HttpGet][Authorize]
		public ActionResult New(string id)
		{
			if (string.IsNullOrWhiteSpace(id)) return RedirectToAction("types");

			var gameParams = new NewGameOptionsViewModel {
				GameTypeName = id,
				PlayersToControl = PlayersToControl.Random
			};

			var gameType = moduleLocator.FindType(gameParams.GameTypeName);

			gameParams.Options.AddRange(GetOptionsForCreatingGameType(gameType));

			return View(gameParams);
		}

		IEnumerable<OptionViewModel> GetOptionsForCreatingGameType(Type gameType)
		{
			var initializers = moduleLocator.FindInitializerTypesFor(gameType);
			foreach (var initializer in initializers) {
				foreach (var prop in initializer.GetProperties()) {
					var defaultValueAttr = prop.GetSingleAttrOrNull<DefaultValueAttribute>(false);
					var value = (defaultValueAttr == null) ? null : defaultValueAttr.Value.ToString();

					var displayNameAttr = prop.GetSingleAttrOrNull<DisplayNameAttribute>(false);
					var displayName = (displayNameAttr == null) ? prop.Name : displayNameAttr.DisplayName;

					yield return new OptionViewModel {
						InitializerType = moduleLocator.GetTypeName(initializer),
						PropertyName = prop.Name,
						DisplayName = displayName,
						Value = value
					};
				}
			}
		}

		[HttpPost][Authorize]
		public ActionResult New(NewGameOptionsViewModel gameParams)
		{
			if (!ModelState.IsValid) return View(gameParams);

			var initializers = gameParams.GetInitializers(moduleLocator).ToArray();

			//http://stackoverflow.com/questions/1755340/validate-data-using-dataannotations-with-wpf-entity-framework/2467387#2467387
			var results = new List<ValidationResult>();
			initializers.ForEach(initializer => {
				var context = new ValidationContext(initializer, null, null);
				Validator.TryValidateObject(initializer, context, results, true);
			});

			results.ForEach(r => ModelState.AddModelError(r.MemberNames.First(), r.ErrorMessage));
			if (!ModelState.IsValid) return View(gameParams);

			gameParams.CreatorIdentity = User.GetOpenIdIdentity();

			var gameType = moduleLocator.FindType(gameParams.GameTypeName);

			var game = gameLoader
				.CreateContext()
				.WithInitializers(initializers)
				.WithModules(gameType.ToEnumerable())
				.CreateGameMachine()
			;

			SetupPlayersControlledByCreator(game.State, gameParams);

			var entry = GameEntry.CreateNew(game, gameParams, gameType);

			var id = repository.Add(entry);

			return RedirectToGame(id);
		}

		[HttpGet]
		public ActionResult List()
		{
			return View(repository.GetAll().Select(g => new GameEntryViewModel(g)).Where(g => g.Entry.IsValid));
		}

		[HttpGet]
		public ActionResult ListDebug()
		{
			return View("list", repository.GetAll().Select(g => new GameEntryViewModel(g)));
		}

		private RedirectToRouteResult RedirectToGame(int id, string error = null)
		{
			var routeValues = (error == null) ? new { id } : (object)new { id, error };

			return RedirectToAction(PlayController.PlayActionName, PlayController.ControllerName, routeValues);
		}

		private void SetupPlayersControlledByCreator(IGameState state, NewGameOptionsViewModel gameParams)
		{
			GetPlayersToBeControllerByUser(state, gameParams.PlayersToControl).ForEach(p => p.SetController(gameParams.CreatorIdentity));
		}

		private IEnumerable<IGamePlayer> GetPlayersToBeControllerByUser(IGameState state, PlayersToControl playersToControl)
		{
			if (playersToControl == PlayersToControl.Hotseat) return state.Players;

			int position
				=  (playersToControl == PlayersToControl.Random)
				? rnd.Next(0, state.Players.Count())
				: (int)playersToControl
			;

			return state.Players.ElementAt(position).ToEnumerable();
		}
	}
}
