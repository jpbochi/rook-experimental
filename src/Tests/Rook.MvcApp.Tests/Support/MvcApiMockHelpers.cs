﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using Moq;

namespace Rook.MvcApp.Tests
{
	public static class MvcApiMockHelpers
	{
		public static T WithFakeApiContext<T>(this T ctrl) where T : ApiController
		{
			ctrl.Configuration = new HttpConfiguration();
			ctrl.Request = new HttpRequestMessage();
			ctrl.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, ctrl.Configuration);
			Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(""), new string[0]);
			return ctrl;
		}

		public static T WithFakeApiUser<T>(this T ctrl, IPrincipal user) where T : ApiController
		{
			Thread.CurrentPrincipal = user;
			return ctrl;
		}
	}
}
