﻿/*global Enumerable */
/// <reference path="../lib/linq.js" />
"use strict";

var Rook = Rook || {};

Rook.Piece = function (id, position, img) {
	this.id = id;
	this.position = position;
	this.img = img;

	this.originalposition = position;

	this.move = function (destination) {
		this.position = destination || this.originalposition;
	};
};

Rook.Piece.prototype.setZorder = function (zorder) {
	this.zorder = zorder;
};

Rook.Piece.prototype.resetZorder = function () {
	delete this.zorder;
};

Rook.Board = function (tiles, pieces) {
	tiles = tiles || [];
	pieces = pieces || [];

	var tileDict = {};
	Enumerable.From(tiles).ForEach(function (tile) {
		tileDict[tile.name] = tile;
	});

	var allTiles = Enumerable.From(tiles).Where(function (tile) { return tile.position && tile.position.x && tile.position.y; });
	var size = allTiles.Any() ? {
		cols: 1 + allTiles.Max(function (tile) { return tile.position.x; }),
		rows: 1 + allTiles.Max(function (tile) { return tile.position.y; })
	} : {
		cols: 0,
		rows: 0
	};

	return {
		getSize: function() {
			return size;
		},
		hasTile: function (tileName) {
			return tileDict.hasOwnProperty(tileName);
		},
		getTilePosition: function (tileName) {
			return tileDict.hasOwnProperty(tileName) && tileDict[tileName].position;
		},
		getPieceById: function (pieceId) {
			return Enumerable.From(pieces).FirstOrDefault(null, function (piece) {
				return pieceId === piece.id;
			});
		},
		getPiecesAt: function (tileName) {
			return Enumerable.From(pieces).Where(function (piece) {
				return tileName === piece.position;
			}).ToArray();
		},
		getAllPieces: function () {
			return pieces.copy();
		},
		getAllTiles: function () {
			return tiles.copy();
		}
	};
};
