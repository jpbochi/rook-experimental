﻿using System.Net;
using System.Net.Http;
using System.Security.Principal;
using Moq;
using Newtonsoft.Json.Linq;
using Rook.Api;
using Rook.MvcApp.Api;
using Rook.MvcApp.Models;
using Rook.Tests;
using Xunit;

namespace Rook.MvcApp.Tests
{
	public class GamesApiControllerTests
	{
		const int gameId = 42;
		readonly Mock<IGameRepository> mockRepository;
		readonly Mock<IGameResourceSerializer> mockSerializer;
		readonly IGameState state;
		readonly IGameMachine machine;
		readonly GamesController controller;

		public GamesApiControllerTests()
		{
			this.mockRepository = new Mock<IGameRepository>();
			this.mockSerializer = new Mock<IGameResourceSerializer>();

			this.state = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");
			this.machine = state.WithReferee();

			var creator = "http://rook/maester";
			var entry = GameEntry.CreateNew(machine, creator, null, null);
			this.mockRepository.Setup(r => r.GetById(gameId)).Returns(entry);

			this.controller = new GamesController(mockRepository.Object, mockSerializer.Object).WithFakeApiContext();
		}

		public class when_getting_public_game : GamesApiControllerTests
		{
			[Fact]
			void should_return_404_if_game_not_found()
			{
				mockRepository.Setup(r => r.GetById(404)).Returns((GameEntry)null);

				controller.Get(404).StatusCode.ShouldBe(HttpStatusCode.NotFound);
			}

			[Fact]
			void should_return_serialized_game_state()
			{
				var expectedContent = new JObject{{ "mocked", "true" }};
				mockSerializer.Setup(s => s.From(state)).Returns(expectedContent);

				var resp = controller.Get(gameId); 

				resp.StatusCode.ShouldBe(HttpStatusCode.OK);
				resp.Content.Headers.ContentType.MediaType.ShouldBe("application/json");
				JToken content;
				resp.TryGetContentValue(out content).ShouldBeTrue();
				content.ShouldBe(expectedContent);
			}
		}

		public class when_getting_game_for_user : GamesApiControllerTests
		{
			private IPrincipal user;

			public when_getting_game_for_user() : base()
			{
				this.user = Fake.User("TiaDoBatima");
				controller.WithFakeApiUser(user);
			}

			[Fact]
			void should_return_404_when_game_not_found()
			{
				var resp = this.controller.GameForPlayer(666, "batima");

				resp.StatusCode.ShouldBe(HttpStatusCode.NotFound);
			}

			[Fact]
			void should_return_404_when_player_not_found()
			{
				var resp = this.controller.GameForPlayer(gameId, "comissario");

				resp.StatusCode.ShouldBe(HttpStatusCode.NotFound);
			}

			[Fact]
			void should_return_serialized_game_machine_for_authorized_user()
			{
				var playingAs = "coringa";
				state.GetPlayerById("coringa").SetController(user.GetOpenIdIdentity());
				mockSerializer.Setup(s => s.From(machine, playingAs)).Returns(new JObject { { "game_for", "coringa" } });

				var resp = this.controller.GameForPlayer(gameId, playingAs);

				resp.StatusCode.ShouldBe(HttpStatusCode.OK);
				resp.Content.Headers.ContentType.MediaType.ShouldBe("application/json");
				JToken json;
				resp.TryGetContentValue(out json).ShouldBeTrue();
				json.ShouldBe(new JObject { { "game_for", "coringa" } });
			}

			[Fact]
			void should_return_Forbidden_if_user_does_not_control_player()
			{
				state.GetPlayerById("robin").SetController(Fake.User("Clotilde").GetOpenIdIdentity());
				
				var resp = controller.GameForPlayer(gameId, "robin");

				resp.StatusCode.ShouldBe(HttpStatusCode.Forbidden);
			}

			[Fact]
			void should_return_not_modified_when_ETag_mathed_current()
			{
				state.GetPlayerById("batima").SetController(user.GetOpenIdIdentity());
				state.Revise();
				controller.Request.Headers.IfNoneMatch.Add(state.ETag());

				var resp = controller.GameForPlayer(gameId, "batima");

				resp.StatusCode.ShouldBe(HttpStatusCode.NotModified);
			}

			[Fact]
			void should_enable_cache_and_send_current_ETag_of_game()
			{
				state.GetPlayerById("robin").SetController(user.GetOpenIdIdentity());
				state.Revise();
				var expectedETag = state.ETag();

				var resp = controller.GameForPlayer(gameId, "robin");

				resp.Headers.ETag.ShouldBe(expectedETag);
				resp.Headers.CacheControl.ToString().ShouldBe("max-age=1, s-maxage=0");
			}
		}
	}
}
