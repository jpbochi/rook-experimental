﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace JpLabs.Extensions
{
	public class MultiValueDictionary<TKey, TValue> : ILookup<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>
	{
		Dictionary<TKey,HashSet<TValue>> sets;
		IEqualityComparer<TValue> valueComparer;

		public MultiValueDictionary() : this(null, null) {}

		public MultiValueDictionary(IEqualityComparer<TKey> keyComparer) : this(keyComparer, null) {}

		public MultiValueDictionary(IEqualityComparer<TKey> keyComparer, IEqualityComparer<TValue> valueComparer)
		{
			keyComparer = keyComparer ?? EqualityComparer<TKey>.Default;
			this.valueComparer = valueComparer ?? EqualityComparer<TValue>.Default;
			this.sets = new Dictionary<TKey,HashSet<TValue>>(keyComparer);
		}

		private HashSet<TValue> CreateSet()
		{
			return new HashSet<TValue>(valueComparer);
		}

		private HashSet<TValue> GetOrCreateSet(TKey key)
		{
			HashSet<TValue> set;
			if (sets.TryGetValue(key, out set)) return set;
			
			set = CreateSet();
			sets.Add(key, set);
			return set;
		}

		public IEnumerable<TValue> this[TKey key]
		{
			get { return GetValues(key); }
		}

		public void Add(TKey key, TValue value)
		{
			if (key == null) throw new ArgumentNullException("key");

			var set = GetOrCreateSet(key);
			set.Add(value);
		}

		public void AddRange(TKey key, IEnumerable<TValue> values)
		{
			if (values == null) throw new ArgumentNullException("values");

			var set = GetOrCreateSet(key);
			foreach (TValue value in values) set.Add(value);
		}

		public bool ContainsKey(TKey key)
		{
			return sets.ContainsKey(key);
		}

		public bool Contains(TKey key, TValue value)
		{
			if (key == null) throw new ArgumentNullException("key");

			HashSet<TValue> set;
			if (sets.TryGetValue(key, out set)) return set.Contains(value);

			return false;
		}

		public bool Remove(TKey key, TValue value)
		{
			if (key == null) throw new ArgumentNullException("key");

			HashSet<TValue> set;
			if (!sets.TryGetValue(key, out set)) return false;

			if (!set.Remove(value)) return false;
			if (set.Count == 0) sets.Remove(key);

			return true;
		}

		public void Merge(ILookup<TKey, TValue> toMergeWith)
		{ 
			if (toMergeWith == null) throw new ArgumentNullException("toMergeWith");

			foreach (var group in toMergeWith) {
				foreach (var value in group) {
					this.Add(group.Key, value);
				}
			}
		}

		public IEnumerable<TValue> GetValues(TKey key, bool returnEmptySet = true)
		{
			HashSet<TValue> set;
			if (sets.TryGetValue(key, out set)) return set;
			
			return returnEmptySet ? Enumerable.Empty<TValue>() : null;
		}

		public void Clear()
		{
			sets.Clear();
		}

		public int Count
		{
			get { return sets.Count; }
		}

		public IEnumerable<TValue> Values
		{
			get {
				foreach (var kvp in sets)
				foreach (var value in kvp.Value) yield return value;
			}
		}

		bool ILookup<TKey, TValue>.Contains(TKey key)
		{
			return sets.ContainsKey(key);
		}

		IEnumerable<TValue> ILookup<TKey,TValue>.this[TKey key]
		{
			get { return GetValues(key, true); }
		}

		IEnumerator<IGrouping<TKey,TValue>> IEnumerable<IGrouping<TKey, TValue>>.GetEnumerator()
		{
			foreach (var kvp in sets) {
				yield return new Grouping<TKey,TValue>(kvp.Key, kvp.Value);
			}
		}

		void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
		{
			Add(item.Key, item.Value);
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
		{
			return Contains(item.Key, item.Value);
		}

		void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			throw new NotImplementedException();
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
		{
			get { return false; }
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
		{
			 return Remove(item.Key, item.Value);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return sets.GetEnumerator();
		}

		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
		{
			foreach (var kvp in sets)
			foreach (var value in kvp.Value) {
				yield return new KeyValuePair<TKey,TValue>(kvp.Key, value);
			}
		}
	}
}
