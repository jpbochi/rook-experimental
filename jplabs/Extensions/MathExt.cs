﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace JpLabs.Extensions
{
	public static class MathExt
	{
	    public static double Round(this double d)				{ return Math.Round(d); }
	    public static double Floor(this double d)				{ return Math.Floor(d); }
	    public static double Ceiling(this double d)				{ return Math.Ceiling(d); }
	    public static double Log(this double d, double basev)	{ return Math.Log(d, basev); }
	    public static double Pow(this double d, double power)	{ return Math.Pow(d, power); }
	    public static double Min(this double d, double other)	{ return Math.Min(d, other); }
	    public static double Max(this double d, double other)	{ return Math.Max(d, other); }
	    
	    static private IEnumerable<int> PossiblePrimes()
	    {
			yield return 2;
			yield return 3;
			
			int i = 5;
			while (true)
			{
				yield return i;
				if (i % 6 == 1) i += 2;
				i += 2;
			}
	    }
	    
	    public static IEnumerable<int> Divisors(this int number)
	    {
			foreach(int p in PossiblePrimes()) {
				while (number % p == 0) {
					number /= p;
					yield return p;
				}
				if (number == 1) yield break;
			}
	    }
	}
}
