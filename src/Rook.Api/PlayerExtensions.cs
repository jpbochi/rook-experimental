﻿using System;
using Rook.Augmentation;

namespace Rook.Api
{
	public static class PlayerExt
	{
		public static AugProperty NameProperty =
			AugProperty.Create("Name", typeof(string), typeof(PlayerExt), null);

		public static AugProperty ControllerIdProperty =
			AugProperty.Create("ControllerId", typeof(string), typeof(PlayerExt), null);

		public static AugProperty AvatarHrefProperty =
			AugProperty.Create("AvatarHref", typeof(string), typeof(PlayerExt), KnownResource.Avatar.Default);

		public static bool IsOpen(this IGamePlayer player)
		{
			return player.IsValueUnset(ControllerIdProperty);
		}

		public static string GetName(this IGamePlayer player)
		{
			return player.GetValue<string>(NameProperty);
		}

		public static string GetAvatarHref(this IGamePlayer player)
		{
			return player.GetValue<string>(AvatarHrefProperty);
		}

		public static T SetAvatar<T>(this T player, IHyperLink avatar) where T : IGamePlayer
		{
			player.SetValue(AvatarHrefProperty, avatar.Href.ToString());
			return player;
		}

		public static string GetControllerId(this IGamePlayer player)
		{
			return player.GetValue<string>(ControllerIdProperty);
		}

		public static IGamePlayer SetController(this IGamePlayer player, IOpenIdIdentity identity)
		{
			player.SetValue(ControllerIdProperty, identity.OpenId);
			player.SetValue(NameProperty, identity.Name);
			return player;
		}

		public static bool IsControlledBy(this IGamePlayer player, IOpenIdIdentity identity)
		{
			return player.GetControllerId() == identity.OpenId;
		}
	}
}
