﻿/// <reference path="../lib/linq.js" />
"use strict";

var Graphics = Graphics || {};

Graphics.BoardCoordinateSystem = function (boardSize) {
	return {
		positionToPoint: function (position) {
			return {
				x: position.x,
				y: (boardSize.rows - 1 - position.y)
			};
		}
	};
};

Graphics.HexBoardCoordinateSystem = function () {
	return {
		positionToPoint: function (position) {
			return {
				x: (position.x + ((position.y % 2) * 0.5)) * (72.5 / 84),
				y: position.y * (62.5 / 84)
			};
		}
	};
};
