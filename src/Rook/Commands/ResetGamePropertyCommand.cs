﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rook.Augmentation;

namespace Rook.Commands
{
	internal class ResetGamePropertyCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return ResetGamePropertyCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				var prop = AugProperty.FromRName(match.BoundVariables["property"]);
				return new ResetGamePropertyCommand(prop);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/reset/{property}");
		internal const string Relation = RookRelations.Commands.ResetGameProperty;

		public AugProperty Property { get; private set; }

		public ResetGamePropertyCommand(AugProperty property)
		{
			this.Property = property;
		}

		IHyperLink IGameMove.Link
		{
			get { return HyperLink.From(Template, Relation, new { Property}); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			if (Property == null) throw new InvalidOperationException();

			context.State.ClearValue(Property);
		}
	}
}