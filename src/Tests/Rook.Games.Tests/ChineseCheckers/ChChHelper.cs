﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using Ninject;
using Rook.Games.ChineseCheckers;

namespace Rook.Games.Tests.ChineseCheckers
{
	public class ChChHelper
	{
		public static readonly RName[] NorthRegion = new RName[] { "e4", "f2", "f3", "f4", "g1", "g2", "g3", "g4", "h3", "h4"};
		public static readonly RName[] NorthwestRegion = new RName[] { "a5", "b5", "c5", "d5", "a6", "b6", "c6", "b7", "c7", "b8"};
		public static readonly RName[] NortheastRegion = new RName[] { "j5", "k5", "l5", "m5", "j6", "k6", "l6", "k7", "l7", "k8"};
		public static readonly RName[] SouthwestRegion = new RName[] {"b10", "b11", "c11", "a12", "b12", "c12", "a13", "b13", "c13", "d13"};
		public static readonly RName[] SoutheastRegion = new RName[] {"k10", "k11", "l11", "j12", "k12", "l12", "j13", "k13", "l13", "m13"};
		public static readonly RName[] SouthRegion = new RName[] {"e14", "f14", "g14", "h14", "f15", "g15", "h15", "f16", "g16", "g17"};

		private readonly Lazy<IKernel> lazyKernel = new Lazy<IKernel>( StaticGameFactory.GetKernel<ChineseCheckersGameModule> );
		private static IDictionary<string,RName> KnownPieces;

		public IKernel Kernel 
		{
			get { return lazyKernel.Value; }
		}

		public IGameBoard ParseBoard(string strBoard)
		{
			var board = Kernel.Get<BasicBoard>();
			
			var pieces = strBoard.Split('|');

			foreach (var piece in pieces) {
				var split = piece.Split(new [] {'-'}, 2);

				board.AddPiece(board.Pieces.ById(ParseClass(split[0])).New(), split[1]);
			}

			return board;
		}

		protected static string PrepareBoard(string boardSetup)
		{
			return boardSetup
				.Replace(" ", "")
				.Replace("\t", "")
				.Replace("\r", "")
				.TrimEnd('\n');
		}

		public static string DumpBoard(IGameBoard board)
		{
			return board.PiecesAt(Tiles.MainBoard)
			.Select(p => string.Format("{0}-{1}", EncodeClass(p.Class), p.Position))
			.OrderBy(p => p)
			.Join("|");
		}

		private static string EncodeClass(RName klass)
		{
			return klass.Name;
		}

		private static RName ParseClass(string klass)
		{
			KnownPieces = KnownPieces ?? new [] {
				ChineseCheckersPiece.North,
				ChineseCheckersPiece.South,
				ChineseCheckersPiece.Northwest,
				ChineseCheckersPiece.Southwest,
				ChineseCheckersPiece.Northeast,
				ChineseCheckersPiece.Southeast,
			}.ToDictionary(p=> p.Name);

			return KnownPieces[klass];
		}

		public static void AssertPiecesPositions(IGameBoard board, RName klass, string[] expectedPositions)
		{
			AssertPiecesPositions(board, klass, expectedPositions.Select(t => (RName)t).ToArray());
		}

		public static void AssertPiecesPositions(IGameBoard board, RName klass, RName[] expectedPositions)
		{
			board.PiecesAt(Tiles.MainBoard)
				.Where(p => p.Class == klass)
				.Select(p => p.Position)
				.ShouldContainSameAs(expectedPositions);
		}
	}
}
