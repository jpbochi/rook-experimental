﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Rook.Games.Amazons;
using Rook.Tests.Extensions;
using Xunit;
using Xunit.Extensions;

namespace Rook.Games.Tests.Amazons
{
	public class AmazonsScoreCommandTests
	{
		private readonly Mother mother = new Mother();

		public static IEnumerable<object[]> WinningConditions
		{
			get { return Mother.GetWinningBoards().Select( t => new object[] { t.Item1, t.Item2, t.Item3 } ); }
		}

		[Theory]
		[PropertyData("WinningConditions")]
		void should_win(string boardDesc, RName currentPlayerId, RName expectedWinner)
		{
			mother.ParseAndBindBoard(boardDesc);
			var state = mother.GetNewState();
			state.CurrentPlayerId = currentPlayerId;

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new AmazonsScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			var expectedLooser = currentPlayerId;
			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", expectedWinner, GameScore.Won), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", expectedLooser, GameScore.Lost), UriKind.Relative),
				new Uri("/set-game-over", UriKind.Relative),
			});
		}

		[Theory]
		[FunctionData(typeof(Mother), "GetGameNotEndedBoards")]
		void should_continue_game(string boardDesc)
		{
			mother.ParseAndBindBoard(boardDesc);
			var state = mother.GetNewState();

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new AmazonsScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			recorder.Commands.ShouldBeEmpty();
		}
	}
}
