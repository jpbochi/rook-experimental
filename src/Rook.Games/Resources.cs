﻿using System.Collections.Generic;
using System.Reflection;

namespace Rook.Games
{
	public static class Resources
	{
		public static EmbeddedResourceInfo SteelSquareBoardBackGround { get; private set; }
		public static EmbeddedResourceInfo ChineseCheckersBackGround { get; private set; }
		public static EmbeddedResourceInfo GoBackGround { get; private set; }

		public static EmbeddedResourceInfo ChessPieceSet { get; private set; }
		public static EmbeddedResourceInfo CheckersPieceSet { get; private set; }

		static Resources()
		{
			var thisAssembly = Assembly.GetExecutingAssembly();

			SteelSquareBoardBackGround = new EmbeddedResourceInfo {
				Assembly = thisAssembly,
				ResourceName = "Rook.Games.Resources.steel_56px.gif",
				MediaType = MediaType.Parse("image/gif"),
				CssClass = "board-backboard squares",
				Description = "Board Background",
			};
			ChineseCheckersBackGround = new EmbeddedResourceInfo {
				Assembly = thisAssembly,
				ResourceName = "Rook.Games.Resources.chinese-checkers.gif",
				MediaType = MediaType.Parse("image/gif"),
				CssClass = "board-backboard chinese-checkers",
				Description = "Board Background",
			};
			GoBackGround = new EmbeddedResourceInfo {
				Assembly = thisAssembly,
				ResourceName = "Rook.Games.Resources.go_board_pattern.png",
				MediaType = MediaType.Parse("image/png"),
				CssClass = "board-backboard go",
				Description = "Board Background",
			};

			ChessPieceSet = new EmbeddedResourceInfo {
				Assembly = thisAssembly,
				ResourceName = "Rook.Games.Resources.mark_set_56px.gif",
				MediaType = MediaType.Parse("image/gif"),
				CssClass = "piece-set chess",
				Description = "Piece Image Set",
				Data = new Dictionary<string,string> {
					{"tile-size", "56"}
				}
			};
			CheckersPieceSet = new EmbeddedResourceInfo {
				Assembly = thisAssembly,
				ResourceName = "Rook.Games.Resources.checkers_set_56px.gif",
				MediaType = MediaType.Parse("image/gif"),
				CssClass = "piece-set checkers",
				Description = "Piece Image Set",
				Data = new Dictionary<string,string> {
					{"tile-size", "56"}
				}
			};
		}
	}
}
