﻿///<reference path="lib/jquery.min.js" />
///<reference path="lib/json2.js" />
///<reference path="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js" />
///<reference path="game/gamecontroller.js" />
///<reference path="helpers.js" />
"use strict";

var Game = Game || {};

Game.Controller = null;
Game.MoveLinks = [];

Game.submitMove = function (move) {
	$("#move-uri").val(move.link.href);
	$("form").submit();
};

$(function () {
	var allMoves = $('.hidden-move-list a').map(function() {
		var $a = $(this);
		return {
			player: $a.data('playerid'),
			origin: $a.data('origin'),
			destination: $a.data('destination'),
			title: $a.data('title'),
			link: {
				href: $a.attr('href'),
				rel: $a.attr('rel')
			}
		};
	});
	var movesPerPlayer = _.groupBy(allMoves, 'player');
	//TODO: remove this after we improve the non-board move rendering
	Game.Moves = movesPerPlayer;
});