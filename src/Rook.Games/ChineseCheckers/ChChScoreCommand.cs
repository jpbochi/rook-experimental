﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Symbols;
using Ninject;
using System;

namespace Rook.Games.ChineseCheckers
{
	internal class ChineseCheckersScoreCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return ChineseCheckersScoreCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new ChineseCheckersScoreCommand();
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/chinese-checkers/score");
		internal const string Relation = RookRelations.Commands.UpdateScore;

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public IHyperLink Link
		{
			get { return HyperLink.From(Template, Relation, null); }
		}

		public void Execute(IMoveExecutionContext context)
		{
			var state = context.State;
			if (IsRegionFilled(state.Board, ChineseCheckersPiece.South, ChineseCheckersGameModule.NorthRegion)) {
				var scoreCmds = state.SetWinnerCommands(ChineseCheckersPlayer.First);
				foreach (var cmd in scoreCmds) context.ExecuteMove(cmd);

				context.ExecuteMove(HyperLink.BuildUri(CommandTemplates.SetGameOver));
			}

			if (IsRegionFilled(state.Board, ChineseCheckersPiece.North, ChineseCheckersGameModule.SouthRegion)) {
				var scoreCmds = state.SetWinnerCommands(ChineseCheckersPlayer.Second);
				foreach (var cmd in scoreCmds) context.ExecuteMove(cmd);

				context.ExecuteMove(HyperLink.BuildUri(CommandTemplates.SetGameOver));
			}
		}

		private static bool IsRegionFilled(IGameBoard board, RName klass, IEnumerable<RName> region)
		{
			var pieces = board.PiecesAt(Tiles.MainBoard).Where(p => p.Class == klass).Select(p => p.Position);

			return region.Except(pieces).IsEmpty();
		}
	}
}
