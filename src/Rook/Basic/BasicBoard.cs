﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Rook
{
	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class BasicBoard : IGameBoard
	{
		[DataMember]
		public BasicPieceContainer Pieces { get; private set; }

		[DataMember]
		public IGameTileGraph Graph { get; private set; }

		public BasicBoard(IPieceContainer pieces, IGameTileGraph graph)
		{
			this.Pieces = (BasicPieceContainer)pieces; //TODO: make the IPieceContainer interface obsolete
			this.Graph = graph;
		}

		[IgnoreDataMember]
		public IEnumerable<IGamePiece> Classes
		{
			get { return Pieces.Where(p => p.BasePieceId == null); }
		}

		[IgnoreDataMember]
		IPieceContainer IGameBoard.Pieces { get { return Pieces; } }

		public IGamePiece AddPiece(IGamePiece piece, RName position)
		{
			Pieces.Add(piece, position);
			return piece;
		}

		public IGamePiece MovePiece(IGamePiece piece, RName destination)
		{
			Pieces.Move(piece, destination);
			return piece;
		}

		public IEnumerable<IGamePiece> PiecesAt(RName tileOrRegion)
		{
			if (tileOrRegion == Tiles.MainBoard) {
				//TODO: optimize this
				return Pieces.Where(p => p.Position != Tiles.OffBoard && p.Position != Tiles.Abstract);
			}
			return Pieces.At(tileOrRegion);
		}

		public IEnumerable<RName> Region(RName region)
		{
			return Graph.Region(region);
		}
	}
}
