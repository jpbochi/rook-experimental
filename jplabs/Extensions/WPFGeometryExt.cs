﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.Extensions
{
	#if WINDOWS_BASE_REFERENCED
	using System.Windows;

	public static class WindowsPointExt
	{
	    public static Point Scale(this Point p, double value)
	    {
	        return new Point(p.X * value, p.Y * value);
	    }
	    
	    public static Point Add(this Point p, Point t)
	    {
	        return new Point(p.X + t.X, p.Y + t.Y);
	    }

	    public static Point Translate(this Point p, double dx, double dy)
	    {
	        return new Point(p.X + dx, p.Y + dy);
	    }
	}
	
	public static class WindowsSizeExt
	{
	    public static Size Scale(this Size s, double value)
	    {
	        return new Size(s.Width * value, s.Height * value);
	    }

	    public static Size Join(this Size s, Size other)
	    {
	        return new Size(
				Math.Max(s.Width, other.Width),
				Math.Max(s.Height, other.Height)
			);
	    }
	}
	#endif
}
