﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<!DOCTYPE html>
<html>
<head>
<title>Rook - QUnit Tests</title>
	<link rel="stylesheet" href="//code.jquery.com/qunit/qunit-1.10.0.css" type="text/css" media="screen">

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/jquery.min.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/linq.js")%>"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/sammy.js/0.7.1/sammy.min.js"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/extensions.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/board.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameController.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameParser.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/game/gameClient.js")%>"></script>

	<script type="text/javascript" src="//code.jquery.com/qunit/qunit-1.10.0.js"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/lib/sinon.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/lib/sinon-qunit.js")%>"></script>

	<script type="text/javascript" src="<%= ResolveUrl("~/tests/piece.tests.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/board.tests.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/gameparser.tests.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/gameclient.tests.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/tests/gamecontroller.tests.js")%>"></script>
</head>
<body>
<h1 id="qunit-header">Rook - QUnit Tests</h1>
<h2 id="qunit-banner"></h2>
<div id="qunit-testrunner-toolbar"></div>
<h2 id="qunit-userAgent"></h2>
<ol id="qunit-tests"></ol>
<div id="qunit-fixture">test markup</div>
</body>
</html>