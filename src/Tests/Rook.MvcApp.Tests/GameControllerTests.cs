﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Moq;
using Rook.Api;
using Rook.Games;
using Rook.MvcApp.Controllers;
using Rook.MvcApp.Models;
using Rook.Tests;
using Xunit;
using Xunit.Extensions;

namespace Rook.MvcApp.Tests
{
	public class GameControllerTests
	{
		private const string _authenticatedUser = "Yoda";
		private readonly Mock<IGameRepository> _mockRepository;
		private readonly Mock<IGameLoader> _mockGameLoader;
		private readonly Mock<IModuleLocator> _mockGameLocator;
		private readonly Mock<Random> _mockRnd;
		private readonly GameController _controller;

		public GameControllerTests()
		{
			_mockRepository = new Mock<IGameRepository>();
			_mockGameLoader = new Mock<IGameLoader>();
			_mockRnd = new Mock<Random>();
			_mockGameLocator = new Mock<IModuleLocator>();

			_controller = new GameController(
				_mockRepository.Object, _mockGameLoader.Object, _mockGameLocator.Object, _mockRnd.Object
			).WithFakeContext().WithFakeUser(Fake.User(_authenticatedUser));
		}

		void SetupLoaderToReturnThisGame(Mock<IGameLoader> mockGameLoader, IGameMachine gameMachine)
		{
			var mockLoadingContext = new Mock<IGameLoadingContext>();
			var mockLoadingContextWithInitializers = mockLoadingContext.As<IGameLoadingContextWithInitializers>();
			var mockLoadingContextWithModules = mockLoadingContext.As<IGameLoadingContextWithModules>();

			mockGameLoader.Setup(m => m.CreateContext()).Returns(mockLoadingContext.Object);
			mockLoadingContext.Setup(m => m.WithInitializers(It.IsAny<IEnumerable<IGameModuleInitializer>>())).Returns(mockLoadingContextWithInitializers.Object);
			mockLoadingContextWithInitializers.Setup(m => m.WithModules(It.IsAny<IEnumerable<Type>>())).Returns(mockLoadingContextWithModules.Object);
			mockLoadingContextWithModules.Setup(m => m.CreateGameMachine()).Returns(gameMachine);
		}

		string GetTypeName(Type type)
		{
			return type.FullName;
		}

		[Fact]
		void should_yield_a_list_of_game_types_with_ids()
		{
			var testTypes = new [] { typeof(int), typeof(object), this.GetType() };

			_mockGameLocator.Setup(l => l.GetTypeName(It.IsAny<Type>())).Returns((Func<Type,string>)GetTypeName);
			_mockGameLocator.Setup(l => l.FindModuleTypes()).Returns(testTypes);

			var viewResult = _controller.Types().ShouldBeOfType<ViewResult>();

			var types = viewResult.ViewData.Model.ShouldBeOfType<IEnumerable<KeyValuePair<string,Type>>>();

			types.ShouldContainSameAs(testTypes.Select(t => new KeyValuePair<string,Type>(GetTypeName(t), t)));
		}

		[Fact]
		void should_create_and_redirect_to_game()
		{
			const int expectedNewGameId = 73;
			const string expectedGameName = "Alien vs. Predator";
			var expectedGameType = typeof(GameControllerTests);

			var gameMachine = new BasicGameMachine(RookTestHelper.CreateGameWithPlayers(2), null, null);

			SetupLoaderToReturnThisGame(_mockGameLoader, gameMachine);

			_mockGameLocator.Setup(l => l.FindType(GetTypeName(expectedGameType))).Returns(expectedGameType);

			_mockRepository.Setup(m =>
				m.Add(It.Is<GameEntry>(
					g => g.GameType == expectedGameType
					&& g.DisplayName == expectedGameName
					&& g.Machine == gameMachine
				))
			).Returns(expectedNewGameId);

			var args = new NewGameOptionsViewModel {
				GameName = expectedGameName,
				GameTypeName = GetTypeName(expectedGameType)
			};

			var	result = _controller.New(args);

			_mockGameLoader.VerifyAll();
			_mockRepository.VerifyAll();

			var redirectResult = result.ShouldBeOfType<RedirectToRouteResult>();
			redirectResult.Permanent.ShouldBeFalse();
			redirectResult.RouteValues["id"].ShouldBe(expectedNewGameId);
			redirectResult.RouteValues["action"].ShouldBe(PlayController.PlayActionName);
			redirectResult.RouteValues["controller"].ShouldBe(PlayController.ControllerName);
		}

		[Theory]
		[InlineData(PlayersToControl.First, new [] { true, false })]
		[InlineData(PlayersToControl.Second, new [] { false, true })]
		[InlineData(PlayersToControl.Hotseat, new [] { true, true })]
		void should_set_user_as_controller_of_requested_players(PlayersToControl requestedOrder, bool[] expectedControlledPlayers)
		{
			var gameType = typeof(GameControllerTests);
			var gameMachine = (IGameMachine)new BasicGameMachine(RookTestHelper.CreateGameWithPlayers(expectedControlledPlayers.Length), null, null);

			SetupLoaderToReturnThisGame(_mockGameLoader, gameMachine);

			var args = new NewGameOptionsViewModel {
				GameTypeName = GetTypeName(gameType),
				PlayersToControl = requestedOrder
			};

			_controller.New(args);

			_mockGameLoader.VerifyAll();

			gameMachine.State.Players.Select(p => p.GetControllerId()).ShouldBeEqualInOrder(expectedControlledPlayers.Select(p => p ? _authenticatedUser : null));
		}
		
		[Fact]
		void should_set_user_as_controller_of_a_random_player()
		{
			const int playerCount = 6;
			const int expectedControlledPlayer = 3;

			var gameType = typeof(GameControllerTests);
			var gameMachine = (IGameMachine)new BasicGameMachine(RookTestHelper.CreateGameWithPlayers(playerCount), null, null);

			SetupLoaderToReturnThisGame(_mockGameLoader, gameMachine);

			_mockRnd.Setup(m => m.Next(0, playerCount)).Returns(expectedControlledPlayer);

			var args = new NewGameOptionsViewModel {
				GameTypeName = GetTypeName(gameType),
				PlayersToControl = PlayersToControl.Random
			};

			_controller.New(args);

			_mockGameLoader.VerifyAll();
			_mockRnd.VerifyAll();

			gameMachine.State.Players
				.Select(p => p.GetControllerId())
				.ShouldBeEqualInOrder(Enumerable.Range(0, playerCount).Select(p => p == expectedControlledPlayer ? _authenticatedUser : null))
			;
		}
	}
}
