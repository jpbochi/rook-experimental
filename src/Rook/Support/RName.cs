﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Rook
{
	/// <summary>
	/// Source: http://www.koders.com/csharp/fid514DF0313DE91947B337912A32B0EFBEDE3EAB84.aspx?s=mdef%3ainsert
	/// </summary>
	[TypeConverter(typeof(RNameConverter))]
	public class RName : IEquatable<RName>
	{
		static readonly Regex NameRegex = new Regex(@"^[_a-zA-Z][-_\w\.]*$", RegexOptions.Compiled);
		static readonly Regex NamespaceRegex = new Regex(@"^(?:[_a-zA-Z][-_\w\.]*)?$", RegexOptions.Compiled);
		static readonly Regex ExpandedRegex = new Regex(@"^(?:(?<ns>[_a-zA-Z][-_\w\.]*)!)?(?<name>[_a-zA-Z][-_\w\.]*)$", RegexOptions.Compiled);

		private int hashcode;

		public string FullName { get; private set; }
		public string Name { get; private set; }
		public string Namespace { get; private set; }

		private RName(string local, string ns)
		{
			this.Name = local.Trim();
			this.Namespace = (ns ?? string.Empty).Trim();
			this.FullName = string.IsNullOrEmpty(ns) ? local : string.Concat(ns, "!", local);
			hashcode = Name.GetHashCode() ^ Namespace.GetHashCode();

		}

		public override bool Equals(object obj)
		{
			var n = obj as RName;
			return n != null && this == n;
		}

		public bool Equals(RName other)
		{
			return other != null && this == other;
		}

		public static bool TryGet(string expandedName, out RName name)
		{
			name = null;
			var match = ExpandedRegex.Match(expandedName);
			if (!match.Success) return false;

			name = new RName(match.Groups["name"].Value, match.Groups["ns"].Value);
			return true;
		}

		public static RName Get(string expandedName)
		{
			var match = ExpandedRegex.Match(expandedName);
			if (!match.Success) throw new ArgumentException(string.Format("{0} is an invalid RName", expandedName));

			return new RName(match.Groups["name"].Value, match.Groups["ns"].Value);
		}

		public static RName Get(string localName, string namespaceName)
		{
			if (!NameRegex.IsMatch(localName)) throw new ArgumentException();
			if (!NamespaceRegex.IsMatch(namespaceName ?? "")) throw new ArgumentException();

			return new RName(localName, namespaceName);
		}

		public override int GetHashCode()
		{
			return hashcode;
		}

		public static bool operator == (RName n1, RName n2)
		{
			if ((object) n1 == null) return (object) n2 == null;
			if ((object) n2 == null) return false;

			return object.ReferenceEquals(n1, n2) || (n1.Namespace == n2.Namespace && n1.Name == n2.Name);
		}

		public static implicit operator RName(string s)
		{
			return s == null ? null : Get(s);
		}

		public static implicit operator string(RName name)
		{
			return (name == null) ? null : name.FullName;
		}

		public static bool operator != (RName n1, RName n2)
		{
			return !(n1 == n2);
		}

		public override string ToString()
		{
			return FullName;
		}
	}

	internal class RNameConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || sourceType == typeof(RName) || base.CanConvertFrom(context, sourceType);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(RName) || base.CanConvertFrom(context, destinationType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string) return (RName)(string)value;
			if (value is RName) return value;
			
			return base.ConvertFrom(context, culture, value);
		}

		public override bool IsValid(ITypeDescriptorContext context, object value)
		{
			return value == null || value is RName;
		}
	}
}
