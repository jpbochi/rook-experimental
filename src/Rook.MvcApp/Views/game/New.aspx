﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Rook.MvcApp.Models.NewGameOptionsViewModel>" %>

<%@ Import Namespace="JpLabs.Extensions" %>
<%@ Import Namespace="Rook.MvcApp.Models" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

	<h2>New Game</h2>

	<% using (Html.BeginForm()) { %>
		<fieldset>
			<legend>Start new game</legend>

			<%: Html.ValidationSummary() %>

			<div>
				<%: Html.HiddenFor(m => m.GameTypeName) %>
				<div class="editor-label"><%: Html.LabelFor(m => m.GameTypeName) %></div>
				<div class="editor-field">
					<%: Rook.GameModuleLocator.FromCurrentDomain().FindType(Model.GameTypeName).GetDescription() %>
				</div>
			</div>

			<div>
				<div class="editor-label"><%: Html.LabelFor(m => m.GameName) %></div>
				<div class="editor-field"><%: Html.EditorFor(m => m.GameName) %><span class="optional">(optional)</span></div>
			</div>

			<% for (int i=0; i < Model.Options.Count; i++) { %>
				<div>
					<%: Html.HiddenFor(m => m.Options[i].InitializerType) %>
					<%: Html.HiddenFor(m => m.Options[i].PropertyName) %>
					<%: Html.HiddenFor(m => m.Options[i].DisplayName) %>
					<div class="editor-label"><%: Html.LabelFor(m => m.Options[i].Value, Model.Options[i].DisplayName) %></div>
					<div class="editor-field"><%: Html.EditorFor(m => m.Options[i].Value) %></div>
				</div>
			<% } %>

			<div>
				<div class="editor-label"><%: Html.LabelFor(m => m.PlayersToControl) %></div>
				<div class="editor-field">
					<%: Html.DropDownListFor(
						m => m.PlayersToControl,
						NewGameOptionsViewModel.PlayerOrderValues.Select(
							e => new SelectListItem(){ Text = e.GetDescription() }
						)
					)%>
				</div>
			</div>

			<div>
				<div class="editor-label"></div>
				<input type="submit" class="button" value="Start" />
			</div>
		</fieldset>
	<% } %>
</asp:Content>
