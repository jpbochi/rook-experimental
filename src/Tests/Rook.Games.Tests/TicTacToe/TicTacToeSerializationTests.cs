﻿using System.Reflection;

namespace Rook.Games.Tests.TicTacToe
{
	public class TicTacToeSerializationTests : BaseSerializationTests
	{
		private readonly Mother mother = new Mother();

		protected override string GetSerializedInitialGameInProd()
		{
			return ReadTextResource("Rook.Games.Tests.SampleData.initial-tic-tac-toe.bson.txt", Assembly.GetExecutingAssembly());
		}

		protected override IGameState GetNewInitialState()
		{
			return mother.GetNewState();
		}

		protected override IGameState GetNewChangedState()
		{
			var game = mother.GetNewGameMachine();

			game.PlayDropAt("a1");

			return game.State;
		}

		protected override IGameMachine GetNewGameMachine()
		{
			return mother.GetNewGameMachine();
		}

		protected override void VerifyInitialState(IGameState state)
		{
			TicTacToeStateTests.VerifyInitialState(state);

			mother.BindState(state);
			var game = mother.GetNewGameMachine();

			TicTacToeRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		protected override void VerifyAnyState(IGameState state)
		{
			TicTacToeStateTests.VerifyAnyState(state);
		}
	}
}
