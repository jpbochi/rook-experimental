﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Rook
{
	public static class RookTestHelper
	{
		static Random GetDeterministicRandom()
		{
			return new Random(42);
		}

		public static IEnumerable<IGamePlayer> CreatePlayers(int count)
		{
			var rnd = GetDeterministicRandom();
			var names = Enumerable.Range(0, count).Select(i => string.Concat("player_", rnd.Next())).ToArray();
			return CreatePlayers(names);
		}

		public static IEnumerable<IGamePlayer> CreatePlayers(params string[] playerNames)
		{
			return playerNames.Select(name => new BasicPlayer(name)).ToArray();
		}

		public static IGameState CreateGame()
		{
			return CreateGameWithPlayers(2);
		}

		public static IGameState CreateGameWithPlayers(int playerCount)
		{
			var players = CreatePlayers(playerCount);
			return new BasicGameState(players, null) { CurrentPlayerId = players.First().Id };
		}

		public static IGameState CreateGameWithPlayers(params string[] playerNames)
		{
			var players = CreatePlayers(playerNames);
			return new BasicGameState(players, null) { CurrentPlayerId = players.First().Id };
		}

		public static IGameMachine WithReferee(this IGameState state, params IGameReferee[] referees)
		{
			return new BasicGameMachine(state, referees);
		}

		public static void AssertLinkEquality(IHyperLink moveOne, IHyperLink moveTwo)
		{
			Tuple.Create(moveOne.Href, moveOne.Rel).ShouldBe(Tuple.Create(moveTwo.Href, moveTwo.Rel));
		}
	}
}
