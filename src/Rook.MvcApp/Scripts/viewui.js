﻿/*global AjaxWaiter, Consts */
/// <reference path="lib/jquery.min.js" />
/// <reference path="lib/jquery.linq.js" />
/// <reference path="lib/linq.js" />
/// <reference path="lib/json2.js" />
/// <reference path="helpers/ajaxwaiter.js" />
/// <reference path="graphics/boardrenderer.js" />
/// <reference path="graphics/boardRoot.js" />
/// <reference path="graphics/templates.js" />
/// <reference path="game/board.js" />
/// <reference path="helpers.js" />

var UI = {};

UI.boardRenderer = {};

UI.displayMessage = function (text, autoHide, delay) {
	autoHide = autoHide || false;

	$('#message-area').show();
	(autoHide) && $('#message-area').delay(delay || 2000).fadeOut(300);

	$('#message-area .message-text').html(text);
};

$(function () {

	if ($('#waitturn').length !== 0) {
		var waiter = new AjaxWaiter(function () {
			return {
				url: [Consts.RootPath, 'play/hasupdate/', Consts.GameId].join(''),
				type: 'GET',
				//beforeSend: function (jqXHR, settings) {
				//	jqXHR.setRequestHeader('if-none-match', UI.game.ETag);
				//},
				cache: false
			};
		});

		waiter.processResponseFunc = function (response) {
			if (response.value === true) {
				UI.displayMessage('it\'s your turn!');

				location.replace(location.pathname);
				return true;
			}

			return false;
		};
		waiter.onerror = function (jqXHR, textStatus, errorThrown) {
			UI.displayMessage([jqXHR && jqXHR.status, textStatus, errorThrown].join(', '));
		};
		waiter.onstop = function () {
			UI.displayMessage('');
		};
		waiter.attachTo($('#waitturn'), $('#loading'));

		setTimeout(waiter.start, 700);
	}

});

