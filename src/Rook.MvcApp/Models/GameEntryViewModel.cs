﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using Newtonsoft.Json;
using Rook.MvcApp.RookAuthentication;
using Rook.Api;

namespace Rook.MvcApp.Models
{
	public class GameMoveRequest
	{
		public class MoveDesc
		{
			[Required]
			public string MoveType { get; set; }

			public string Origin { get; set; }
			public string Target { get; set; }
		}

		[Required]
		public int GameId { get; set; }

		public MoveDesc Move { get; set; }
	}

	public class GamePlayResult
	{
		public string Message { get; set; }
	}

	public class GameEntryViewModel
	{
		public GameEntryViewModel(GameEntry model = null, IOpenIdIdentity user = null)
		{
			this.Entry = model;
			this.ControlledPlayers
				= (user == null) ? new RName[0]
				: model.State.Players.Where(p => p.IsControlledBy(user)).Select(p => p.Id).ToArray();
		}

		public GameEntry Entry { get; private set; }
		public IEnumerable<RName> ControlledPlayers { get; private set; }
	}
}