﻿using System;
using System.Collections.Generic;

namespace Rook
{
	internal class UniqueIdGenerator
	{
		readonly Random Rnd;
		readonly HashSet<int> Set = new HashSet<int>();

		public UniqueIdGenerator(int seed)
		{
			Rnd = new Random(seed);
		}

		public int Next()
		{
			while (true) {
				var key = Rnd.Next();

				if (Set.Add(key)) return key;
			}
		}
	}
}
