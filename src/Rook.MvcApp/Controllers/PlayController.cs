﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JpLabs.Extensions;
using Newtonsoft.Json;
using Rook.Api;
using Rook.MvcApp.Models;
using Rook.MvcApp.RookAuthentication;

namespace Rook.MvcApp.Controllers
{
	public class PlayController : Controller
	{
		/// TODO:
		/// - Engine:
		///		- Resign move [DONE];
		///		- Move history;
		/// - View:
		///		- Auto-Refresh for real-time playing [DONE];
		///		- Ajax refresh [WIP];
		///		- Rematch;
		///		- Highlight last move [depends on move history];
		///		- Flip board feature;
		///			- Automatic flip board for Black (does not makes sense for some game types);
		///	- Controller improvements:
		///		- Avoid any accidental play if the browser view was outdated (HTTP ETag to the rescue);

		public const string ControllerName = "play";
		public const string PlayActionName = "play";
		public const string ErrorViewName = "error";

		readonly IGameRepository repository;

		public PlayController() : this(new GameRepository()) {}

		public PlayController(IGameRepository repository)
		{
			this.repository = repository;
		}

		[Authorize][HttpGet]
		public ActionResult Play(int id, string error)
		{
			var game = repository.GetById(id);
			if (game == null) return GameNotFoundError();

			if (!string.IsNullOrWhiteSpace(error)) ModelState.AddModelError("", error);

			return PlayResult(game);
		}

		[Authorize][HttpGet]
		[Obsolete("to be replaced by games rest api")]
		public ActionResult Moves(int gameId, string playerId)
		{
			var game = repository.GetById(gameId);
			if (game == null) return GameNotFoundError();

			var user = User.GetOpenIdIdentity();
			return PartialView(game.Machine.GetMoveOptions(user).ToReadOnlyColl());
		}

		[Authorize][HttpPost][ActionName(PlayActionName)]
		[Obsolete("to be replaced by games rest api")]
		public ActionResult PlayPost(int id, string moveUri)
		{
			var game = repository.GetById(id);
			if (game == null) return GameNotFoundError();

			if (string.IsNullOrWhiteSpace(moveUri)) return RedirectToGame(id, "Select a move to play");

			var uri = new Uri(moveUri, UriKind.Relative);

			//maps partial move href to API href (will die with this route) 
			var route = new UriTemplate("api/games/{gameId}/move/{*moveHref}");
			var baseAddress = new Uri("http://example.org");
			var match = route.Match(baseAddress, new Uri(baseAddress, uri));
			if (match != null) uri = new Uri(match.BoundVariables["moveHref"], UriKind.Relative);

			var principal = RookPrincipal.From(User.GetOpenIdIdentity(), game.Machine.State);
			//TODO: fix race condition here (use locks and HTTP ETag)
			{
				if (!game.Machine.TryPlay(uri, principal)) return RedirectToGame(id, "Invalid move");
				//TODO: invalidate game etag cache
				repository.Update(game);
			}

			return RedirectToGame(id);
		}

		[Authorize][HttpGet] //TODO: this should be a POST
		public ActionResult Join(int id, string player)
		{
			var game = repository.GetById(id);
			if (game == null) return GameNotFoundError();

			RName playerId;
			if (!RName.TryGet(player, out playerId)) return RedirectToGame(id, "invalid player");

			//TODO: fix race condition here (use locks and HTTP ETag)
			{
				if (!game.State.TryJoinAsPlayer(User.GetOpenIdIdentity(), playerId)) return RedirectToGame(id, "Can't join game");
				//TODO: invalidate game etag cache
				repository.Update(game);
			}

			return RedirectToGame(id);
		}

		[Authorize][HandleJsonException][HttpGet]
		[Obsolete("to be replaced by games rest api")]
		public ActionResult HasUpdate(int id)
		{
			//TODO: hold the reply for a couple of seconds, while waiting for an update on the game

			var game = repository.GetById(id);
			if (game == null) return new HttpStatusCodeResult((int)HttpStatusCode.NotFound);

			var ifNoneMatch = Request.Headers["If-None-Match"];
			if (ifNoneMatch != null && ifNoneMatch != game.State.Version) return JsonBool(true);

			return JsonBool(game.State.CanPlayAsCurrentPlayer(User.GetOpenIdIdentity()) || game.State.IsOver());
		}

		private object JsonValue(object value)
		{
			return new { value };
		}

		private ContentResult JsonMessage(string message)
		{
			return JsonContent(JsonValue(message));
		}

		private ContentResult JsonBool(bool value)
		{
			return JsonContent(JsonValue(value));
		}

		private ContentResult JsonContent(object response)
		{
			return Content(JsonConvert.SerializeObject(response), "application/json; charset=utf-8");
		}

		private RedirectToRouteResult RedirectToGame(int id, string error = null)
		{
			var routeValues = (error == null) ? new { id } : (object)new { id, error };

			return RedirectToAction(PlayActionName, routeValues);
		}

		private ViewResult PlayResult(GameEntry game)
		{
			var viewModel = new GameEntryViewModel(game, User.GetOpenIdIdentity());
			return View(string.Empty, viewModel);
		}

		private ViewResult ErrorResult(string errorMessage)
		{
			ModelState.AddModelError("", errorMessage);
			return View(ErrorViewName);
		}

		private ViewResult PlayResultWithError(GameEntry game, string errorMessage)
		{
			ModelState.AddModelError("", errorMessage);
			return PlayResult(game);
		}

		private ViewResult GameNotFoundError()
		{
			Response.StatusCode = (int)HttpStatusCode.NotFound;
			return ErrorResult("A game with this id was not found!");
		}
	}
}
