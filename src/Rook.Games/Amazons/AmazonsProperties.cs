﻿using JpLabs.Symbols;
using Rook.Augmentation;

namespace Rook.Games.Amazons
{
	public static class AmazonsPieces
	{
		public const string Namespace = "amazons";

		public static readonly RName Amazon = RName.Get("amazon", Namespace);
		public static readonly RName Arrow = RName.Get("arrow", Namespace);
		public static readonly RName WhiteAmazon = RName.Get("white-amazon", Namespace);
		public static readonly RName BlackAmazon = RName.Get("black-amazon", Namespace);
	}

	internal static class AmazonsProperties
	{
		public static AugProperty LastPieceToMoveProperty =
			AugProperty.Create("LastPieceToMove", typeof(RName), typeof(AmazonsProperties));

		public static AugProperty ArrowClassProperty =
			AugProperty.Create("ArrowClass", typeof(RName), typeof(AmazonsProperties));

		public static IGamePiece GetLastPieceToMove(this IGameState state)
		{
			var pieceId = state.GetValue<RName>(LastPieceToMoveProperty);

			if (pieceId == null) return null;
			return state.Board.Pieces.ById(pieceId);
		}

		public static IGamePiece SetArrowClass(this IGamePiece piece, RName pieceClass)
		{
			piece.SetValue(ArrowClassProperty, pieceClass);
			return piece;
		}

		public static RName GetArrowClass(this IGamePiece piece)
		{
			return piece.GetValue<RName>(ArrowClassProperty);
		}
	}
}
