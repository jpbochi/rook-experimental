﻿using System;
using System.Net;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace Rook.MvcApp.Controllers
{
    public class ResourceController : Controller
    {
		public const string ControllerName = "resource";
		public const string GetResourceActionName = "get";

		[HttpGet]
		[OutputCache(Duration=3600, VaryByParam="id")]
		public ActionResult Get(string id)
		{
			var etag = new EntityTagHeaderValue(string.Concat("\"", id, "\""));
			var ifNoneMatch = Request.Headers["If-None-Match"];
			if (ifNoneMatch != null && ifNoneMatch != etag.Tag) return new HttpStatusCodeResult(HttpStatusCode.NotModified);

			var resource = HttpContext.Cache.GetResourceFromId(id);
			if (resource == null) return new HttpNotFoundResult();

			Response.Cache.SetETag(etag.Tag);
			Response.Cache.SetMaxAge(TimeSpan.FromHours(24));

			return new FileStreamResult(resource.GetStream(), resource.MediaType.ToString());
		}
    }
}
