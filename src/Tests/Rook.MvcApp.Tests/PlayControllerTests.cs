﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using JpLabs.Extensions;
using Moq;
using Rook.Api;
using Rook.MvcApp.Controllers;
using Rook.MvcApp.Models;
using Xunit;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Web;
using Rook.Tests;

namespace Rook.MvcApp.Tests
{
	public class PlayControllerTests
	{
		const int gameId = 42;

		Mock<IGameReferee> mockReferee;
		BasicGameMachine machine;
		PlayController controller;
		IGameState gameState;
		System.Security.Principal.IPrincipal user;

		public PlayControllerTests()
		{
			const string creator = "http://rook/maester";

			this.gameState = RookTestHelper.CreateGameWithPlayers("batima", "robin", "coringa");
			this.mockReferee = new Mock<IGameReferee>();
			this.machine = new BasicGameMachine(gameState, mockReferee.Object.ToEnumerable(), null);
			var entry = GameEntry.CreateNew(machine, creator, null, null);

			var mockRepository = new Mock<IGameRepository>();
			mockRepository.Setup(r => r.GetById(gameId)).Returns(entry);

			this.user = Fake.User("TiaDoBatima");
			this.controller = new PlayController(mockRepository.Object).WithFakeContext().WithFakeUser(user);
		}

		[Fact]
		void should_list_moves()
		{
			var player = gameState.GetCurrentPlayer();
			player.SetController(user.GetOpenIdIdentity());
			var playerid = player.Id;

			this.mockReferee.Setup(r => r.GetMoves(It.IsAny<IGameState>())).Returns(new [] {
				HyperLink.From(new UriTemplate("/doit/better"), "/test/daft", null, new { playerid }),
				HyperLink.From(new UriTemplate("/doit/harder"), "/test/daft", null, new { playerid }),
				HyperLink.From(new UriTemplate("/celebrate"), "/test/onemoretime", null, new { playerid }),
			});

			var result = this.controller.Moves(gameId, playerid);

			var partialView = result.ShouldBeOfType<PartialViewResult>();
			var moves = partialView.Model.ShouldBeOfType<IEnumerable<IHyperLink>>();

			moves.Select(m => Tuple.Create(m.Href.ToString(), m.Rel)).ShouldContainSameAs(new [] {
				Tuple.Create("/doit/better", "/test/daft"),
				Tuple.Create("/doit/harder", "/test/daft"),
				Tuple.Create("/celebrate", "/test/onemoretime")
			});
		}
	}
}
