﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Moq;
using Rook.Games.Common;
using Rook.Games.TicTacToe;
using Xunit;

namespace Rook.Games.Tests.TicTacToe
{
	public class TicTacToeRefereeTests
	{
		private readonly Mother mother = new Mother();

		[Fact]
		void should_provide_initial_move_options()
		{
			var state = mother.GetNewState();
			var referee = mother.GetNewReferee();

			var moves = referee.GetMoves(state);
			Assert.NotNull(moves);

			VerifyInitialMoveOptions_Links(moves, state.Board);
		}

		internal static void VerifyInitialMoveOptions_Links(IEnumerable<IHyperLink> actualMoves, IGameBoard board)
		{
			var expectedPlayerId = TicTacToePlayer.X;
			var expectedPieceClass = TicTacToePieces.X;

			var expectedMoves = board.Region(Tiles.MainBoard).Select(
				tile => HyperLink.From(
					TicTacToeMove.Template,
					TicTacToeMove.Relation,
					new { PlayerId = expectedPlayerId, Piece = expectedPieceClass, Position = tile }
				).Href
			);
			
			actualMoves.Where(m => !m.Is<ResignMove>()).Select(m => m.Href).ShouldContainSameAs(expectedMoves);
		}

		[Fact]
		void should_accept_move_if_user_is_authorized()
		{
			var state = mother.GetNewState();
			var referee = new TicTacToeReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == TicTacToeMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(true).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeTrue("Failed to play move");
			userMock.Verify();
		}

		[Fact]
		void should_reject_move_if_user_is_not_authorized()
		{
			var state = mother.GetNewState();
			var referee = new TicTacToeReferee();

			var move = referee.GetMoves(state).First(m => m.Rel == TicTacToeMove.Relation).Href;
			var player = state.CurrentPlayerId;

			var userMock = new Mock<IPrincipal>();
			userMock.Setup(u => u.IsInRole(player)).Returns(false).Verifiable();

			var moveWasAccepted = referee.ExecuteMove(move, new MoveExecutionContext(state, referee, userMock.Object));
			
			moveWasAccepted.ShouldBeFalse("Referee accepted unauthorized move");
			userMock.Verify();
		}
	}
}
