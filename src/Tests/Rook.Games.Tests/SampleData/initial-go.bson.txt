﻿{
  "_t": "BasicGameState",
  "Aug": {
    "{Rook.BasicGameState}CurrentPlayerId": "\"player!black\""
  },
  "Players": [
    {
      "_t": "BasicPlayer",
      "_id": "player!black"
    },
    {
      "_t": "BasicPlayer",
      "_id": "player!white"
    }
  ],
  "Board": {
    "_t": "BasicBoard",
    "Pieces": {
      "SerializableBoardDict": [
        {
          "_t": "BasicPiece",
          "Aug": {
            "{Rook.PieceProperties}Banner": "\"player!black\""
          },
          "_id": "go_stone!black",
          "Class": "go_stone!black",
          "Position": "_class_"
        },
        {
          "_t": "BasicPiece",
          "Aug": {
            "{Rook.PieceProperties}Banner": "\"player!white\""
          },
          "_id": "go_stone!white",
          "Class": "go_stone!white",
          "Position": "_class_"
        }
      ]
    },
    "Graph": {
      "_t": "BasicTileGraph",
      "TileSet": {
        "_t": "RectangularGridTileSet",
        "Cols": 9,
        "Rows": 9
      },
      "LinkSet": {
        "_t": "VectorBasedLinkSet",
        "directions": [
          {
            "Name": "S",
            "SerializableVector": "{0,-1}"
          },
          {
            "Name": "N",
            "SerializableVector": "{0,1}"
          },
          {
            "Name": "W",
            "SerializableVector": "{-1,0}"
          },
          {
            "Name": "E",
            "SerializableVector": "{1,0}"
          }
        ]
      }
    }
  }
}