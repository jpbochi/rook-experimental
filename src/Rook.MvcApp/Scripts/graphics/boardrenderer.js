﻿/*global Enumerable, CanvasRenderingContext2D, Mouse */
/// <reference path="../lib/linq.js" />
/// <reference path="boardCoordinateSystem.js" />

var Graphics = Graphics || {};

$(function () {
	// Canvas test get from here: http://w3c-test.org/html/tests/approved/canvas/canvas_complexshapes_ispointInpath_001.htm
	// Workaround get from here: https://bug405300.bugzilla.mozilla.org/attachment.cgi?id=312691

	var canvas = $('<canvas></canvas>').get(0);
	var ctx = canvas.getContext("2d");

	// Create a path that is transformed by a translation transformation matrix.
	ctx.translate(100, 50);
	ctx.rect(0, 0, 100, 50);

	// Ensure that the coordinates passed to isPointInPath are unaffected by the current transformation matrix.
	if (!ctx.isPointInPath(125, 75) || ctx.isPointInPath(25, 25)) {
		//alert('isPointInPath fix applied');

		var isPointInPathWithBug = CanvasRenderingContext2D.prototype.isPointInPath;
		CanvasRenderingContext2D.prototype.isPointInPath = function(x, y)
		{
			this.save();
			this.setTransform(1, 0, 0, 1, 0, 0);
			var ret = isPointInPathWithBug.call(this, x, y);
			this.restore();
			return ret;
		};
	}
});

Graphics.ImageRepository = function(imageRepositoryElement) {
	this.get = function(selector) {
		return imageRepositoryElement.find(selector).get(0);
	};
};

Graphics.BoardRenderer = function (canvas, board, imageRepository, rendererArgs) {

	var resetCanvasSize = function (ctx) {
		var size = rendererArgs.rootObj.getSize();

		canvas.width = size.width * ctx.renderer.zoom();
		canvas.height = size.height * ctx.renderer.zoom();
	};

	var getContext = function (renderer) {
		var ctx = canvas.getContext("2d");

		ctx.canvas = canvas;
		ctx.lineWidth = 1.0;
		ctx.renderer = renderer;

		ctx.isMouseInPath = function () {
			return ctx.mousePos && ctx.isPointInPath(ctx.mousePos.x, ctx.mousePos.y);
		};

		ctx.translateFromBoardToTile = function (position) {
			position = rendererArgs.coordinateSystem.positionToPoint(position);
			ctx.translate(position.x + 0.5, position.y + 0.5);
		};

		ctx.drawAt = function (tile, drawFunc) {
			ctx.save();
			ctx.translateFromBoardToTile(tile.position || board.getTilePosition(tile));

			drawFunc();

			ctx.restore();
		};

		ctx.drawTemplate = function(templateName) {
			var template = rendererArgs.templates[templateName];
			template.draw.apply(template, [ctx].concat(Array.prototype.slice.call(arguments, 1)));
		};

		return ctx;
	};

	var drawPiece = function(ctx, piece) {
		ctx.drawTemplate('piece', piece);
	};

	var getAllValues = function (obj) {
		var values = [];
		for (var key in obj) { values.push(obj[key]); }
		return values;
	};
		
	var drawDecoration = function (ctx, decoration) {
		if (!decoration) { return; }
		ctx.save();

		(decoration.position) && ctx.translateFromBoardToTile(decoration.position);
		decoration.draw(ctx);

		ctx.restore();
	};

	var canvasHandlers = {
		click: [],
		mousemove: []
	};

	var decorations = {};
	var zoom = rendererArgs.zoom || 0.9;
	var requiresRefresh = false;
	var renderingPaused = false;

	var renderer = {
		canvas: function() {
			return canvas;
		},
		board: function() {
			return board;
		},
		zoom: function() {
			return zoom;
		},
		getImage: function(id) {
			return imageRepository.get(id);
		},
		decoration: function (key, newValue) {
			newValue && (decorations[key] = newValue);
			return decorations[key];
		},
		removeDecoration: function (key) {
			delete decorations[key];
		},
		drawBoard: function (ctx) {
			ctx.save();
			ctx.scale(zoom, zoom);

			rendererArgs.rootObj.draw(ctx, function() {

				Enumerable.From(getAllValues(decorations)).OrderBy(function (dec) {
					return dec.zorder || 0;
				}).ForEach(function (dec) {
					drawDecoration(ctx, dec);
				});

				Enumerable.From(board.getAllPieces()).OrderBy(function (piece) {
					return piece.zorder || 0;
				}).ForEach(function (piece) {
					board.hasTile(piece.position) && drawPiece(ctx, piece);
				});

				if (ctx.mousePos) {
					board.getAllTiles().each(function (tile) {
						ctx.drawAt(tile, function() {
							ctx.drawTemplate('tile');

							ctx.isMouseInPath() && ctx.hits.push({ klass: 'tile', position: tile});

							/*
							ctx.fillStyle = "#000000";
							ctx.font = "bold 1px Courier New";
							ctx.textAlign = "center";
							ctx.textBaseline = "middle";
							ctx.scale(.4, .7);
							ctx.fillText(tile.name, 0, 0);//*/
						});
					});
				}
			});

			ctx.restore();
		},
		repaint: function () {
			if (renderingPaused) {
				requiresRefresh = true;
				return;
			}

			var ctx = getContext(this);
			resetCanvasSize(ctx);
			this.drawBoard(ctx);
		},
		repaintWithHitTest: function (mousePos) {
			var ctx = getContext(this);
			resetCanvasSize(ctx);

			ctx.mousePos = mousePos;
			ctx.hits = [];

			this.drawBoard(ctx);

			return ctx.hits;
		},
		click: function (selector, callback) {
			canvasHandlers.click.push({
				selector: selector,
				callback: callback
			});
		},
		mousemove: function (selector, callback) {
			canvasHandlers.mousemove.push({
				selector: selector,
				callback: callback
			});
		}
	};

	var passFilter = function(selector, hit) {
		return (selector.substr(0, 1) === '.') && (selector.substr(1) === hit.klass);
	};

	var bindEvent = function(eventName, handleDelay) {
		var actualHandler = function(mousePos) {
			var handlers = canvasHandlers[eventName];
			if (handlers.length === 0) { return; }

			renderingPaused = true;

			try {
				var hits = renderer.repaintWithHitTest(mousePos);

				Enumerable.From(handlers).ForEach(function(handler) {
					Enumerable.From(hits).Where(function(hit) {
						return passFilter(handler.selector, hit);
					}).ForEach(function(hit) {
						handler.callback(hit);
					});
				});
			} finally {
				renderingPaused = false;
			}

			(requiresRefresh) && renderer.repaint();
		};

		var timeout = null;

		var eventHandler = function(evt) {
			var mousePos = Mouse.getRelativeCoordinates(evt, canvas);

			if (!handleDelay) {
				actualHandler(mousePos);
			} else {
				clearTimeout(timeout);
				timeout = setTimeout(function() {
					try {
						actualHandler(mousePos);
					} finally {
						timeout = null;
					}
				}, handleDelay);
			}
		};
		$(canvas).unbind(eventName).bind(eventName, eventHandler);
	};

	bindEvent('click');
	bindEvent('mousemove', 7);

	return renderer;
};
