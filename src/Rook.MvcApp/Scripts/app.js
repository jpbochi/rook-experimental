﻿///<reference path="http://cdnjs.cloudflare.com/ajax/libs/sammy.js/0.7.1/sammy.min.js" />

var Rook = Rook || {};

Rook.App = Sammy('#container', function() {
	"use strict";
	var gameStateLink = this.$element("a[rel='public-game']");
	var gameStateUrl = gameStateLink.attr('href');

	this.use('Mustache', 'ms');

	this.helpers({
		templatePath: function(templateName) {
			return [Consts.RootPath, 'content/templates/', templateName, '.ms'].join('');
		}
	});
	
	this.get('#/as/:player', function(context) {
		Rook.GameClient.loadState(gameStateUrl, function(state) {
			var player = state.playersById[context.params.player];
			if (!player) {
				return context.notFound();
			}
			var playUrl = state.playersById[context.params.player].links['game-for-player'];

			Rook.GameClient.loadGame(playUrl, function(game) {
				startGame(game, context);
			}).error(function() {
				context.redirect('#/not-playing'); //not allowed
			});
		});
	});

	this.post('', function() {
		return true;
	});

	this.get('#/not-playing', function(context) {
		Rook.GameClient.loadState(gameStateUrl, function(state) {
			startGame(state, context);
		});
	});

	this.get('#/', function(context) {
		Rook.GameClient.loadState(gameStateUrl, function(state) {
			var currentPlayer = state.playersById[state.currentPlayer];
			if (!currentPlayer) {
				context.redirect('#/not-playing');
				return;
			}

			var playUrl = state.playersById[state.currentPlayer].links['game-for-player'];

			Rook.GameClient.loadGame(playUrl).done(function() {
				context.redirect('#/as', state.currentPlayer);
			}).error(function() {
				context.redirect('#/not-playing');
			});
		});
	});

	this.notFound = function() {
		this.runRoute('get', '#/');
	};
});

$(function () {
	"use strict";

	Rook.App.run();
});