﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rook.MvcApp.Models.GameEntryViewModel>" %>

<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.MvcApp" %>

<div id="board-image-repository">
	<%
		Model.Entry.Machine.RenderResourceImages(Writer, Url);
	%>
</div>

<div class="canvas-border">
	<canvas id="board_canvas">
		Your browser does not support the canvas element.<br />
		Please, try another browser.
	</canvas>
</div>