﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;

namespace Rook
{
	public abstract class BaseReferee
	{
		public abstract IGameMove FindMove(Uri moveUri, IGameState state);

		public bool ExecuteMove(Uri moveUri, IMoveExecutionContext context)
		{
			var move = FindMove(moveUri, context.State);
			if (move == null || !context.User.IsInRole(move.RequiredRole)) return false;

			move.Execute(context.AsReferee());
			return true;
		}
	}

	public class MasterReferee : BaseReferee, IGameReferee
	{
		public ICollection<IGameReferee> Referees { get; private set; }

		public static IGameReferee MergeReferees(IEnumerable<IGameReferee> referees)
		{
			if (referees == null) return null; //actually should return an empty referee
			var refereeList = referees.ToList();

			if (refereeList.Count == 1) return refereeList.First();
			return new MasterReferee(refereeList);
		}

		private MasterReferee(IEnumerable<IGameReferee> referees)
		{
			this.Referees = referees.ToReadOnlyColl();
		}

		IEnumerable<IHyperLink> IGameReferee.GetMoves(IGameState state)
		{
			return Referees.SelectMany(r => r.GetMoves(state));
		}

		public override IGameMove FindMove(Uri moveUri, IGameState state)
		{
			//What to do if more than one referee claim to know the Move?
			return Referees.Select(r => r.FindMove(moveUri, state)).SingleOrDefault(m => m != null);
		}
	}
}
