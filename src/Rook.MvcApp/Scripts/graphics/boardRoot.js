﻿/// <reference path="../lib/linq.js" />
/// <reference path="boardCoordinateSystem.js" />

var Graphics = Graphics || {};

Graphics.RectBoardCoordinatesTemplate = function() {
	return {
		draw: function (ctx, tileSize, margin) {
			// Draws coordinate numbers and letters
			ctx.save();

			ctx.fillStyle = "#666666";
			ctx.font = "bold 12px verdana,Arial,Helvetica,sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "middle";

			var canvasHeight = ctx.canvas.height / ctx.renderer.zoom();
			var i, left, bottom;
			var size = ctx.renderer.board().getSize();

			for (i = 0; i < size.cols; i++) {
				left = margin + ((i + 0.5) * tileSize);
				bottom = margin / 2.5;
				ctx.fillText(String.fromCharCode(97 + i), left, canvasHeight - bottom);
				//ctx.strokeRect(left - 14.5, canvasHeight - bottom - 3.5, 29, 7);
			}

			for (i = 0; i < size.rows; i++) {
				left = margin / 2;
				bottom = margin + ((i + 0.5) * tileSize);
				ctx.fillText((i + 1).toString(), left, canvasHeight - bottom);
				//ctx.strokeRect(left - 3.5, canvasHeight - bottom - 14.5, 7, 29);
			}
			ctx.restore();
		}
	};
};

Graphics.RectBoardRoot = function (boardSize) {
	var options = {
		margin: 16,
		tileSize: 56,
		boardBorderStyle: "rgb(102, 102, 102)"
	};

	var translateToBoard = function (ctx) {
		ctx.translate(options.margin, options.margin);
	};
	
	var scaleFromPixelToTile = function (ctx) {
		var scale = options.tileSize;
		ctx.lineWidth /= scale;
		ctx.scale(scale, scale);
	};

	return {
		getSize: function() {
			return {
				width: (options.margin * 2 + boardSize.cols * options.tileSize),
				height: (options.margin * 2 + boardSize.rows * options.tileSize)
			};
		},
		draw: function (ctx, drawChildrenFunc) {
			ctx.drawTemplate('coordinates', options.tileSize, options.margin);

			ctx.save();

			translateToBoard(ctx);
			var backgroundImage = ctx.renderer.getImage('.board-backboard');

			var boardSize = ctx.renderer.board().getSize();
			if (backgroundImage.complete) {
				ctx.fillStyle = ctx.createPattern(backgroundImage, "repeat");
				ctx.fillRect(0, 0, boardSize.cols * options.tileSize, boardSize.rows * options.tileSize);
			}

			scaleFromPixelToTile(ctx);
			var halfPixel = ctx.lineWidth / 2;
			ctx.strokeStyle = options.boardBorderStyle;
			ctx.strokeRect(-halfPixel, -halfPixel, boardSize.cols + 2 * halfPixel, boardSize.rows + 2 * halfPixel);

			drawChildrenFunc && drawChildrenFunc();

			ctx.restore();
		}
	};
};

Graphics.HexBoardRoot = function (boardSize) {
	var options = {
		margin: 4,
		padding: { x: 23, y: 32 },
		tileSize: 84
	};
	
	var scaleFromPixelToTile = function (ctx) {
		var scale = options.tileSize;
		ctx.lineWidth /= scale;
		ctx.scale(scale, scale);
	};

	return {
		getSize: function() {
			return {
				width: (options.margin + options.padding.x + 10) * 2 + boardSize.cols * options.tileSize * (72.5 / 84),
				height: (options.margin + options.padding.y + 10) * 2 + boardSize.rows * options.tileSize * (62.5 / 84)
			};
		},
		draw: function (ctx, drawChildrenFunc) {
			ctx.save();

			function createImageNotFoundFillStyle() {
				var style = ctx.createLinearGradient(0, 0, 300, 300);
				style.addColorStop(0, "grey");
				style.addColorStop(0.25, "white");
				style.addColorStop(0.5, "grey");
				style.addColorStop(0.75, "white");
				style.addColorStop(1, "grey");
				return style;
			}

			ctx.translate(options.margin, options.margin);

			//TODO: backgroundImage.complete does not mean that the Image has loaded successfully
			var backgroundImage = ctx.renderer.getImage('.board-backboard');

			var backgroundPattern = (backgroundImage.complete) ? ctx.createPattern(backgroundImage, "no-repeat") : createImageNotFoundFillStyle(ctx);
			ctx.fillStyle = backgroundPattern;
			ctx.rect(0, 0, ctx.canvas.width / ctx.renderer.zoom(), ctx.canvas.height / ctx.renderer.zoom());
			ctx.fill();

			ctx.translate(options.padding.x, options.padding.y);
			scaleFromPixelToTile(ctx);
			drawChildrenFunc && drawChildrenFunc();

			ctx.restore();
		}
	};
};
