﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using JpLabs.Extensions;
using JpLabs.Geometry;
using JpLabs.Symbols;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Rook.Api;

namespace Rook.Games.Amazons
{
	[GameInitializer(typeof(AmazonsGameModule))]
	public class GoGameModuleInitializer : IGameModuleInitializer
	{
		[Range(4, 13)]
		[DefaultValue(7)]
		[DisplayName("Board Size")]
		public ushort BoardSize { get; set; }

		public void Initialize(INinjectModule module)
		{
			var game = module as AmazonsGameModule;
			if (game == null) return;

			game.BoardSize = BoardSize;
		}
	}

	[GameModule("Game of the Amazons", WikiUrl="http://en.wikipedia.org/wiki/Game_of_the_Amazons")]
	public class AmazonsGameModule : NinjectModule
	{
		public const string DefaultBindName = "Rook.Games.Amazons";
		public static readonly Symbol BoardSizeOptionName = Symbol.Declare<AmazonsGameModule>("BoardSize");

		public const int DefaultBoardSize = 7; // regular Amazons board size is 10

		public ushort BoardSize { get; set; }

		public override void Load()
		{
			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<IGameBoard>().To<BasicBoard>().OnActivation( InitializeBoard );
			Bind<IPieceContainer>().To<BasicPieceContainer>().OnActivation( InitializePieceContainer );

			Bind<ushort>().ToMethod(ctx => GetBoardSizeOrDefault()).Named(BoardSizeOptionName);
			Bind<IGameTileGraph>().ToMethod( CreateInitialTileGraph );

			Bind<IGameMove>().To<AmazonsScoreCommand>().Named(CommandType.UpdateScore);

			Bind<IGameReferee>().To<AmazonsReferee>().Named(DefaultBindName);

			Bind<IResourceInfo>().ToConstant(Resources.SteelSquareBoardBackGround);
			Bind<IResourceInfo>().ToConstant(Resources.ChessPieceSet);
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return new [] {
				new BasicPlayer(ClassicPlayer.White).Tap(p => p.SetAvatar(KnownResource.Avatar.White)),
				new BasicPlayer(ClassicPlayer.Black).Tap(p => p.SetAvatar(KnownResource.Avatar.Black))
			};
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = ClassicPlayer.White;
		}

		private ushort GetBoardSizeOrDefault()
		{
			var size = BoardSize;
			return size.Between<ushort>(4, 31) ? size : (ushort)DefaultBoardSize;
		}

		private void InitializePieceContainer(IPieceContainer container)
		{
			var arrow = new BasicPiece(AmazonsPieces.Arrow).SetDisplayImageOffset(PieceImageSet.LightRook);
			var amazon = new BasicPiece(AmazonsPieces.Amazon).SetArrowClass(AmazonsPieces.Arrow);
			var whiteAmazon = new BasicPiece(amazon, AmazonsPieces.WhiteAmazon)
				.SetBanner(ClassicPlayer.White)
				.SetDisplayImageOffset(PieceImageSet.WhiteQueen)
			;
			var blackAmazon = new BasicPiece(amazon, AmazonsPieces.BlackAmazon)
				.SetBanner(ClassicPlayer.Black)
				.SetDisplayImageOffset(PieceImageSet.BlackQueen)
			;

			container.Add(arrow);
			container.Add(amazon);
			container.Add(whiteAmazon);
			container.Add(blackAmazon);
		}

		private static IGameTileGraph CreateInitialTileGraph(IContext context)
		{
			var size = context.Kernel.Get<ushort>(BoardSizeOptionName.FullName);
			return new BasicTileGraph(
				new RectangularGridTileSet(size, size),
				VectorBasedLinkSet.CreateCardinalPlusIntercardinal()
			);
		}

		private static void InitializeBoard(IContext context, IGameBoard board)
		{
			var whiteAmazonClass = board.Pieces.ById(AmazonsPieces.WhiteAmazon);
			var blackAmazonClass = board.Pieces.ById(AmazonsPieces.BlackAmazon);

			var size = context.Kernel.Get<ushort>(BoardSizeOptionName.FullName);
			var max = size - 1;
			var lo = max / 3;
			var hi = max - lo;

			board.AddPiece(whiteAmazonClass.New(), new PointInt(0, lo).ToTileName());
			board.AddPiece(whiteAmazonClass.New(), new PointInt(lo, 0).ToTileName());
			board.AddPiece(whiteAmazonClass.New(), new PointInt(hi, 0).ToTileName());
			board.AddPiece(whiteAmazonClass.New(), new PointInt(max, lo).ToTileName());
			board.AddPiece(blackAmazonClass.New(), new PointInt(0, hi).ToTileName());
			board.AddPiece(blackAmazonClass.New(), new PointInt(lo, max).ToTileName());
			board.AddPiece(blackAmazonClass.New(), new PointInt(hi, max).ToTileName());
			board.AddPiece(blackAmazonClass.New(), new PointInt(max, hi).ToTileName());
		}
	}
}
