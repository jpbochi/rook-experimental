﻿using System.Reflection;

namespace Rook.Games.Tests.Go
{
	public class GoSerializationTests : BaseSerializationTests
	{
		private readonly Mother mother = new Mother();

		protected override string GetSerializedInitialGameInProd()
		{
			return ReadTextResource("Rook.Games.Tests.SampleData.initial-go.bson.txt", Assembly.GetExecutingAssembly());
		}

		protected override IGameState GetNewInitialState()
		{
			return mother.GetNewState();
		}

		protected override IGameState GetNewChangedState()
		{
			var game = mother.GetNewGameMachine();

			game.PlayDropAt("a1");

			return game.State;
		}

		protected override IGameMachine GetNewGameMachine()
		{
			return mother.GetNewGameMachine();
		}

		protected override void VerifyInitialState(IGameState state)
		{
			GoGameModuleTests.VerifyInitialState(state);

			mother.BindState(state);
			var game = mother.GetNewGameMachine();

			GoRefereeTests.VerifyInitialMoveOptions_Links(game.GetMoveOptions(), game.State.Board);
		}

		protected override void VerifyAnyState(IGameState state)
		{
			GoGameModuleTests.VerifyAnyState(state);
		}
	}
}
