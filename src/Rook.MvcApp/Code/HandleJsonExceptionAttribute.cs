﻿using System;
using System.Net;
using System.Web.Mvc;

namespace Rook.MvcApp
{
	[Obsolete("API Controllers don't need this")]
	public class HandleJsonExceptionAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuted(ActionExecutedContext context)
		{
			if (context.HttpContext.Request.IsAjaxRequest() && context.Exception != null) {
				context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				context.ExceptionHandled = true;

				context.Result = new JsonResult {
					JsonRequestBehavior = JsonRequestBehavior.AllowGet,
					Data = new {
						Message = string.Format("{0}: {1}", context.Exception.GetType().FullName, context.Exception.Message),
						context.Exception.StackTrace
					}
				};
			}
		}
	}
}