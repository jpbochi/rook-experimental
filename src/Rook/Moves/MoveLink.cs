﻿using System;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using JpLabs.Extensions;

namespace Rook
{
	public interface IHyperLink
	{
		string Rel { get; }
		Uri Href { get; }
		string Title { get; }
		object Data { get; }
	}

	public class HyperLink : IHyperLink
	{
		public static readonly Uri BaseUri = new Uri("http://rook");

		public string Rel { get; private set; }

		public Uri Href { get; private set; }
		public string Title { get; private set; }

		public object Data { get; private set; }

		public HyperLink(string href, string relation, string title = null, object data = null)
		: this(new Uri(href, UriKind.RelativeOrAbsolute), relation, title, data) {}

		public HyperLink(Uri href, string relation, string title = null, object data = null)
		{
			this.Href = href;
			this.Rel = relation;
			this.Title = title;
			this.Data = data;
		}

		public static Uri BuildUri(UriTemplate template, object uriArgs = null)
		{
			var args = uriArgs.ToObjectDictionary(value =>
				//HttpUtility.UrlEncode((value ?? "").ToString()) //might be necessary for AugProperties
				(value ?? "").ToString()
			);
			return new Uri(template.BindByName(BaseUri, args).PathAndQuery, UriKind.Relative);
		}

		public static HyperLink From(UriTemplate template, string relation, object uriArgs, object data = null)
		{
			var uri = BuildUri(template, uriArgs);
			var title = data.TryGetProperty("title") as string;
			return new HyperLink(uri, relation, title, data);
		}

		public static bool HasMatch(UriTemplate template, Uri moveUri)
		{
			return Match(template, moveUri) != null;
		}

		public static UriTemplateMatch Match(UriTemplate template, Uri moveUri)
		{
			return template.Match(BaseUri, new Uri(BaseUri, moveUri));
		}
	}

	public static class HyperLinkExt
	{
		public static XElement ToHtmlAnchor(this IHyperLink self)
		{
			var a = new XElement("a", new XAttribute("rel", self.Rel), new XAttribute("href", self.Href));

			a.Add(
				self.Data.ToObjectDictionary().Select(
					kvp => new XAttribute("data-" + kvp.Key, kvp.Value)
				)
			);

			a.Add(self.Title ?? self.Rel);

			return a;
		}

		public static RName GetPlayerId(this IHyperLink self)
		{
			return self.Data.TryGetProperty("playerid") as RName;
		}

		public static RName GetOrigin(this IHyperLink self)
		{
			return self.Data.TryGetProperty("origin") as RName;
		}

		public static RName GetDestination(this IHyperLink self)
		{
			return self.Data.TryGetProperty("destination") as RName;
		}

		public static bool Is<T>(this IHyperLink self) where T : IGameMove
		{
			//TODO: remove this magic (looks like I've been programming in Ruby too much these days)
			var relationProp = typeof(T).GetField("Relation", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			var relation = relationProp.GetValue(null) as string;
			return self.Rel == relation;
		}
	}
}
