﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Extensions;
using System;
using JpLabs.Symbols;

namespace Rook.Games.Amazons
{
	internal class AmazonsReferee : BaseReferee, IGameReferee
	{
		private static readonly MoveBuilderTable moveTable = new MoveBuilderTable(new [] {
			AmazonsSlideMove.Builder,
			AmazonsShootArrowMove.Builder,
			AmazonsScoreCommand.Builder,
		});

		public IEnumerable<IHyperLink> GetMoves(IGameState state)
		{
			if (state.IsOver()) return Enumerable.Empty<HyperLink>();

			var lastPieceToMove = state.GetLastPieceToMove();

			if (lastPieceToMove != null) return GetArrowShotMoves(state, lastPieceToMove).Select(m => m.Link);
			
			return GetAmazonMoves(state).Select(m => m.Link);
		}

		public override IGameMove FindMove(Uri moveUri, IGameState state)
		{
			return moveTable.Match(moveUri, state);
		}

		private IEnumerable<IGameMove> GetAmazonMoves(IGameState state)
		{
			var currentPlayerId = state.CurrentPlayerId;
			var amazonPieces = state.Board.PiecesAt(Tiles.MainBoard).Where( p => p.GetBanner() == currentPlayerId );

			return
				from amazon in amazonPieces
				from destination in SlideDestinations(amazon, state.Board)
				select new AmazonsSlideMove(currentPlayerId, amazon, destination);
		}

		private IEnumerable<IGameMove> GetArrowShotMoves(IGameState state, IGamePiece lastPieceToMove)
		{
			var currentPlayerId = state.CurrentPlayerId;
			var arrowClass = lastPieceToMove.GetArrowClass();

			return
				from destination in SlideDestinations(lastPieceToMove, state.Board)
				select new AmazonsShootArrowMove(currentPlayerId, arrowClass, destination);
		}

		private IEnumerable<RName> SlideDestinations(IGamePiece amazon, IGameBoard board)
		{
			return SlideDestinations(amazon.Position, board);
		}

		private IEnumerable<RName> SlideDestinations(RName origin, IGameBoard board)
		{
			foreach (var initialLink in board.Graph.GetOutLinks(origin)) {
				var link = initialLink;

				while (CanSlideAcrossTile(board.PiecesAt(link.Target))) {
					yield return link.Target;
			
					link = board.Graph.GetLabeledOutLink(link.Target, link.Label);
					if (link == null) break;
				}
			}
		}

		private static bool CanSlideAcrossTile(IEnumerable<IGamePiece> entries)
		{
			return entries.IsEmpty();
		}
	}
}
