﻿using OpenQA.Selenium;
using Xunit;

namespace Rook.Browser.Tests
{
	public class NavigationTests : BaseSeleniumTest
	{
		[Fact]
		void should_open_home_page()
		{
			Driver.Navigate().GoToUrl(BaseUri);
			Driver.FindElement(By.CssSelector("#intro")).ShouldBeVisible("#intro");
		}

		[Fact]
		void should_open_game_list()
		{
			Driver.Navigate().GoToUrl(BaseUri);
			Driver.FindElement(By.CssSelector("a[rel='game list']")).Click();

			Driver.FindElement(By.CssSelector(".game-list-table")).ShouldBeVisible(".game-list-table");
		}
	}
}
