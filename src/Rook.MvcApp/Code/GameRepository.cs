﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using Rook.MvcApp.Entities;
using Rook.MvcApp.Models;

namespace Rook.MvcApp
{
	public interface IGameRepository
	{
		IEnumerable<GameEntry> GetAll();

		GameEntry GetById(int id);

		int Add(GameEntry entry);

		void Update(GameEntry entry);
	}

	public class GameRepository : IGameRepository
	{
		private readonly IModuleLocator moduleLocator;

		public GameRepository() : this(GameModuleLocator.FromCurrentDomain()) {}

		public GameRepository(IModuleLocator moduleLocator)
		{
			this.moduleLocator = moduleLocator;
		}

		private static RookEntities GetEntitiesContext()
		{
			//TODO: remove the direct dependency on the real RookDatabase
			return RookDatabase.GetEntities();
		}

		public IEnumerable<GameEntry> GetAll()
		{
			//TODO: Cache games at least for a couple of seconds (use HttpContext.Current.Cache)
			return GetEntitiesContext().Games.OrderByDescending(g => g.LastPlayedAt).AsEnumerable().Select(_ => GameEntry.FromRecord(_, moduleLocator));
		}

		public GameEntry GetById(int id)
		{
			using (var db = GetEntitiesContext()) {
				var rec = db.Games.FirstOrDefault(g => g.Id == id);
				if (rec == null) return null;

				db.Detach(rec);

				return GameEntry.FromRecord(rec, moduleLocator);
			}
		}

		public int Add(GameEntry entry)
		{
			using (var db = GetEntitiesContext())
			{
				db.Games.AddObject(entry.GetRecord(moduleLocator));
				db.SaveChanges();
			}
			return entry.Id;
		}

		public void Update(GameEntry entry)
		{
			entry.State.Revise();
			using (var db = GetEntitiesContext())
			{
				var rec = entry.GetRecord(moduleLocator);

				db.Games.Attach(rec);
				db.ObjectStateManager.GetObjectStateEntry(rec).SetModified();
				db.Refresh(RefreshMode.ClientWins, rec);

				db.SaveChanges();
			}
		}
	}
}