﻿using System;
using System.Collections;

namespace Rook.Commands
{
	internal class CapturePieceCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return CapturePieceCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new CapturePieceCommand(state.Board.Pieces.ById(match.BoundVariables["piece"]));
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/capture/{piece}");
		internal const string Relation = RookRelations.Commands.CapturePiece;
		public IGamePiece Piece { get; private set; }

		public CapturePieceCommand(IGamePiece piece)
		{
			this.Piece = piece;
		}
		public IHyperLink Link
		{
			get { return HyperLink.From(Template, Relation, new { Piece = Piece.Id }); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			if (Piece == null) throw new InvalidOperationException();

			context.State.Board.MovePiece(Piece, Tiles.OffBoard);
		}
	}
}
