﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rook.Commands
{
	internal class SetGameOverCommand : SetGamePropertyCommand, IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return SetGameOverCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new SetGameOverCommand();
			}
		}

		internal new static readonly IMoveBuilder Builder = new MoveBuilder();
		internal new static readonly UriTemplate Template = new UriTemplate("/set-game-over");
		internal new const string Relation = RookRelations.Commands.SetGameOver;

		public SetGameOverCommand() : base(BasicGameState.CurrentPlayerIdProperty, null) {}

		IHyperLink IGameMove.Link
		{
			get { return HyperLink.From(Template, Relation, null); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }
	}
}