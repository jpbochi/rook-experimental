﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ContentPlaceHolderID="HelperContent" runat="server">
	<div id="intro">
        <div class="fl_left">
            <h1>Welcome*</h1>
            <ul>
            <li>
                <h2>for Players</h2>
                <p>Play the board game you like.</p>
                <p>Online. Free.</p>
                <p>No registration required.</p>
                <p>Real-time or play-by-email style.</p>
                <!--<p class="readmore"><a href="#">Continue Reading &raquo;</a></p>-->
            </li>
            <li class="last">
                <h2>for Developers</h2>
                <p>The engine behind the site is open source.</p>
                <p><a href="https://bitbucket.org/jpbochi/rook">Fork</a> us and create your own game!</p>
                <p>Like to contribute? Help us enhance the engine.</p>
                <!--<p class="readmore"><a href="#">Continue Reading &raquo;</a></p>-->
            </li>
            </ul>
        </div>
        <div class="fl_right">
            <ul id="rotation">
            <li><a href="<%= ResolveUrl("/game/new") %>"><img src="<%= ResolveUrl("~/content/img/demo_slide_1.png")%>" alt="Amazons" /></a></li>
            <li><a href="<%= ResolveUrl("/game/new") %>"><img src="<%= ResolveUrl("~/content/img/demo_slide_2.png")%>" alt="Tic-Tac-Toe" /></a></li>
            <li><a href="<%= ResolveUrl("/game/new") %>"><img src="<%= ResolveUrl("~/content/img/demo_slide_3.png")%>" alt="Go" /></a></li>
            </ul>
        </div>
        <br class="clear" />
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div id="content">
		<h2>About the engine</h2>
		<p>
			Rook's engine is designed to handle nearly any
			<a href="http://en.wikipedia.org/wiki/Abstract_strategy_game">abstract board game</a>. From the extremely simple
			<a href="http://en.wikipedia.org/wiki/Tic-tac-toe">Tic-Tac-Toe</a> to
			<a href="http://en.wikipedia.org/wiki/Chess">Chess</a> and all its
			<a href="http://en.wikipedia.org/wiki/Chess_variant">variants</a>.
		</p>
		<p>
			In addition to strict abstract strategy games,
			<a href="http://en.wikipedia.org/wiki/Stochastic_game">stochastic</a> games will be covered.
			These are the games where luck is involved. Any board games that have dices, for example.
		</p>
		<p>
			Besides that, the key feature will be the support for
			<a href="http://en.wikipedia.org/wiki/Complete_information">incomplete</a> and
			<a href="http://en.wikipedia.org/wiki/Perfect_information">imperfect</a> information games,
			like <a href="http://en.wikipedia.org/wiki/Stratego">Stratego</a>.
			The secrecy and bluffing involved in such games make them much more interesting.
		</p>
		<h2>Other places like this</h2>
		<p>
			The basic idea of this site is not completely original. At least, two similar systems exist.
		</p>
		<ul>
			<li><a href="http://en.wikipedia.org/wiki/Zillions_of_games">Zillions of Games</a></li>
			<li><a href="http://play.chessvariants.org/">Chess Variants Game Courier</a></li>
		</ul>
		<h2><b>*</b> This site is under development</h2>
		<p>
			Many of the features announced here are currently being developed. With time, they shall be released.
			Any programmer or web designer willing to help us build the engine or the site is welcome.
			Currently, there are no plans to monetize the whole thing, so don't expect any finacial reward.
		</p>
	</div>

	<div id="column">
		<!--<div class="holder">
		<h2 class="title"><img src="images/demo/60x60.gif" alt="" />Nullamlacus dui ipsum conseque loborttis</h2>
		<p>Nullamlacus dui ipsum conseque loborttis non euisque morbi penas dapibulum orna. Urnaultrices quis curabitur phasellentesque congue magnis vestibulum quismodo nulla et feugiat adipisciniapellentum leo.</p>
		<p class="readmore"><a href="#">Continue Reading &raquo;</a></p>
		</div>-->
	</div>
	<div class="clear"></div>

	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/jquery.min.js")%>"></script>
	<script type="text/javascript" src="<%= ResolveUrl("~/scripts/lib/jquery.innerfade.js")%>"></script>

	<script type="text/javascript"><!--
	//<![CDATA[
		// **** Apply the fade settings ****
		$(document).ready(
		function(){
			$('#rotation').innerfade({
				animationtype: 'fade',
				speed: 750,
				timeout: 6000,
				type: 'sequence',
				containerheight: 'auto'
			});
		});
	//]]>-->
	</script>
</asp:Content>
