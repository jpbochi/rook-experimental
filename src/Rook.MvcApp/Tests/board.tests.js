﻿/*global module, test, equal, deepEqual */
/*global Rook */

/// <reference path="../scripts/game/board.js" />
/// <reference path="http://code.jquery.com/qunit/qunit-1.10.0.js" />
/// <reference path="lib/sinon.js" />
/// <reference path="lib/sinon-qunit.js" />

module("Board");

test("should give all tiles and pieces", function() {
	var tiles = [
		{ name: "a1" },
		{ name: "h8" }
	];
	var pieces = ['piece one', 'piece two'];

	var board = new Rook.Board(tiles, pieces);

	deepEqual(board.getAllTiles(), tiles, "board.getAllTiles()");
	deepEqual(board.getAllPieces(), pieces, "board.getAllPieces()");
});

test("should give its size", function() {
	var tiles = [
		{ name: 'a1', position: { x: 0, y: 0 } },
		{ name: 'h8', position: { x: 7, y: 7 } }
	];
	var expectedSize = {
		cols: 8,
		rows: 8
	};

	var board = new Rook.Board(tiles, null);

	deepEqual(board.getSize(), expectedSize, "board.getSize()");
});

test("should tell if it contains a tile", function() {
	var tiles = [
		{ name: 'a1', position: {x: 0, y: 0} },
		{ name: 'c4', position: {x: 2, y: 3} }
	];

	var board = new Rook.Board(tiles, null);

	equal(board.hasTile('a1'), true, 'board.hasTile("a1")');
	equal(board.hasTile('c4'), true, 'board.hasTile("c4")');
	equal(board.hasTile('a4'), false, 'board.hasTile("a4")');
	equal(board.hasTile('c1'), false, 'board.hasTile("c1")');
});

test("should give a tile's position", function() {
	var tiles = [
		{ name: 'a1', position: {x: 0, y: 0} },
		{ name: 'c4', position: {x: 2, y: 3} }
	];

	var board = new Rook.Board(tiles, null);

	deepEqual(board.getTilePosition(tiles[0].name), tiles[0].position);
	deepEqual(board.getTilePosition(tiles[1].name), tiles[1].position);
});

test("should return pieces by its id", function() {
	var pieces = [
		new Rook.Piece('42'),
		new Rook.Piece('99'),
		new Rook.Piece('27')
	];

	var board = new Rook.Board(null, pieces);

	equal(board.getPieceById('42'), pieces[0], 'board.getPieceById("42")');
	equal(board.getPieceById('99'), pieces[1], 'board.getPieceById("99")');
	equal(board.getPieceById('27'), pieces[2], 'board.getPieceById("27")');
	equal(board.getPieceById('100'), null, 'board.getPieceById("100")');
});

test("should return pieces at a position", function() {
	var pieces = [
		new Rook.Piece('1', 'a1', {}),
		new Rook.Piece('2', 'a1', {}),
		new Rook.Piece('3', 'a1', {}),
		new Rook.Piece('4', 'b1', {}),
		new Rook.Piece('5', 'b1', {}),
		new Rook.Piece('6', 'a2', {})
	];

	var board = new Rook.Board(null, pieces);

	deepEqual(board.getPiecesAt('a1'), [pieces[0], pieces[1], pieces[2]], 'board.getPiecesAt("a1")');
	deepEqual(board.getPiecesAt('b1'), [pieces[3], pieces[4]], 'board.getPiecesAt("b1")');
	deepEqual(board.getPiecesAt('a2'), [pieces[5]], 'board.getPiecesAt("a2")');
	deepEqual(board.getPiecesAt('b2'), [], 'board.getPiecesAt("b2")');
});
