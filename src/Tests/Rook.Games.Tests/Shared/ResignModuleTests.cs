﻿using System;
using System.Linq;
using Ninject;
using Rook.Games.Common;
using Xunit;

namespace Rook.Games.Tests.Common
{
	public class ResignModuleTests
	{
		private readonly Lazy<IKernel> lazyKernel = new Lazy<IKernel>( StaticGameFactory.GetDefaultKernel );

		private IKernel Kernel
		{
			get { return lazyKernel.Value; }
		}

		private IGameReferee GetNewResignReferee()
		{
			return Kernel.GetReferee(ResignModule.DefaultBindName);
		}

		[Fact]
		void should_allow_all_players_to_resign_at_any_turn()
		{
			var state = RookTestHelper.CreateGameWithPlayers(3);
			state.CurrentPlayerId = state.Players.First().Id;

			var referee = GetNewResignReferee();

			var moveUris = referee.GetMoves(state).Select(m => m.Href);

			var expectedMoves = state.Players.Select(p => HyperLink.From(ResignMove.Template, ResignMove.Relation, new { PlayerId = p.Id }).Href);

			moveUris.ShouldContainSameAs(expectedMoves);
		}

		[Fact]
		void should_not_allow_any_move_when_game_is_over()
		{
			var state = RookTestHelper.CreateGameWithPlayers(3);
			state.CurrentPlayerId = null;

			var referee = GetNewResignReferee();

			var moves = referee.GetMoves(state);

			moves.ShouldBeEmpty();
		}

		[Fact]
		void should_not_allow_moves_to_defeated_players()
		{
			var players = RookTestHelper.CreatePlayers(8).ToArray();
			players[3].Score = GameScore.Lost;
			players[5].Score = GameScore.Resigned;

			var state = new BasicGameState(players, null);
			state.CurrentPlayerId = state.Players.First().Id;

			var referee = GetNewResignReferee();

			var moveUris = referee.GetMoves(state).Select(m => m.Href);

			var expectedMoves = new [] {0, 1, 2, 4, 6, 7}
				.Select(i => players[i])
				.Select(p => HyperLink.From(ResignMove.Template, ResignMove.Relation, new { PlayerId = p.Id }).Href)
			;
			moveUris.ShouldContainSameAs(expectedMoves);
		}
	}
}
