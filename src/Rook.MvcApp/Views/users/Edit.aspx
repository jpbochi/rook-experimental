﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Rook.MvcApp.Models.RookUser>" %>

<%@ Import Namespace="Rook.MvcApp.Controllers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
Rook
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

	<h2>Choose you user name</h2>

	<%: Html.ValidationSummary() %>
	<%-- Html.EnableClientValidation(); --%>

	<%
		var returnUrl = Request.QueryString["ReturnUrl"];
		var queryString = string.IsNullOrWhiteSpace(returnUrl) ? "" : "?ReturnUrl=" + HttpUtility.UrlEncode(returnUrl);
	%>

	<%-- using (Html.BeginForm("register", "account")) { --%>
	<form action="<%= Url.Action(UsersController.EditActionName)%><%= queryString %>" id="openid_form" method="post">
		
		<div>
			<%--: Html.EditorForModel() --%>
			<div>
				<div class="editor-label"><%: Html.LabelFor(m => m.OpenId) %></div>
				<div class="editor-field"><%: Html.DisplayFor(m => m.OpenId) %></div>
				<%: Html.HiddenFor(m => m.OpenId) %>
			</div>
			<div>
				<div class="editor-label"><%: Html.LabelFor(m => m.UserName) %></div>
				<div class="editor-field"><%: Html.EditorFor(m => m.UserName) %></div>
			</div>
		</div>

		<div>
			<div class="editor-label"></div>
			<input type="submit" class="button" value="Save" />
		</div>

		<div class="user-welcome-message">
			<small>- User name is not, yet, being stored in our DB. It's valid only for this session.</small>
			<br />
			<small>- Games are associated to your OpenID. So, only you can play for you.</small>
		</div>
	</form>
	<%-- } --%>
</asp:Content>
