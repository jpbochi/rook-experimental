﻿using System;
using System.Collections;
using Rook.Augmentation;

namespace Rook.Commands
{
	internal class SetPlayerPropertyCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return SetPlayerPropertyCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				var prop = AugProperty.FromRName(match.BoundVariables["property"]);
				return new SetPlayerPropertyCommand(
					match.BoundVariables["player"],
					prop,
					prop.ConvertValue(match.BoundVariables["value"])
				);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/set-player/{player}/{property}/{*value}");
		internal const string Relation = RookRelations.Commands.SetPlayerProperty;

		public RName Player { get; private set; }
		public AugProperty Property { get; private set; }
		public object Value { get; private set; }

		public SetPlayerPropertyCommand(RName player, AugProperty property, object value)
		{
			this.Player = player;
			this.Property = property;
			this.Value = value;
		}

		public IHyperLink Link
		{
			get { return HyperLink.From(Template, Relation, new { Player, Property, Value }); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			context.State.GetPlayerById(Player).SetValue(Property, Value);
		}
	}
}
