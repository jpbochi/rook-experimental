﻿
namespace Rook
{
	public interface IJavascriptRenderer
	{
		string GetBoardRendererArgsConstructor();
	}
	
	public class ConstJavascriptRenderer : IJavascriptRenderer
	{
		private readonly string boardRendererArgsConstructor;

		public ConstJavascriptRenderer(string boardRendererArgsConstructor)
		{
			this.boardRendererArgsConstructor= boardRendererArgsConstructor;
		}

		public string GetBoardRendererArgsConstructor()
		{
			return boardRendererArgsConstructor;
		}
	}
}
