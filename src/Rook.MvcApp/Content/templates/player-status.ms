﻿{{#gameOver}}
	<h4>Game Over</h4>
{{/gameOver}}
{{^gameOver}}
	{{#isCurrentPlayer}}
		<h4>Your turn</h4>
	{{/isCurrentPlayer}}
	{{^isCurrentPlayer}}
		<div class="inline">
			Waiting for:
			<img src="{{currentPlayer.links.icon}}" alt="current player avatar" />
			<h4 class="inline player-name">
				{{#currentPlayer.links.join}}
					<i>open</i>
				{{/currentPlayer.links.join}}
				{{^currentPlayer.links.join}}
					{{currentPlayer.name}}
				{{/currentPlayer.links.join}}
			</h4>
		</div>
	{{/isCurrentPlayer}}
{{/gameOver}}