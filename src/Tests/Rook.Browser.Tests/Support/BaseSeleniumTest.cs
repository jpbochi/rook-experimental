﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using Xunit;

namespace Rook.Browser.Tests
{
	[RunWith(typeof(IISExpressRunner)), IISParams("Rook.MvcApp")]
	public abstract class BaseSeleniumTest : IDisposable
	{
		private readonly IISExpressFixture iis;
		private readonly SeleniumBrowserFixture browser;

		public IWebDriver Driver { get { return browser.Driver; } }
		public Uri BaseUri { get { return iis.BaseUri; } }

		public BaseSeleniumTest()
		{
			iis = new IISExpressFixture(IISExpressRunner.IISProcess);
			browser = new SeleniumBrowserFixture(); 	
		}

		void IDisposable.Dispose()
		{
			browser.Dispose();
			iis.Dispose();
		}
	}
}
