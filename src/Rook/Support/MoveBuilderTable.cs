﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rook
{
	public interface IMoveBuilder
	{
		UriTemplate Template { get; }
		IGameMove Build(UriTemplateMatch match, IGameState state);
	}

	public class MoveBuilderTable
	{
		private readonly UriTemplateTable table;

		public MoveBuilderTable(IEnumerable<IMoveBuilder> builders)
		{
			table = new UriTemplateTable(HyperLink.BaseUri, builders.Select(b => new KeyValuePair<UriTemplate,object>(b.Template, b)));
		}

		public IGameMove Match(Uri moveUri, IGameState state)
		{
			var match = table.MatchSingle(new Uri(HyperLink.BaseUri, moveUri));
			if (match == null) return null;

			return ((IMoveBuilder)match.Data).Build(match, state);
		}
	}
}
