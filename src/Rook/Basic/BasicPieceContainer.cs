﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using JpLabs.Extensions;

namespace Rook
{
	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class BasicPieceContainer : IPieceContainer
	{
		[IgnoreDataMember]private MultiValueDictionary<RName,IGamePiece> boardDict;

		public BasicPieceContainer()
		{
			CreateBoardDictionary();
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			CreateBoardDictionary();
		}

		[DataMember(Name="Pieces", EmitDefaultValue=false)]
		private IEnumerable<IGamePiece> SerializableBoardDict
		{
			get { return boardDict.Values.ToList(); }
			set {
				CreateBoardDictionary();
				foreach (var piece in value) InternalAdd(piece);
				foreach (var piece in value) piece.ResolveReferencesAt(this);
			}
		}

		private void CreateBoardDictionary()
		{
			if (boardDict == null) boardDict = new MultiValueDictionary<RName,IGamePiece>();
		}

		public IEnumerable<IGamePiece> GetAllPiecesOnBoard()
		{
			return boardDict.Values;
		}

		public IGamePiece Add(IGamePiece piece)
		{
			return Add(piece, Tiles.Abstract);
		}

		public IGamePiece Add(IGamePiece piece, RName position)
		{
			piece.SetPosition(position);
			InternalAdd(piece);
			return piece;
		}

		public IGamePiece Move(IGamePiece piece, RName destination)
		{
			if (!Contains(piece)) throw new InvalidOperationException("Piece to be moved should be added to board first");

			InternalRemove(piece);
			piece.SetPosition(destination);
			InternalAdd(piece);

			return piece;
		}

		private bool InternalRemove(IGamePiece piece)
		{
			return boardDict.Remove(piece.Position, piece);
		}

		private void InternalAdd(IGamePiece piece)
		{
			boardDict.Add(piece.Position, piece);
		}

		private bool Contains(IGamePiece piece)
		{
			return boardDict.Contains(piece.Position, piece);
		}

		public IGamePiece ById(RName id)
		{
			//TODO: optimize this function
			return this.Where(p => p.Id == id).FirstOrDefault();
		}

		public IEnumerable<IGamePiece> At(RName position)
		{
			return boardDict[position].ToReadOnlyColl();
		}

		public IEnumerator<IGamePiece> GetEnumerator()
		{
			return boardDict.Values.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
