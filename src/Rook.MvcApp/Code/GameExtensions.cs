﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Mvc;
using System.Web.UI;
using Rook.Api;
using Rook.Games.Go;

namespace Rook.MvcApp
{
	public static class GameMachineExt
	{
		public static IEnumerable<IHyperLink> GetMoveOptions(this IGameMachine game, IOpenIdIdentity user)
		{
			var playersControlledByUser = game.State.Players.Where(p => p.IsControlledBy(user)).Select(p => p.Id).ToArray();

			return game.GetMoveOptions().Where(m => playersControlledByUser.Contains(m.GetPlayerId()));
		}

		public static string GetJSBoardRendererFunc(this IGameMachine game)
		{
			return game.JSRenderer == null ? "null" : game.JSRenderer.GetBoardRendererArgsConstructor();
		}

		public static void RenderResourceImages(this IGameMachine game, HtmlTextWriter writer, UrlHelper Url)
		{
			foreach (var res in game.Resources.OfType<EmbeddedResourceInfo>().Where(r => r.MediaType.Type == MediaType.Image)) {
				writer.Write(res.AsHtml(Url.GetResourceUrl(res)));
			}
		}
	}

	public static class GameStateExt
	{
		public static EntityTagHeaderValue ETag(this IGameState state)
		{
			return new EntityTagHeaderValue(string.Concat("\"", state.Version, "\""));
		}

		public static bool CanPlayAsCurrentPlayer(this IGameState state, IOpenIdIdentity user)
		{
			if (state.IsOver()) return false;
			if (user == null) return false;

			var currentPlayer = state.GetCurrentPlayer();
			return currentPlayer.IsControlledBy(user);
		}

		public static bool CanPlayOrWaitForTurn(this IGameState state, IOpenIdIdentity user)
		{
			if (state.IsOver()) return false;
			if (user == null) return false;

			return state.Players.Any(p => p.IsControlledBy(user));
		}

		public static bool CanJoinAsPlayer(this IGameState state, IOpenIdIdentity user, RName playerId)
		{
			if (state.IsOver()) return false;
			if (user == null) return false;

			var player = state.GetPlayerById(playerId);
			if (player == null) return false;
			
			return player.IsOpen();
		}

		public static bool TryJoinAsPlayer(this IGameState state, IOpenIdIdentity user, RName playerId)
		{
			if (!CanJoinAsPlayer(state, user, playerId)) return false;

			var player = state.GetPlayerById(playerId);
			if (player == null) return false;

			player.SetController(user);
			return true;
		}
	}

	public static class PlayerExt
	{
		public static string GetHtmlName(this IGamePlayer player)
		{
			return player.IsOpen() ? "<i>open</i>" : MvcHtmlString.Create(player.GetName()).ToHtmlString();
		}

		public static string GetExtraScoreDisplay(this IGamePlayer player)
		{
			if (player.IsValueUnset(GoProperties.AreaProperty)) return null;

			return string.Concat("Score: ", player.GetArea());
		}
	}
}
