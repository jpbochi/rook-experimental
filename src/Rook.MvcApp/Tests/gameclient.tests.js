﻿/*global module, test, equal, deepEqual */
/*global Rook */
/// <reference path="http://code.jquery.com/qunit/qunit-1.10.0.js" />
/// <reference path="lib/sinon.js" />
/// <reference path="lib/sinon-qunit.js" />
/// <reference path="../scripts/game/gameclient.js" />
/// <reference path="../scripts/game/gameparser.js" />

module("Rook.GameClient/loadState");

test("should GET and parse state", function() {
	var server = sinon.fakeServer.create();
	server.respondWith(
		'GET', '/api/games/42',
		[200, { "Content-Type": "application/json" }, '{"game":"fake"}']
	);

	var fakeParsedGame = { game: 'parsed' };
	this.stub(Rook.Parser, 'parseState').withArgs({ game: 'fake' }).returns(fakeParsedGame);

	var callback = this.spy();
	Rook.GameClient.loadState('/api/games/42', callback);
	server.respond();

	sinon.assert.calledWith(callback, fakeParsedGame);
});

module("Rook.GameClient/loadGame");

test("should GET and parse game", function() {
	var server = sinon.fakeServer.create();
	server.respondWith(
		'GET', '/api/games/42/for/robin',
		[200, { "Content-Type": "application/json" }, '{"game":"fake"}']
	);

	var fakeParsedGame = { game: 'parsed' };
	this.stub(Rook.Parser, 'parseGame').withArgs({ game: 'fake' }).returns(fakeParsedGame);

	var callback = this.spy();
	Rook.GameClient.loadGame('/api/games/42/for/robin', callback);
	server.respond();

	sinon.assert.calledWith(callback, fakeParsedGame);
});