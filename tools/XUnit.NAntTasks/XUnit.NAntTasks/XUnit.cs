﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using NAnt.Core;
using NAnt.Core.Attributes;
using Xunit;
using System.Collections.Generic;

namespace XUnit.NAntTask
{
	/// Code originally downloaded from: http://www.codinginstinct.com/2008/09/xunit-and-teamcity-support.html

    [TaskName("xunit")]
    public class XUnitNantTask : Task
    {
        public XUnitNantTask()
        {
            ShadowCopy = true;
        }

        [TaskAttribute("assemblies", Required = true)]
        public string Assemblies { get; set; }

        [TaskAttribute("xml")]
        public string Xml { get; set; }

        [TaskAttribute("nunitXml")]
        public string NUnitXml { get; set; }
        
        [TaskAttribute("html")]
        public string Html { get; set; }

        [TaskAttribute("configFile")]
        public string ConfigFile { get; set; }

        [TaskAttribute("workingDir")]
        public string WorkingDir { get; set; }

        [TaskAttribute("shadowCopy")]
        public bool ShadowCopy { get; set; }

        [TaskAttribute("teamCity")]
        public bool TeamCity { get; set; }
        
        protected override void ExecuteTask()
        {
            try
            {
				Log(Level.Verbose, string.Format("TeamCity log is {0}.", TeamCity ? "on" : "off"));

                var assemblyNodes = new List<XmlNode>();

                string currentDirectory = Directory.GetCurrentDirectory();

                if (!string.IsNullOrEmpty(WorkingDir))
                {
                    Directory.SetCurrentDirectory(WorkingDir);
                }

                var assemblyFullnames = Assemblies.Split('|').Select(x => new FileInfo(x).FullName);

				foreach (var assemblyFilename in assemblyFullnames) {
					try
					{
						using (var wrapper = new ExecutorWrapper(assemblyFilename, null, ShadowCopy))
						{
							ForTeamCity("##teamcity[testSuiteStarted name='{0}']", assemblyFilename);

							Log(Level.Info, "xUnit.net XUnit runner (xunit.dll version {0})", wrapper.XunitVersion);
							Log(Level.Info, "Test assembly: {0}", assemblyFilename);

							wrapper.RunAssembly(node =>
													{
														switch (node.Name)
														{
															case "assembly":
																assemblyNodes.Add(node);
																break;

															case "class":
																OnData_Class(node);
																break;

															case "test":
																OnData_Test(node);
																break;
														}

														return true;
													});

							ForTeamCity("##teamcity[testSuiteFinished name='{0}']", assemblyFilename);
						}
					}
					catch (ArgumentException ex)
					{
						Log(Level.Error, ex.Message);
						Log(Level.Error, "While running: {0}", assemblyFilename);
					}
					finally
					{
						Directory.SetCurrentDirectory(currentDirectory);
					}
				}

                if (assemblyNodes.Count == 0) return;

				assemblyNodes.ForEach(node => 
					Log(Level.Info,
						"  Total tests: {0}, Failures: {1}, Skipped: {2}, Time: {3} seconds",
						node.Attributes["total"].Value,
						node.Attributes["failed"].Value,
						node.Attributes["skipped"].Value,
						Double.Parse(node.Attributes["time"].Value, CultureInfo.InvariantCulture).ToString("0.000")
					)
				);

                //string xmlDoc = assemblyNodes[0].OuterXml;

				//var xmlDocument = new XmlDocument();
				//assemblyNodes.ForEach(n => xmlDocument.AppendChild(n));
				//string xmlDoc = xmlDocument.InnerXml;

				var xmlDoc = "<assemblies>" + string.Join("", assemblyNodes.Select(node => node.OuterXml)) + "</assemblies>";

                if (Xml != null)
                {
                    string filename = new FileInfo(Xml).FullName;
                    Log(Level.Info, "  Writing XML: {0}", filename);
                    File.WriteAllText(filename, xmlDoc);
                }

                if (Html != null)
                {
                    string filename = new FileInfo(Html).FullName;
                    Log(Level.Info, "  Writing HTML: {0}", filename);
                    TransformXml(xmlDoc, "HTML.xslt", filename);
                }

                if (NUnitXml != null)
                {
                    string filename = new FileInfo(NUnitXml).FullName;
                    Log(Level.Info, "  Writing NUnit XML: {0}", filename);
                    TransformXml(xmlDoc, "NUnitXml.xslt", filename);
                }

                if (assemblyNodes.Any(node => node.Attributes["failed"].Value == "1")) throw new BuildException("XUnitTask failed");
            }
            catch (Exception ex)
            {
                Exception e = ex;

                while (e != null)
                {
                    Log(Level.Error, e.GetType().FullName + ": " + e.Message);

                    foreach (
                        string stackLine in e.StackTrace.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries))
                        Log(Level.Error, stackLine);

                    e = e.InnerException;
                }

                throw new BuildException("XUnitTask failed");
            }
        }

        private void OnData_Class(XmlNode node)
        {
            if (node.SelectSingleNode("failure") != null)
            {
                Log(Level.Error, "[CLASS] {0}: {1}", node.Attributes["name"].Value,
                    node.SelectSingleNode("failure/message").InnerText);
                Log(Level.Error, node.SelectSingleNode("failure/stack-trace").InnerText);
            }
        }

        private void OnData_Test(XmlNode node)
        {
            string name = node.Attributes["name"].Value;
            ForTeamCity("##teamcity[testStarted name='{0}']", name);

            switch (node.Attributes["result"].Value)
            {
                case "Pass":
                    Log(Level.Verbose, "Passed: {0}", name);
                    break;

                case "Fail":
                    string message = node.SelectSingleNode("failure/message").InnerText.Replace(Environment.NewLine,"\n");
                    string stackTrace = node.SelectSingleNode("failure/stack-trace").InnerText.Replace(Environment.NewLine, "\n");
                    
                    ForTeamCity("##teamcity[testFailed name='{0}' message='{1}' details='{2}']", name, message, stackTrace);

                    Log(Level.Error, "FAILED: {0}: {1}", name, message);
                    Log(Level.Verbose, stackTrace);
                    break;

                case "Skip":
                    string reason = node.SelectSingleNode("reason/message").InnerText.Replace(Environment.NewLine, "\n");

                    ForTeamCity("##teamcity[testIgnored name='{0}' message='{1}']", name, reason);
                    
                    Log(Level.Warning, "Ignored: {0}: {1}", name, reason);
                    break;
            }

            ForTeamCity("##teamcity[testFinished name='{0}']", name);
        }

        static void TransformXml(string xmlDoc, string xmlResourceName, string outputFilename)
        {
            using (Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("XUnit.NAntTasks." + xmlResourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                XPathDocument doc = new XPathDocument(new StringReader(xmlDoc));
                XslCompiledTransform transform = new XslCompiledTransform();
                XmlTextReader transformReader = new XmlTextReader(reader);
                transform.Load(transformReader);

                using (FileStream outStream = new FileStream(outputFilename, FileMode.Create))
                    transform.Transform(doc, null, outStream);
            }
        }

        private void ForTeamCity(string message, params string[] args)
        {
            if (TeamCity)
            {
                var escaped = args.Select(x => x.Replace("'", "|'"));
                Log(Level.Info, message, args);
            }
        }
    }
}