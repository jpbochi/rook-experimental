﻿using System.Runtime.Serialization;
using Rook.Augmentation;

namespace Rook
{
	[DataContract]
	public class BasicPlayer : AugObject, IGamePlayer
	{
		public static AugProperty ScoreProperty =
			AugProperty.Create("Score", typeof(RName), typeof(BasicPlayer), GameScore.StillPlaying);

		[DataMember]public RName Id { get; private set; }
		
		[IgnoreDataMember]
		public RName Score
		{
			get { return this.GetValue<RName>(ScoreProperty); }
			set { SetValue(ScoreProperty, value); }
		}

		public BasicPlayer(RName id)
		{
			this.Id = id;
		}
	}
}
