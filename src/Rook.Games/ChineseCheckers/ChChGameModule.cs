﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using JpLabs.Symbols;
using Ninject.Activation;
using Ninject.Modules;
using Rook.Api;
using JpLabs.Extensions;

namespace Rook.Games.ChineseCheckers
{
	public static class ChineseCheckersPiece
	{
		public const string Namespace = "chinese-checkers.piece";

		public static readonly RName North		= RName.Get("N", Namespace);
		public static readonly RName Northeast	= RName.Get("NE", Namespace);
		public static readonly RName Southeast	= RName.Get("SE", Namespace);
		public static readonly RName South		= RName.Get("S", Namespace);
		public static readonly RName Southwest	= RName.Get("SW", Namespace);
		public static readonly RName Northwest	= RName.Get("NW", Namespace);
	}

	public static class ChineseCheckersPlayer
	{
		internal const string Namespace = "chinese-checkers.player";

		public static readonly RName First	= RName.Get("_1st", Namespace);
		public static readonly RName Second	= RName.Get("_2nd", Namespace);
		public static readonly RName Third	= RName.Get("_3rd", Namespace);
		public static readonly RName Fourth	= RName.Get("_4th", Namespace);
		public static readonly RName Fifth	= RName.Get("_5th", Namespace);
		public static readonly RName Sixth	= RName.Get("_6th", Namespace);
	}

	[GameModule("Chinese Checkers", WikiUrl="http://en.wikipedia.org/wiki/Chinese_checkers")]
	public class ChineseCheckersGameModule : NinjectModule
	{
		public const string DefaultBindName = "Rook.Games.ChineseCheckers";

		public static readonly RName[] NorthRegion = new RName[] { "e4", "f2", "f3", "f4", "g1", "g2", "g3", "g4", "h3", "h4"};
		public static readonly RName[] NorthwestRegion = new RName[] { "a5", "b5", "c5", "d5", "a6", "b6", "c6", "b7", "c7", "b8"};
		public static readonly RName[] NortheastRegion = new RName[] { "j5", "k5", "l5", "m5", "j6", "k6", "l6", "k7", "l7", "k8"};
		public static readonly RName[] SouthwestRegion = new RName[] {"b10", "b11", "c11", "a12", "b12", "c12", "a13", "b13", "c13", "d13"};
		public static readonly RName[] SoutheastRegion = new RName[] {"k10", "k11", "l11", "j12", "k12", "l12", "j13", "k13", "l13", "m13"};
		public static readonly RName[] SouthRegion = new RName[] {"e14", "f14", "g14", "h14", "f15", "g15", "h15", "f16", "g16", "g17"};

		public override void Load()
		{
			Bind<IGameMachine>().To<BasicGameMachine>();

			Bind<IGameState>().To<BasicGameState>().WithConstructorArgument("players", GetPlayers()).OnActivation(StartUpGame);

			Bind<IGameBoard>().To<BasicBoard>().OnActivation( InitializeBoard );
			Bind<IPieceContainer>().To<BasicPieceContainer>().OnActivation( InitializePieceContainer );
			Bind<IGameTileGraph>().ToMethod( CreateInitialTileGraph );

			Bind<IGameMove>().To<ChineseCheckersScoreCommand>().Named(CommandType.UpdateScore);

			Bind<IGameReferee>().To<ChChReferee>().Named(DefaultBindName);

			Bind<IResourceInfo>().ToConstant(Resources.ChineseCheckersBackGround);
			Bind<IResourceInfo>().ToConstant(Resources.CheckersPieceSet);

			Rebind<IJavascriptRenderer>().ToConstant(new ConstJavascriptRenderer(
				@"function (board, imageRepository) {
					return {
						rootObj: new Graphics.HexBoardRoot(board.getSize()),
						templates: {
							circle: new Graphics.CircleTemplate(),
							tile: new Graphics.HexTileTemplate(),
							coordinates: new Graphics.RectBoardCoordinatesTemplate(),
							piece: new Graphics.PieceTemplate(imageRepository.get('.piece-set')).withTransformation(function (ctx) { ctx.scale(0.7, 0.7); })
						},
						coordinateSystem: new Graphics.HexBoardCoordinateSystem(),
						zoom: 0.65
					};
				}"
			));
		}

		public static IEnumerable<IGamePlayer> GetPlayers()
		{
			return new [] {
				new BasicPlayer(ChineseCheckersPlayer.First).Tap(p => p.SetAvatar(KnownResource.Avatar.Red)),
				new BasicPlayer(ChineseCheckersPlayer.Second).Tap(p => p.SetAvatar(KnownResource.Avatar.Green))
			};
		}

		private static void StartUpGame(IGameState state)
		{
			state.CurrentPlayerId = ChineseCheckersPlayer.First;
		}

		private void InitializePieceContainer(IPieceContainer container)
		{
			container.Add(
				new BasicPiece(ChineseCheckersPiece.South)
				.SetBanner(ChineseCheckersPlayer.First)
				.SetDisplayImageOffset(new Point(6, 0))
			);
			container.Add(
				new BasicPiece(ChineseCheckersPiece.North)
				.SetBanner(ChineseCheckersPlayer.Second)
				.SetDisplayImageOffset(new Point(1, 0))
			);
		}

		private static IGameTileGraph CreateInitialTileGraph(IContext context)
		{
			return new BasicTileGraph(new BasicTileSet(new RName[] {
					"e4", "f2", "f3", "f4", "g1", "g2", "g3", "g4", "h3", "h4", //north region
					"a5", "b5", "c5", "d5", "a6", "b6", "c6", "b7", "c7", "b8", //northwest region
					"j5", "k5", "l5", "m5", "j6", "k6", "l6", "k7", "l7", "k8", //northeast region
					"b10", "b11", "c11", "a12", "b12", "c12", "a13", "b13", "c13", "d13", //southwest region
					"k10", "k11", "l11", "j12", "k12", "l12", "j13", "k13", "l13", "m13", //southeast region
					"e14", "f14", "g14", "h14", "f15", "g15", "h15", "f16", "g16", "g17", //south region
					//no checker's land
					              "e5",  "f5",  "g5",  "h5",  "i5",
					       "d6",  "e6",  "f6",  "g6",  "h6",  "i6",
					       "d7",  "e7",  "f7",  "g7",  "h7",  "i7",  "j7",
					"c8",  "d8",  "e8",  "f8",  "g8",  "h8",  "i8",  "j8",
					"c9",  "d9",  "e9",  "f9",  "g9",  "h9",  "i9",  "j9",  "k9",
					"c10", "d10", "e10", "f10", "g10", "h10", "i10", "j10",
					       "d11", "e11", "f11", "g11", "h11", "i11", "j11",
					       "d12", "e12", "f12", "g12", "h12", "i12",
					              "e13", "f13", "g13", "h13", "i13",
				}),
				new HexLinkSet(HexModel.KeepLinesAlined)
			);
		}

		private static void FillRegion(IGameBoard board, IGamePiece pieceClass, IEnumerable<RName> region)
		{
			foreach (var tile in region) board.AddPiece(pieceClass.New(), tile);
		}

		private static void InitializeBoard(IContext context, IGameBoard board)
		{
			var klass = board.Pieces.ById(ChineseCheckersPiece.North);
			if (klass != null) FillRegion(board, klass, NorthRegion);
			
			klass = board.Pieces.ById(ChineseCheckersPiece.South);
			if (klass != null) FillRegion(board, klass, SouthRegion);
			
			klass = board.Pieces.ById(ChineseCheckersPiece.Northwest);
			if (klass != null) FillRegion(board, klass, NorthwestRegion);
			
			klass = board.Pieces.ById(ChineseCheckersPiece.Northeast);
			if (klass != null) FillRegion(board, klass, NortheastRegion);
			
			klass = board.Pieces.ById(ChineseCheckersPiece.Southwest);
			if (klass != null) FillRegion(board, klass, SouthwestRegion);
			
			klass = board.Pieces.ById(ChineseCheckersPiece.Southeast);
			if (klass != null) FillRegion(board, klass, SoutheastRegion);
		}
	}
}
