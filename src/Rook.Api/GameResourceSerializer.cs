﻿using System;
using System.Linq;
using System.Web.Http.Routing;
using JpLabs.Extensions;
using Newtonsoft.Json.Linq;

namespace Rook.Api
{
	public interface IGameResourceSerializer
	{
		UrlHelper Url { get; set; }

		JToken From(IGameState state);
		JToken From(IGameMachine machine, RName player);
	}

	public class GameResourceSerializer : IGameResourceSerializer
	{
		static readonly Uri BaseUri = new Uri("http://rook");

		public UrlHelper Url { get; set; }

		public GameResourceSerializer(UrlHelper url)
		{
			this.Url = url;
		}

		public JToken From(IGameState state)
		{
			var jsonState = new JObject();
			jsonState.Add("version", state.Version);
			jsonState.Add("currentPlayer", (string)state.CurrentPlayerId);
			jsonState.Add("players", GetPlayers(state));
			jsonState.Add("tiles", GetTilesOnMainBoard(state.Board));
			jsonState.Add("pieces", GetPieces(state.Board));
			return jsonState;
		}

		public JToken From(IGameMachine machine, RName player)
		{
			var jsonGame = new JObject();
			jsonGame.Add("player", player.FullName);
			jsonGame.Add("state", From(machine.State));
			jsonGame.Add("moves", GetMovesForPlayer(machine, player));
			return jsonGame;
		}

		private JToken GetPlayer(IGamePlayer player, string gameId)
		{
			return new JObject {
				{ "id", player.Id.FullName },
				{ "score", player.Score.Name },
				{ "name", player.GetName() },
				{ "links", GetPlayerLinks(player, gameId)}
			};
		}

		private JObject JsonLink(string rel, string href)
		{
			return new JObject{ { "rel", rel }, { "href", href } };
		}

		private string Route(string routeName, object routeValues)
		{
			if (Url == null) return "/undefined";
			return Url.Route(routeName, routeValues);
		}

		private JObject JsonLinkForRoute(string rel, object routeValues)
		{
			return JsonLink(rel, Route(rel, routeValues));
		}

		private JToken GetPlayerLinks(IGamePlayer player, string gameId)
		{
			var links = new JArray(
				JsonLinkForRoute(ApiRelation.GameForPlayer, new { gameId, playerid = player.Id })
			);
			var avatarHref = player.GetAvatarHref();
			if (avatarHref != null) links.Add(JsonLink(ApiRelation.Avatar, avatarHref));

			if (player.IsOpen()) links.Add(JsonLinkForRoute(ApiRelation.Join, null));

			return links;
		}

		private JToken GetPlayers(IGameState state)
		{
			return new JArray(state.Players.Select(p => GetPlayer(p, state.Id)));
		}

		private JArray GetTilesOnMainBoard(IGameBoard board)
		{
			if (board == null || board.Graph == null) return new JArray();
			return new JArray(
				board
				.Region(Tiles.MainBoard)
				.Where(t => t.ToPointInt() != null)
				.Select(t => {
					var name = t.ToString();
					var position = t.ToPointInt().Value;
					return new JObject {
						{ "name", name },
						{ "position", new JObject { { "x", position.X }, { "y", position.Y } } }
					};
				})
			);
		}

		private JArray GetPieces(IGameBoard board)
		{
			if (board == null || board.Pieces == null) return new JArray();
			return new JArray(
				board
				.Pieces
				.Select(p => {
					var imagePos = p.GetDisplayImageOffset();
					return new JObject {
						{ "id", (string)p.Id },
						{ "position", (string)p.Position },
						{ "imagePos", new JObject { { "x", imagePos.X }, { "y", imagePos.Y } } }
					};
				})
			);
		}

		private JToken GetMovesForPlayer(IGameMachine machine, RName player)
		{
			return new JArray(
				machine.GetMoveOptions()
				.Where(m => m.GetPlayerId() == player)
				.Select(m => {
					var data = new JObject();
					m.Data.ToObjectDictionary().ForEach(
						kvp => data.Add(kvp.Key, kvp.Value.ToString())
					);
					var moveHref = Route(ApiRelation.Move, new { moveHref = m.Href.ToString() });
					return new JObject { { "href", moveHref }, { "rel", m.Rel }, { "data", data } };
				})
			);
		}
	}
}