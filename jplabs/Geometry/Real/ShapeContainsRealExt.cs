﻿using System;
using System.Diagnostics;

namespace JpLabs.Geometry
{
	internal static class ShapeContainsRealExt
	{
		//static public bool Contains(this IShape s1, IShape s2)
		//{
		//    if (s1.IsVoid || s2.IsVoid)	return false;
		//    if (s1 is BoundingBoxInt)	return Contains((BoundingBoxInt)s1, s2);
		//    if (s1 is PointInt)			return Contains((PointInt)s1, s2);
		//    throw new NotImplementedException();
		//}

		static public bool Contains(this BoundingBoxReal b, IShape s)
		{
			if (b.IsVoid || s.IsVoid)	return false;
			if (b.IsPunctual)			return Contains(b.Lower, s);
			if (s is BoundingBoxReal)	return Contains(b, (BoundingBoxReal)s);
			if (s is PointReal)			return Contains(b, (PointReal)s);
			throw new NotImplementedException();
		}

		static public bool Contains(this PointReal p, IShape s)
		{
			if (s.IsVoid)				return false;
			if (s is BoundingBoxInt)	return Contains((BoundingBoxReal)s, p);
			if (s is PointInt)			return Contains((PointReal)s, p);
			if (s == null) throw new NullReferenceException();
			throw new NotImplementedException();
		}
		
		static public bool Contains(this BoundingBoxReal b, BoundingBoxReal other)
		{
			if (b.IsVoid || other.IsVoid)	return false;
			if (b.IsPunctual)				return Contains(b.Lower, other);
			if (other.IsPunctual)			return Contains(b, other.Lower);

			if (b.Dimensions != other.Dimensions) throw new ArgumentException("Boxes have to have the same number of dimensions");

			for (int i=0; i<b.Dimensions; i++) {
				if ((b.Lower[i] <= other.Lower[i])
				&&  (b.Upper[i] >= other.Upper[i])) return true;
			}
			return false;
		}

		static public bool Contains(this BoundingBoxReal b, PointReal p)
		{
			if (b.IsVoid)		return false;
			if (b.IsPunctual)	return Contains(b.Lower, p);
			
			if (b.Dimensions != p.Dimensions) throw new ArgumentException("'b' has to have the same number of dimensions as 'p'");

			for (int i=0; i<b.Dimensions; i++) {
			    if ((b.Lower[i] <= p[i])
			    &&  (b.Upper[i] >= p[i])) return true;
			}
			return false;
		}

		static public bool Contains(this PointReal p, BoundingBoxReal b)
		{
			if (b.IsVoid)		return false;
			if (!b.IsPunctual)	return false;
			return (p == b.Lower);
		}

		static public bool Contains(this PointReal p1, PointReal p2)
		{
			return p1 == p2;
		}
	}
}
