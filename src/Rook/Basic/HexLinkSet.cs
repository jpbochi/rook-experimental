﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using JpLabs.Geometry;

namespace Rook
{
	public enum HexModel
	{
		///a1  c1  
		///  b1  d1
		///a2  c2  
		///  b2  d2
		///a3  c3  
		///  b3  d3
		KeepColumnsAligned = 0,
		///a1  b1  c1
		///  a2  b2  c2
		///a3  b3  c3
		///  a4  b4  c4
		KeepLinesAlined = 1
	}

	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class HexLinkSet : IGraphLinkSet
	{
		private static readonly VectorInt[][][] directionSets;
		private static readonly string[][] directionNames;

		static HexLinkSet()
		{
			directionSets = new [] {
				new [] {
					new [] {
						new VectorInt(-1, -1), new VectorInt(-1, 0),//NW and SW
						new VectorInt( 0, -1), new VectorInt( 0, 1),//N and S
						new VectorInt( 1, -1), new VectorInt( 1, 0)	//NE and SE
					},
					new [] {
						new VectorInt(-1, 0), new VectorInt(-1, 1),	//NW and SW
						new VectorInt( 0, -1), new VectorInt( 0, 1),//N and S
						new VectorInt( 1, 0), new VectorInt( 1, 1)	//NE and SE
					}
				},
				new [] {
					new [] {
						new VectorInt(-1, -1), new VectorInt(0, -1),//NW and NE
						new VectorInt(-1,  0), new VectorInt(1,  0),//W and E
						new VectorInt(-1,  1), new VectorInt(0,  1)	//SW and SE
					},
					new [] {
						new VectorInt(0, -1), new VectorInt(1, -1),	//NW and NE
						new VectorInt(-1, 0), new VectorInt(1,  0),	//W and E
						new VectorInt(0,  1), new VectorInt(1,  1)	//SW and SE
					}
				}
			};
			directionNames = new [] {
				new [] {"NW", "SW", "N", "S", "NE", "SE"},
				new [] {"NW", "NE", "W", "E", "SW", "SE"}
			};
		}

		[DataMember]
		public HexModel Model { get; private set; }

		public HexLinkSet(HexModel model)
		{
			this.Model = model;
		}

		public IEnumerable<IGraphLink> GetOutLinks(RName tile)
		{
			var nullablePosition = tile.ToPointInt();
			if (nullablePosition == null) yield break;

			var position = nullablePosition.Value;
			var labels = directionNames[(int)Model];
			var directions = directionSets[(int)Model][position[(int)Model] % 2];
			
			var labelIndex = 0;
			foreach (var direction in directions) {
				var target = (position + direction).ToTileName();
			
				yield return new BasicBoardLink(labels[labelIndex++], tile, target);
			}
		}
	}
}
