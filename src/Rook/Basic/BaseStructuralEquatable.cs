﻿using System;
using System.Collections;

namespace Rook
{
	[Obsolete("Such a lovely class, but nobody is using it now.")]
	public abstract class BaseStructuralEquatable : IStructuralEquatable
	{
		public override bool Equals(object obj)
		{
			return StructuralComparisons.StructuralEqualityComparer.Equals(this, obj);
		}

		public override int GetHashCode()
		{
			return GetHashCode(StructuralComparisons.StructuralEqualityComparer);
		}

		public bool Equals(object objOther, IEqualityComparer comparer)
		{
			var other = objOther as BaseStructuralEquatable;
			if (other == null) return false;
			if (!GetType().IsAssignableFrom(other.GetType())) return false;

			return comparer.Equals(GetEquatableSurrogate(), other.GetEquatableSurrogate());
		}

		public int GetHashCode(IEqualityComparer comparer)
		{
			return comparer.GetHashCode(GetEquatableSurrogate());
		}

		protected abstract IStructuralEquatable GetEquatableSurrogate();
	}
}
