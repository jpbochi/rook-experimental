﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rook.MvcApp.Models.GameEntryViewModel>" %>

<%@ Import Namespace="Rook" %>
<%@ Import Namespace="Rook.Api" %>
<%@ Import Namespace="Rook.MvcApp" %>

<%
var game = Model.Entry;
%>

<div class="play-or-wait-area">
	<% if (game.State.CanPlayAsCurrentPlayer(Page.User.GetOpenIdIdentity())) { %>

		<input type="button" class="button" value="Play" id="button-play-move" />
		<input id="auto-play-checkbox" type="checkbox" />
		<label for="auto-play-checkbox" id="auto-play-checkbox-label">{...}</label>

	<% } else if (game.State.CanPlayOrWaitForTurn(Page.User.GetOpenIdIdentity())) { %>

		<img id="loading" alt="waiting for other player" src="<%= ResolveUrl("~/content/img/loading.gif") %>" />
		<input type="button" class="button" value="Wait" id="waitturn" />

	<% } %>
</div>

<div id="message-area" style="display:<%= ViewData.ModelState.IsValid ? "none" : "block"  %>;">
	<div class="message-text"><%: Html.ValidationSummary() %></div>
</div>

