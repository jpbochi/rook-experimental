﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rook.Games.TicTacToe;
using Rook.Tests.Extensions;
using Xunit;
using Xunit.Extensions;

namespace Rook.Games.Tests.TicTacToe
{
	public class TicTacToeScoreCommandTests
	{
		private readonly Mother mother = new Mother();

		public static IEnumerable<object[]> WinningConditions
		{
			get { return Mother.GetWinningBoards().Select(t => new object[] { t.Item1, t.Item2 }); }
		}

		[Theory]
		[PropertyData("WinningConditions")]
		void should_win(string boardDesc, RName expectedWinner)
		{
			mother.ParseAndBindBoard(boardDesc);
			var state = mother.GetNewState();

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new TicTacToeScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			var expectedLooser = state.Players.First(p => p.Id != expectedWinner).Id;
			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", expectedWinner, GameScore.Won), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", expectedLooser, GameScore.Lost), UriKind.Relative),
				new Uri("/set-game-over", UriKind.Relative),
			});
		}

		[Theory]
		[FunctionData(typeof(Mother), "GetDrawBoards")]
		void should_draw(string boardDesc)
		{
			mother.ParseAndBindBoard(boardDesc);
			var state = mother.GetNewState();

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new TicTacToeScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			recorder.Commands.ShouldContainSameAs(new [] {
				new Uri(string.Format("/set-player-score/{0}/{1}", state.Players.First().Id, GameScore.Drawn), UriKind.Relative),
				new Uri(string.Format("/set-player-score/{0}/{1}", state.Players.Last().Id, GameScore.Drawn), UriKind.Relative),
				new Uri("/set-game-over", UriKind.Relative),
			});
		}

		[Theory]
		[FunctionData(typeof(Mother), "GetGameNotEndedBoards")]
		void should_continue_game(string boardDesc)
		{
			mother.ParseAndBindBoard(boardDesc);
			var state = mother.GetNewState();

			var recorder = new UriCommandRecorder(mother.GetNewReferee());
			var cmd = new TicTacToeScoreCommand();
			cmd.Execute(new MoveExecutionContext(state, recorder, TestUser.AsReferee));

			recorder.Commands.ShouldBeEmpty();
		}
	}
}
