@echo off
where /Q wget
IF NOT %ERRORLEVEL% == 0 (
	echo wget not found.
	exit 0
)
where /Q sed
IF NOT %ERRORLEVEL% == 0 (
	echo sed not found.
	exit 0
)

wget --convert-links http://localhost:9725/runtests -O runtests.html -q
IF %ERRORLEVEL% == 0 (
	sed 's/http:\/\/localhost:9725\///' runtests.html > src/Rook.MvcApp/runtests.html
	echo Successfully updated JS test runner html file.
) ELSE (
	echo Failed to get http://localhost:9725/runtests
)
del /Q runtests.html