﻿using System;
using System.Collections.Generic;
using JpLabs.Symbols;

namespace Rook.Games.Go
{
	internal class PassTurnMove : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return PassTurnMove.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				return new PassTurnMove(match.BoundVariables["playerid"]);
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		public static UriTemplate Template = new UriTemplate("/pass/{playerid}");
		internal static string Relation = RookRelations.Moves.PassTurn;

		public RName PlayerId { get; private set; }

		public PassTurnMove(RName playerId)
		{
			this.PlayerId = playerId;
		}

		string IGameMove.RequiredRole { get { return PlayerId; } }

		public IHyperLink Link
		{
			get { return HyperLink.From(Template, Relation, new { PlayerId }, new { PlayerId, Title = "Pass Turn" }); }
		}

		public void Execute(IMoveExecutionContext context)
		{
			var state = context.State;
			var previousMove = state.GetPreviousMove();
			bool wasPreviousMovePass = previousMove != null && HyperLink.HasMatch(Template, previousMove);

			var cmds = new List<Uri>();

			if (wasPreviousMovePass) {
				cmds.Add(HyperLink.BuildUri(CommandTemplates.SetGameOver));
			} else {
				cmds.Add(HyperLink.BuildUri(CommandTemplates.SetGameProperty, new { Property = GoProperties.PreviousMoveProperty, Value = Link.Href }));
				cmds.Add(HyperLink.BuildUri(CommandTemplates.ResetGameProperty, new { Property = GoProperties.PreviousBoardProperty }));
				cmds.Add(HyperLink.BuildUri(CommandTemplates.CycleTurn));
			}

			cmds.Add(HyperLink.BuildUri(GoScoreCommand.Template));

			foreach (var cmd in cmds) context.ExecuteMove(cmd);
		}
	}
}
