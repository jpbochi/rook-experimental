﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using JpLabs.Extensions;

namespace Rook
{
	[DataContract(Namespace=RookDataContracts.Namespace)]
	public class BasicTileSet : IGraphTileSet
	{
		[IgnoreDataMember]
		private ICollection<RName> tiles;

		public BasicTileSet(IEnumerable<RName> tiles)
		{
			this.tiles = tiles.ToReadOnlyColl();
		}

		[DataMember(Name="tiles", EmitDefaultValue=false)]
		private string SerializableTiles
		{
			get { return tiles.EmptyIfNull().Select(t => t.ToString()).Join("|"); }
			set { tiles = value.Split('|').Select(t => (RName)t).ToReadOnlyColl(); }
		}

		public bool Contains(RName tile)
		{
			return tiles.Contains(tile);
		}

		public IEnumerable<RName> GetAllTiles()
		{
			return tiles;
		}
	}
}
