﻿using System;

namespace Rook.MvcApp.Entities
{
	public partial class GameRec
	{
		public GameRec()
		{
			CreatedAt = DateTime.UtcNow;
		}

		partial void OnZippedDataChanged()
		{
			LastPlayedAt = DateTime.UtcNow;
		}
	}
}