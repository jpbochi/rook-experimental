﻿using System.Web;
using System.Web.Security;
using Rook.Api;

namespace Rook.MvcApp.RookAuthentication
{
	public interface IFormsAuthentication
	{
		void SignIn(string userName, string friendlyName);
		void SignOut();
	}

	public class FormsAuthenticationService : IFormsAuthentication
	{
		public void SignIn(string userName, string friendlyName)
		{
			var authTicket = RookIdentity.GenerateTicket(userName, friendlyName);

			string encTicket = FormsAuthentication.Encrypt(authTicket);

			HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
		}

		public void SignOut()
		{
			FormsAuthentication.SignOut();
		}
	}
}