﻿using System;
using System.Collections;
using Rook.Augmentation;

namespace Rook.Commands
{
	internal class SetGamePropertyCommand : IGameMove
	{
		class MoveBuilder : IMoveBuilder
		{
			UriTemplate IMoveBuilder.Template { get { return SetGamePropertyCommand.Template; } }

			public IGameMove Build(UriTemplateMatch match, IGameState state)
			{
				var prop = AugProperty.FromRName(match.BoundVariables["property"]);
				return new SetGamePropertyCommand(prop, prop.ConvertValue(match.BoundVariables["value"]));
			}
		}

		internal static readonly IMoveBuilder Builder = new MoveBuilder();
		internal static readonly UriTemplate Template = new UriTemplate("/set/{property}/{*value}");
		internal const string Relation = RookRelations.Commands.SetGameProperty;

		public AugProperty Property { get; private set; }
		public object Value { get; private set; }

		public SetGamePropertyCommand(AugProperty property, object value)
		{
			this.Property = property;
			this.Value = value;
		}

		public IHyperLink Link
		{
			get { return HyperLink.From(Template, Relation, new { Property, Value }); }
		}

		string IGameMove.RequiredRole { get { return RookRoles.Referee; } }

		public void Execute(IMoveExecutionContext context)
		{
			if (Property == null) throw new InvalidOperationException();

			context.State.SetValue(Property, Value);
		}
	}
}
