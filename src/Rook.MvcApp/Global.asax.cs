﻿using System;
using System.IO;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Xml.Linq;
using Newtonsoft.Json;
using Rook.Api;
using Rook.MvcApp.Api;
using Rook.MvcApp.Controllers;
using Rook.MvcApp.RookAuthentication;

namespace Rook.MvcApp
{
	public class MvcApplication : HttpApplication
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapHttpRoute(
				ApiRelation.GameForPlayer,
				"api/games/{gameId}/for/{playerId}",
				new { controller = GamesController.ControllerName, action = ApiRelation.GameForPlayer }
			);
			routes.MapHttpRoute(
				ApiRelation.Move,
				"api/games/{gameId}/move/{*moveHref}",
				new { controller = GamesController.ControllerName, action = ApiRelation.Move }
			);
			routes.MapHttpRoute(
				ApiRelation.Game,
				"api/games/{gameId}",
				new { controller = GamesController.ControllerName }
			);
			routes.MapHttpRoute(
				ApiRelation.Join,
				"api/to-be-implemented"
			);

			routes.MapRoute(
				"play-route",
				"play/{id}",
				new { controller = PlayController.ControllerName, action = PlayController.PlayActionName }
			);

			routes.MapRoute( //TODO: obsolete route to be replaced by game-for-player
				"play-moves",
				"play/moves/{gameId}/for/{playerId}",
				new { controller = PlayController.ControllerName, action = "moves" }
			);

			routes.MapRoute(
				"qunit-tests-route",
				"runtests",
				new { controller = TestsController.ControllerName, action = TestsController.RunTestsActionName }
			);
			routes.MapRoute(
				"default-route",
				"{controller}/{action}/{id}",
				new { controller = "home", action = "index", id = UrlParameter.Optional }
			);
		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterRoutes(RouteTable.Routes);

			//http://www.asp.net/web-api/overview/formats-and-model-binding/json-and-xml-serialization
			GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
			var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
			json.UseDataContractJsonSerializer = false;
			json.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

			ModelBinders.Binders.DefaultBinder = new JsonModelBinder();

			//TODO: install this and verify if cache is invalidated after a move is done
			//GlobalConfiguration.Configuration.MessageHandlers.Add(new CacheCow.Server.CachingHandler());
		}

		public override void Init()
		{
			this.PostAuthenticateRequest += new EventHandler(MvcApplication_PostAuthenticateRequest);
			base.Init();
		}

		void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
		{
			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			if (authCookie == null) return;

			string encTicket = authCookie.Value;
			if (String.IsNullOrWhiteSpace(encTicket)) return;

			var ticket = FormsAuthentication.Decrypt(encTicket);
			var id = new RookIdentity(ticket);
			var principal = new GenericPrincipal(id, null);
			HttpContext.Current.User = principal;
		}
	}
	//*/
	public class JsonModelBinder : DefaultModelBinder
	{
		public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			//http://stackoverflow.com/questions/4164114/posting-json-data-to-asp-net-mvc
			//http://odetocode.com/blogs/scott/archive/2009/04/27/6-tips-for-asp-net-mvc-model-binding.aspx

			if (!IsJSONRequest(controllerContext, bindingContext)) {
				return base.BindModel(controllerContext, bindingContext);
			}

			// Get the JSON data that's been posted
			var requestStream = controllerContext.HttpContext.Request.InputStream;
			requestStream.Seek(0, SeekOrigin.Begin);
			var jsonStringData = new StreamReader(requestStream).ReadToEnd();
			requestStream.Seek(0, SeekOrigin.Begin);

			// Use the built-in serializer to do the work for us
			//return new JavaScriptSerializer().Deserialize(jsonStringData, bindingContext.ModelMetadata.ModelType);

			if (bindingContext.ModelType == typeof(XDocument)) return JsonConvert.DeserializeXNode(jsonStringData);

			return JsonConvert.DeserializeObject(jsonStringData);

			// -- REQUIRES .NET4
			// If you want to use the .NET4 version of this, change the target framework and uncomment the line below
			// and comment out the above return statement
			//return new JavaScriptSerializer().Deserialize(jsonStringData, bindingContext.ModelMetadata.ModelType);
		}

		private static bool IsJSONRequest(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var modelType = bindingContext.ModelType;
			if (modelType != typeof(XDocument) && modelType != typeof(string)) return false;

			var contentType = controllerContext.HttpContext.Request.ContentType;
			return contentType.Contains("application/json");
		}
	}
	/*/
	class MyJsonValueProviderFactory : Microsoft.Web.Mvc.JsonValueProviderFactory
	{
		public override IValueProvider GetValueProvider(ControllerContext controllerContext)
		{
			var provider = base.GetValueProvider(controllerContext) as DictionaryValueProvider<object>;
			if (provider == null) return null;

			var json = GetDeserializedJson(controllerContext);

			//http://msdn.microsoft.com/en-us/library/bb412179.aspx

			return null;//provider; //new MyValueProvider(provider);
		}

		private static object GetDeserializedJson(ControllerContext controllerContext)
		{
			if (!controllerContext.HttpContext.Request.ContentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase)) return null;

			controllerContext.HttpContext.Request.InputStream.Seek(0, SeekOrigin.Begin);

			string str = new StreamReader(controllerContext.HttpContext.Request.InputStream).ReadToEnd();

			if (string.IsNullOrEmpty(str)) return null;

			//var serializer = new JavaScriptSerializer();
			//return serializer.DeserializeObject(str);
			//return JsonConvert.DeserializeObject(str);
			return JsonConvert.DeserializeXNode(str);
		}
	}

	class MyValueProvider : IValueProvider
	{
		readonly IValueProvider provider;

		public MyValueProvider(IValueProvider provider)
		{
			this.provider = provider;
		}

		#region IValueProvider Members

		bool IValueProvider.ContainsPrefix(string prefix)
		{
			return provider.ContainsPrefix(prefix);
		}

		ValueProviderResult IValueProvider.GetValue(string key)
		{
			return provider.GetValue(key);
		}

		#endregion
	}//*/
}